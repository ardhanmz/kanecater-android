package ardhanmz.com.kanecater.acitivities;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.fragment.LihatAnakFragment;
import ardhanmz.com.kanecater.fragment.TambahAnakFragment;
import ardhanmz.com.kanecater.views.FragNavController;

public class ManajemenAnakActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manajemen_anak);
        String bundle = getIntent().getStringExtra("args");
        if (bundle.equals("tambah_anak")){
            Bundle bundle2 = new Bundle();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            TambahAnakFragment tambahAnakFragment = new TambahAnakFragment();
            tambahAnakFragment.setArguments(bundle2);
            fragmentTransaction.replace(R.id.fragmentFrame,tambahAnakFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
        if(bundle.equals("lihat_anak")){
            Integer idanak = getIntent().getIntExtra("idanak", 0);
            Bundle bundle1 = new Bundle();
            bundle1.putInt("idanak",idanak);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            LihatAnakFragment lihatAnakFragment = new LihatAnakFragment();
            lihatAnakFragment.setArguments(bundle1);
            fragmentTransaction.replace(R.id.fragmentFrame, lihatAnakFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
//        MainActivity.pos = 4;
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
    }
}
