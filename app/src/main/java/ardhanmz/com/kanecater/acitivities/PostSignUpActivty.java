package ardhanmz.com.kanecater.acitivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.security.MessageDigest;

import ardhanmz.com.kanecater.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PostSignUpActivty extends AppCompatActivity {
    @BindView(R.id.post_username)
    TextView post_username;
    @BindView(R.id.post_nothankyou)
    TextView post_nothankyou;
    @BindView(R.id.post_start)
    Button post_start;
    private String username, password,email ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_sign_up_activty);
        ButterKnife.bind(this);
        arguments();
        onClick();
    }

    private void arguments() {
        username = getIntent().getStringExtra("username");
        post_username.setText(username);
    }

    private void onClick() {
        post_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), ManajemenAnakActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtra("args", "tambah_anak");
                startActivity(intent);
            }
        });
        post_nothankyou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("username", username);
                bundle.putString("password", password);
                bundle.putString("email",email);
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
