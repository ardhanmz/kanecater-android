package ardhanmz.com.kanecater.acitivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.adapter.MasakanAdapter;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.response.PaketCustomResponse;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

public class PaketCustomActivity extends AppCompatActivity {
    @BindView(R.id.rv_kategori_masakan)
    RecyclerView data_masakan;
    RecyclerELEAdapter recyclerELEAdapter;
    ViewDialog viewDialog;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    ArrayList<PaketCustomResponse> arrayList = new ArrayList<>();
    private  MasakanAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        setContentView(R.layout.activity_paketcustom);
        ButterKnife.bind(this);
        viewDialog = new ViewDialog(this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        data_masakan.setLayoutManager(mLayoutManager);
        data_masakan.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        data_masakan.setItemAnimator(new DefaultItemAnimator());
        data_masakan.setAdapter(adapter);;
        View loadingView = getLayoutInflater().inflate(R.layout.layout_rvloading, data_masakan, false);
        View errorView = getLayoutInflater().inflate(R.layout.layout_rv_error, data_masakan, false);
        View emptyView = getLayoutInflater().inflate(R.layout.layout_rvempty, data_masakan, false);
        data_masakan.setHasFixedSize(true);
        adapter = new MasakanAdapter(this, arrayList);
        data_masakan.addOnItemTouchListener(new RecyclerItemClickListener(getBaseContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Integer id_paket = arrayList.get(position).getID();
                Bundle bundle = new Bundle();
                bundle.putInt("idpaket",id_paket);
                Intent intent = new Intent(getBaseContext(), Pemesanan.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }));
        recyclerELEAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
        data_masakan.setAdapter(recyclerELEAdapter);
        loadDataMasakan();
        viewDialog.showDialog();
    }

    private void loadDataMasakan() {
        compositeSubscription.add(NetworkUtil.getRetrofit().getAllMasakan()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void onError(Throwable throwable) {
    }

    private void handleResponse(ArrayList<PaketCustomResponse> masakanModelArrayList) {
        viewDialog.hideDialog();
        if (masakanModelArrayList!=null){
            arrayList.clear();
            arrayList.addAll(masakanModelArrayList);
            recyclerELEAdapter.setCurrentView(RecyclerELEAdapter.VIEW_NORMAL);
            recyclerELEAdapter.notifyDataSetChanged();
        }else {
            recyclerELEAdapter.notifyDataSetChanged();
            recyclerELEAdapter.setCurrentView(RecyclerELEAdapter.VIEW_EMPTY);
        }

    }
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }
    }
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }

}
