package ardhanmz.com.kanecater.acitivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.shobhitpuri.custombuttons.GoogleSignInButton;

import java.io.IOException;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.Data;
import ardhanmz.com.kanecater.response.LoginResponse;
import ardhanmz.com.kanecater.model.Pengguna;
import ardhanmz.com.kanecater.netutil.ResponseLogin;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.utils.Constants;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static ardhanmz.com.kanecater.utils.Validator.validateFields;

public class SignInActivity extends AppCompatActivity {
    @BindView(R.id.et_uname)
    EditText et_uname;
    @BindView(R.id.et_password) EditText et_password;
    @BindView(R.id.ti_uname)
    TextInputLayout ti_uname;
    @BindView(R.id.ti_password) TextInputLayout ti_password;
    @BindView(R.id.masuk)
    Button signInButton;
    @BindView(R.id.googleSignInButton)
    GoogleSignInButton gSignInButton;
    @BindView(R.id.tv_forgot_password)
    TextView forgot_password;
    @BindView(R.id.tv_register) TextView registerButton;

    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    ScrollView scrollView;
    ViewDialog viewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_activity);
        ButterKnife.bind(this);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        viewDialog = new ViewDialog(this);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginProcess();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    private void register() {
        Intent register = new Intent(getBaseContext(), SignUpActivity.class);
        startActivity(register);
    }

    private void setError(){
        ti_uname.setError(null);
        ti_password.setError(null);
    }
    private void loginProcess() {
        setError();

        String username = et_uname.getText().toString();
        String password = et_password.getText().toString();
        int err = 0;
        if (!validateFields(username)){
            err++;
            ti_uname.setError("Username Tidak Boleh Kosong");
        }

        if (!validateFields(password)){
            err++;
            ti_password.setError("Password Tidak Boleh Kosong");
        }
        if (err == 0){
            Pengguna pengguna = new Pengguna();
            pengguna.setUsername(username);
            pengguna.setPassword(password);
            proses(pengguna);
        }
    }

    private void proses(Pengguna pengguna) {
        viewDialog.showDialog();
        compositeSubscription.add(NetworkUtil.getRetrofit().login(pengguna)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void handleResponse(LoginResponse responseLogin){
        viewDialog.hideDialog();
        boolean status = responseLogin.getSuccess();
        Data data = new Data();
        if (status){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(Constants.IDPENGGUNA, responseLogin.getData().getIdpengguna());
            editor.putString(Constants.USERNAME, responseLogin.getData().getUsername());
            editor.putString(Constants.PASSWORD, responseLogin.getData().getPassword());
            editor.putBoolean(Constants.IS_LOGGED_IN, true);
            editor.apply();
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            compositeSubscription.unsubscribe();
        }else {
            Toasty.info(getApplicationContext(), responseLogin.getMessage(), Toasty.LENGTH_SHORT, true).show();
        }
    }

    private void onError(Throwable err){
        if(err instanceof HttpException){
            Gson gson = new GsonBuilder().create();
            try {
                String errorBody = ((HttpException)err).response().errorBody().string();
                ResponseLogin response = gson.fromJson(errorBody, ResponseLogin.class);
                showSnackBarMessage(response.getMessage());
            } catch (IOException er){
                er.printStackTrace();
            }
        }else {
//            showSnackBarMessage("Kesalahan Jaringan");
        }
    }
    private void showSnackBarMessage(String message) {
//        Snackbar.make(scrollView,message,Snackbar.LENGTH_SHORT).show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
