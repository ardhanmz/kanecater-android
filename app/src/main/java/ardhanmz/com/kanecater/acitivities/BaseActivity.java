package ardhanmz.com.kanecater.acitivities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import ardhanmz.com.kanecater.R;

/**
 * Created by Zephyrus on 09/02/2019.
 */

public class BaseActivity extends AppCompatActivity{


    private ProgressDialog mProgressDialog;

    @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage("Loading...");
        }
    public void showDialog() {

        if(mProgressDialog != null && !mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    public void hideDialog() {

        if(mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

        public void initToolbar(Toolbar toolbar, boolean isBackEnabled) {
            setSupportActionBar(toolbar);

            if(isBackEnabled) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);

            }
        }

        public void initToolbar(Toolbar toolbar, String title, boolean isBackEnabled) {

            setSupportActionBar(toolbar);

            if(isBackEnabled) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);

            }

            getSupportActionBar().setTitle(title);



        }

    @Override
    protected void onStart() {
        super.onStart();
    }
}

