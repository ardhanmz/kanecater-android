package ardhanmz.com.kanecater.acitivities;

import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import ardhanmz.com.kanecater.adapter.SearchAdapter;
import ardhanmz.com.kanecater.fragment.PaketLanggananFragment;
import ardhanmz.com.kanecater.response.SearchResponse;
import ardhanmz.com.kanecater.utils.BottomNavigationViewHelper;
import ardhanmz.com.kanecater.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

public class Pencarian extends AppCompatActivity implements SearchView.OnQueryTextListener{
    @BindView(R.id.nav_bar_pencarian)
    BottomNavigationView nav_pencarian;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
//    @BindView(R.id.rv_search)
//    RecyclerView rv_search;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    ArrayList<SearchResponse> arrayList;
    SearchAdapter adapter;
    RecyclerELEAdapter RAdapter;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private MenuItem itemsearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pencarian);
        ButterKnife.bind(this);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        BottomNavigationViewHelper.removeShiftMode(nav_pencarian);
        nav_pencarian.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Menu nav_menu = nav_pencarian.getMenu();
        MenuItem menuItem = nav_menu.getItem(2);
        menuItem.setChecked(true);
        setSupportActionBar(toolbar);
//        rv_search.setLayoutManager(new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false));
//        View loadingView = getLayoutInflater().inflate(R.layout.layout_rvloading, rv_search, false);
//        View errorView = getLayoutInflater().inflate(R.layout.layout_rv_error, rv_search, false);
//        View emptyView = getLayoutInflater().inflate(R.layout.layout_rvempty, rv_search, false);
//        adapter = new SearchAdapter(this, arrayList);
//        RAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
//        rv_search.setAdapter(adapter);
//        rv_search.addOnItemTouchListener(new RecyclerItemClickListener(getBaseContext(), new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                /*Intent intent = new Intent(getBaseContext(), ManajemenAnakActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putInt("data", anaklist.get(position).getIdanak());
//                intent.putExtras(bundle);
//                startActivity(intent);*/
//                Integer idlanggann = arrayList.get(position).getIdpaket();
//                String id = Integer.toString(idlanggann);
//                Bundle bundle = new Bundle();
//                bundle.putString("id", id);
//                /*Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();*/
//                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                PaketLanggananFragment paketLanggananFragment = new PaketLanggananFragment();
//                DetailPaketFragment detailPaketFragment = new DetailPaketFragment();
//                detailPaketFragment.setArguments(bundle);
//                fragmentTransaction.replace(R.id.frame,detailPaketFragment);
//                fragmentTransaction.commit();
//            }
//        }));
    }
    private void searchData(String que){
        Bundle bundle = new Bundle();
        bundle.putString("query", que);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        PaketLanggananFragment paketLanggananFragment = new PaketLanggananFragment();
        paketLanggananFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frame_search,paketLanggananFragment);
        fragmentTransaction.commit();
//        compositeSubscription.add(NetworkUtil.getRetrofit().search(que)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(this::handleResponse,this::onError));
    }
//
//    private void handleResponse(ArrayList<SearchResponse> searchResponses) {
//        arrayList.addAll(searchResponses);
//        adapter.notifyDataSetChanged();
//        adapter.notifyItemRangeChanged(0, arrayList.size());
//    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intent = new Intent(Pencarian.this, DashboardActivity.class);
                    startActivity(intent);
                    break;
                case R.id.navigation_pembelian:Intent intent1 = new Intent(Pencarian.this, Pembelian.class);
                    startActivity(intent1);
                    break;
                case R.id.navigation_search:

                case R.id.navigation_profile:
                    Intent intent2 = new Intent(Pencarian.this, Profil.class);
                    startActivity(intent2);
                    break;
            }return false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pencarian, menu);
        itemsearch = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(itemsearch);
        searchView.setOnQueryTextListener(this);
        MenuItemCompat.setOnActionExpandListener(itemsearch, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchData(query);
        return false;
    }

//
//    private void onError(Throwable throwable) {
//    }
    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
