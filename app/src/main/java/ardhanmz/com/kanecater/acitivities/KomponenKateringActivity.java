package ardhanmz.com.kanecater.acitivities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.KomponenModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.response.AnakResponse;
import ardhanmz.com.kanecater.response.Example;
import ardhanmz.com.kanecater.response.ResponseAVG;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class KomponenKateringActivity extends AppCompatActivity {
    @BindView(R.id.komponen_pilihnasi)
    ImageButton komponen_pilihnasi;
    @BindView(R.id.komponen_pilihlauk)
    ImageButton komponen_pilihlauk;
    @BindView(R.id.komponen_pilihsayur)
    ImageButton komponen_pilihsayur;
    @BindView(R.id.komponen_pilihlauk2)
    ImageButton komponen_pilihlauk2;
    @BindView(R.id.komponen_pilihdessert)
    ImageButton komponen_pilihdessert;
    @BindView(R.id.komp_pilihnasi)
    ImageView komp_pilihnasi;
    @BindView(R.id.komp_pilihlauk)
    ImageView komp_pilihlauk;
    @BindView(R.id.komp_pilihsayur)
    ImageView komp_pilihsayur;
    @BindView(R.id.komp_pilihlauk2)
    ImageView komp_pilihlauk2;
    @BindView(R.id.komp_pilihdessert)
    ImageView komp_pilihdessert;
    @BindView(R.id.besar_harga)
    TextView besar_harga;
    @BindView(R.id.besar_kalori)
    TextView besar_kalori;
    @BindView(R.id.besar_maxkalori)
    TextView besar_maxkalori;
    @BindView(R.id.komp_selanjutnya)
    Button komp_selanjutnya;

    private Context mContext;
    private Boolean nasiclicked = false, lauksatuclicked = false, sayurclicked = false, laukduaclicked = false, dessertclicked = false;
    private double nasiAvg, lauksatuAVG, laukduaAVG, sayurAVG, dessertAVG = 0;
    private CompositeSubscription compositeSubscription;
    private int idpaket, jumlahharga;
    private String mDateStart;
    private String mDateEnd;
    private String wkwk;
    private String list;
    private String idmasakan;
    private String idorder;
    private String nama_anak;
    private String idanak;
    private int harga_nasi = 0, harga_lauk1 = 0, harga_lauk2 = 0, harga_sayur = 0, harga_buah = 0;
    private Integer tinggi_badan;
    private Integer berat_badan;
    private Double kalori_nasi, kalori_lauk1, kalori_lauk2, kalori_sayur;
    ViewDialog viewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komponen_katering);
        mContext = getApplicationContext();
        viewDialog = new ViewDialog(this);
        ButterKnife.bind(this);
        compositeSubscription = new CompositeSubscription();
        idpaket = getIntent().getExtras().getInt("idpaket",0);
        mDateStart = getIntent().getStringExtra("start_date");
        mDateEnd = getIntent().getStringExtra("end_date");
        nama_anak = getIntent().getExtras().getString("nama_anak");
        idorder = getIntent().getExtras().getString("idorder");
        idanak = getIntent().getStringExtra("idanak");
        loadDataAnak();
        onClick();
//        calculateAVG();
    }

    private void calculateAVG() {
        double total = nasiAvg + lauksatuAVG + laukduaAVG + sayurAVG + dessertAVG ;
        double avg = total / 5.0 ;
        besar_kalori.setText(String.valueOf(avg));
    }

    private void onClick() {
        komponen_pilihnasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nasiclicked){
                    nasiclicked = false;
                    komponen_pilihnasi.setImageResource(R.drawable.ic_nasi_deselected);
                    komp_pilihnasi.setVisibility(View.GONE);
                    nasiAvg = 0;
                    harga_nasi = 0;
                    double total = nasiAvg + lauksatuAVG + laukduaAVG + sayurAVG + dessertAVG ;
                    besar_kalori.setText(String.valueOf(Math.round(total)));
                    jumlahharga = harga_nasi + harga_lauk1 + harga_lauk2 + harga_sayur + harga_buah;
                    besar_harga.setText(String.valueOf(jumlahharga));
                } else {
                    nasiAvg = kalori_nasi;
                    double total = nasiAvg + lauksatuAVG + laukduaAVG + sayurAVG + dessertAVG ;
                    besar_kalori.setText(String.valueOf(Math.round(total)));
                    harga_nasi = 50000;
                    jumlahharga = harga_nasi + harga_lauk1 + harga_lauk2 + harga_sayur + harga_buah;
                    besar_harga.setText(String.valueOf(jumlahharga));
                    nasiclicked = true;
                    komponen_pilihnasi.setImageResource(R.drawable.ic_nasi_selected);
                    komp_pilihnasi.setVisibility(View.VISIBLE);
                }
            }
        });
        komponen_pilihlauk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lauksatuclicked){
                    lauksatuclicked = false;
                    komponen_pilihlauk.setImageResource(R.drawable.ic_lauk_deselected);
                    komp_pilihlauk.setVisibility(View.GONE);
                    lauksatuAVG = 0;
                    harga_lauk1 = 0;
                    double total = nasiAvg + lauksatuAVG + laukduaAVG + sayurAVG + dessertAVG ;
                    besar_kalori.setText(String.valueOf(Math.round(total)));
                    jumlahharga = harga_nasi + harga_lauk1 + harga_lauk2 + harga_sayur + harga_buah;
                    besar_harga.setText(String.valueOf(jumlahharga));
                } else {
                    lauksatuAVG = kalori_lauk1;
                    double total = nasiAvg + lauksatuAVG + laukduaAVG + sayurAVG + dessertAVG ;
                    besar_kalori.setText(String.valueOf(Math.round(total)));
                    harga_lauk1 = 50000;
                    jumlahharga = harga_nasi + harga_lauk1 + harga_lauk2 + harga_sayur + harga_buah;
                    besar_harga.setText(String.valueOf(jumlahharga));
                    lauksatuclicked = true;
                    komponen_pilihlauk.setImageResource(R.drawable.ic_lauk_selected);
                    komp_pilihlauk.setVisibility(View.VISIBLE);
                }
            }
        });
        komponen_pilihsayur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sayurclicked){
                    sayurclicked = false;
                    komponen_pilihsayur.setImageResource(R.drawable.ic_sayur_deselected);
                    komp_pilihsayur.setVisibility(View.GONE);
                    harga_sayur = 0;
                    sayurAVG = 0;
                    double total = nasiAvg + lauksatuAVG + laukduaAVG + sayurAVG + dessertAVG ;
                    besar_kalori.setText(String.valueOf(Math.round(total)));
                    jumlahharga = harga_nasi + harga_lauk1 + harga_lauk2 + harga_sayur + harga_buah;
                    besar_harga.setText(String.valueOf(jumlahharga));
                } else {
                    sayurAVG = kalori_sayur;
                    harga_sayur = 50000;
                    double total = nasiAvg + lauksatuAVG + laukduaAVG + sayurAVG + dessertAVG ;
                    besar_kalori.setText(String.valueOf(Math.round(total)));
                    jumlahharga = harga_nasi + harga_lauk1 + harga_lauk2 + harga_sayur + harga_buah;
                    besar_harga.setText(String.valueOf(jumlahharga));
                    sayurclicked = true;
                    komponen_pilihsayur.setImageResource(R.drawable.ic_sayur_selected);
                    komp_pilihsayur.setVisibility(View.VISIBLE);
                    getSayurAVG(idpaket);
                }
            }
        });
        komponen_pilihlauk2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (laukduaclicked){
                    laukduaclicked = false;
                    komponen_pilihlauk2.setImageResource(R.drawable.ic_lauk_deselected);
                    komp_pilihlauk2.setVisibility(View.GONE);
                    harga_lauk2 = 0;
                    laukduaAVG = 0;
                    double total = nasiAvg + lauksatuAVG + laukduaAVG + sayurAVG + dessertAVG ;
                    besar_kalori.setText(String.valueOf(Math.round(total)));
                    jumlahharga = harga_nasi + harga_lauk1 + harga_lauk2 + harga_sayur + harga_buah;
                    besar_harga.setText(String.valueOf(jumlahharga));
                } else {
                    laukduaAVG = kalori_lauk2;
                    harga_lauk2 = 50000;
                    double total = nasiAvg + lauksatuAVG + laukduaAVG + sayurAVG + dessertAVG ;
                    besar_kalori.setText(String.valueOf(Math.round(total)));
                    jumlahharga = harga_nasi + harga_lauk1 + harga_lauk2 + harga_sayur + harga_buah;
                    besar_harga.setText(String.valueOf(jumlahharga));
                    laukduaclicked = true;
                    komponen_pilihlauk2.setImageResource(R.drawable.ic_lauk_selected);
                    komp_pilihlauk2.setVisibility(View.VISIBLE);
                }
            }
        });
        komponen_pilihdessert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dessertclicked){
                    dessertclicked = false;
                    komponen_pilihdessert.setImageResource(R.drawable.ic_dessert_deselected);
                    komp_pilihdessert.setVisibility(View.GONE);
                } else {
                    getBuahAVG(idpaket);
                    dessertclicked = true;
                    komponen_pilihdessert.setImageResource(R.drawable.ic_dessert_selected);
                    komp_pilihdessert.setVisibility(View.VISIBLE);
                }
            }
        });
        komp_selanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                Intent i = new Intent(mContext, PemesananCustomActivity.class);
                bundle.putString("start_date", mDateStart);
                bundle.putString("end_date", mDateEnd);
                bundle.putString("idanak", idanak);
                bundle.putString("nama_anak", nama_anak);
                bundle.putString("idorder", idorder);
                bundle.putInt("idpaket", idpaket);
                bundle.putInt("paket",0);
                bundle.putInt("harga",jumlahharga);
                if (nasiclicked && lauksatuclicked){
                    if (laukduaclicked){
                        if (sayurclicked){
                            if (dessertclicked){
                                bundle.putInt("order",5);
                            }else{
                                bundle.putInt("order", 4);
                            }
                        }else {
                            bundle.putInt("order", 3);
                        }
                    }else {
                        bundle.putInt("order", 2);
                    }
                }
                i.putExtras(bundle);
                startActivity(i);
            }
        });
    }
    private void getNasiAvg(int idpaket){
        compositeSubscription.add(NetworkUtil.getRetrofit().getNasiAVG(idpaket)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }
    private void getLauksatuAVG(int idpaket){
        compositeSubscription.add(NetworkUtil.getRetrofit().getLaukSatuAVG(idpaket)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }
    private void getLaukDuaAVG(int idpaket){
        compositeSubscription.add(NetworkUtil.getRetrofit().getLaukDuaAVG(idpaket)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }
    private void getSayurAVG(int idpaket){
        compositeSubscription.add(NetworkUtil.getRetrofit().getSayurAVG(idpaket)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }
    private void getBuahAVG(int idpaket){
        compositeSubscription.add(NetworkUtil.getRetrofit().getBuahAvg(idpaket)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void onError(Throwable throwable) {
        //do Nothing
    }

    private void handleResponse(ResponseAVG responseAVG) {
        if (responseAVG!=null){
            Integer jumlah = Integer.parseInt(responseAVG.getJumlah());
            if (jumlah==1){
                this.nasiAvg = Double.parseDouble(responseAVG.getKalori());
                compositeSubscription.clear();
            }
            if (jumlah==8){
                this.lauksatuAVG = Double.parseDouble(responseAVG.getKalori());
                compositeSubscription.clear();
            }
            if (jumlah==32){
                this.laukduaAVG = Double.parseDouble(responseAVG.getKalori());
                compositeSubscription.clear();
            }
            if (jumlah==27){
                this.sayurAVG = Double.parseDouble(responseAVG.getKalori());
                compositeSubscription.clear();
            }
        }
        compositeSubscription.clear();
    }
    private void loadDataAnak() {
        viewDialog.showDialog();
        KomponenModel komponenModel = new KomponenModel();
        komponenModel.setIdpaket(idpaket);
        compositeSubscription.add(NetworkUtil.getRetrofit().getAnak(idanak, komponenModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void handleResponse(Example response) {
        if (response!=null){
            tinggi_badan = response.getAnakresponse().getTinggiBadan();
            berat_badan = response.getAnakresponse().getBeratBadan();
            kalori_nasi = Double.valueOf(response.getNasi().getKalori());
            kalori_lauk1= response.getLauksatu().getKalori();
            kalori_lauk2 = response.getLaukdua().getKalori();
            kalori_sayur = response.getSayur().getKalori();
            hitungGizi();
        } else {
            Toasty.warning(getBaseContext(), "Error Memuat Data Anak", Toasty.LENGTH_SHORT, true).show();
        }
        compositeSubscription.clear();
    }
    private void hitungGizi() {
        viewDialog.hideDialog();
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        double t = tinggi_badan;
        double b = berat_badan;
        double t_m = t / 100;
        double pow = Math.pow(t_m, 2);
        double IMT = b / pow;
        int BMR = berat_badan * 24;
        double SDA = 0.1 * BMR;
        Double total = 1.7*(BMR+SDA);
        double akhir = total / 3;
        String hasil_akhir = decimalFormat.format(akhir);
        int max_calori = (int) Math.rint(akhir);
        String h = Double.toString(total);
        String wkwk = Double.toString(max_calori);
        besar_maxkalori.setText(String.valueOf(wkwk));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
