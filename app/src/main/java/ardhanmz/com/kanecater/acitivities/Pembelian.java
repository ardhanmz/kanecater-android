package ardhanmz.com.kanecater.acitivities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import ardhanmz.com.kanecater.utils.BottomNavigationViewHelper;
import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.utils.ViewPagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class Pembelian extends AppCompatActivity {
    @BindView(R.id.nav_bar_pembelian)
    BottomNavigationView nav_pembelian;
    @BindView(R.id.container) ViewPager mViewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;

    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembelian);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        BottomNavigationViewHelper.removeShiftMode(nav_pembelian);
        nav_pembelian.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Menu nav_menu = nav_pembelian.getMenu();
        MenuItem menuItem= nav_menu.getItem(1);
        menuItem.setChecked(true);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);



    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intent = new Intent(Pembelian.this, DashboardActivity.class);
                    startActivity(intent);
                    break;
                case R.id.navigation_pembelian:
                case R.id.navigation_search:
                    Intent intent1 = new Intent(Pembelian.this, Pencarian.class);
                    startActivity(intent1);
                    break;
                case R.id.navigation_profile:
                    Intent intent2 = new Intent(Pembelian.this, Profil.class);
                    startActivity(intent2);
                    break;
            }return false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pembelian, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
