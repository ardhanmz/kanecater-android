package ardhanmz.com.kanecater.acitivities;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.fragment.DetailPaketFragment;
import ardhanmz.com.kanecater.fragment.PaketLanggananFragment;

public class PaketLanggananActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paket_langganan);
        String bundle = getIntent().getStringExtra("args");
        String id = getIntent().getStringExtra("id");
        String query = getIntent().getStringExtra("query");
        if (bundle.equals("paket_langganan")){
            Bundle bundle1 = new Bundle();
            bundle1.putString("query", query);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            PaketLanggananFragment paketLanggananFragment = new PaketLanggananFragment();
            paketLanggananFragment.setArguments(bundle1);
            fragmentTransaction.replace(R.id.frame,paketLanggananFragment);
            fragmentTransaction.commit();
        } else if (bundle.equals("detail")){
            Bundle bundle1 = new Bundle();
            String hha = getIntent().getExtras().getString("id","");
            bundle1.putString("id",hha);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            PaketLanggananFragment paketLanggananFragment = new PaketLanggananFragment();
            DetailPaketFragment detailPaketFragment = new DetailPaketFragment();
            detailPaketFragment.setArguments(bundle1);
            fragmentTransaction.replace(R.id.frame,detailPaketFragment);
            fragmentTransaction.commit();
        }
    }
    //    @BindView(R.id.rv_list_langganan)
//    RecyclerView rv_langganan;
//    @BindView(R.id.text_rvlangganan_kosong)
//    TextView rv_kosong;
//    private CompositeSubscription compositeSubscription;
//    private SharedPreferences sharedPreferences;
//    ArrayList<DashboardPaketModel> arrayList;
//    ListLanggananAdapter adapter;
//    RecyclerELEAdapter RAdapter;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_paket_langganan);
//        ButterKnife.bind(this);
//        arrayList = new ArrayList<DashboardPaketModel>();
//        compositeSubscription = new CompositeSubscription();
//        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
//        rv_langganan.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        View loadingView = getLayoutInflater().inflate(R.layout.layout_rvloading, rv_langganan, false);
//        View errorView = getLayoutInflater().inflate(R.layout.layout_rv_error, rv_langganan, false);
//        View emptyView = getLayoutInflater().inflate(R.layout.layout_rvempty, rv_langganan, false);
//        adapter = new ListLanggananAdapter(this, arrayList);
//        RAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
//        rv_langganan.setHasFixedSize(true);
//        rv_langganan.setAdapter(RAdapter);
//        loadComponent();
//    }
//    private void loadComponent(){
//        RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_LOADING);
//        loadDataLangganan();
//    }
//
//    private void loadDataLangganan() {
//
//        compositeSubscription.add(NetworkUtil.getRetrofit().getAllPaket()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(this::handleResponse,this::onError));
//    }
//
//    private void onError(Throwable throwable) {
//
//    }
//
//    private void handleResponse(ArrayList<DashboardPaketModel> dashboardPaketModels) {
//        if (dashboardPaketModels!=null){
//            arrayList.clear();
//            arrayList.addAll(dashboardPaketModels);
//            RAdapter.notifyDataSetChanged();
//            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_NORMAL);
//        } else {
//            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_EMPTY);
////            rv_kosong.setVisibility(View.VISIBLE);
//        }
//    }
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        compositeSubscription.unsubscribe();
//    }

}
