package ardhanmz.com.kanecater.acitivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import java.security.MessageDigest;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.Pengguna;
import ardhanmz.com.kanecater.netutil.ResponseSignup;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.utils.Constants;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static ardhanmz.com.kanecater.utils.Validator.validateFields;

public class SignUpActivity extends AppCompatActivity {
    @BindView(R.id.et_uname_signup)
    EditText uname;
    @BindView(R.id.et_mail_signup) EditText mail;
    @BindView(R.id.et_password_signup) EditText pwd;
    @BindView(R.id.ti_uname_signup)
    TextInputLayout username_ti;
    @BindView(R.id.ti_email_signup) TextInputLayout email_ti;
    @BindView(R.id.ti_password_signup) TextInputLayout password_ti;
    @BindView(R.id.Signup_Btn)
    Button registernow;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private String username, password, email;
    ViewDialog viewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_activity);
        ButterKnife.bind(this);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        viewDialog = new ViewDialog(this);
        registernow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Register();
            }
        });
    }

    private void Register() {
        username = uname.getText().toString();
        password = pwd.getText().toString();
        email = mail.getText().toString();
        int err = 0;
        if (!validateFields(username)){
            err++;
            username_ti.setError("Username Tidak Boleh Kosong");
        }
        if (!validateFields(password)){
            err++;
            password_ti.setError("Password Tidak Boleh Kosong");
        }
        if (!validateFields(email)){
            err++;
            email_ti.setError("Email Tidak Boleh Kosong");
        }
        if (err == 0){
            Pengguna pengguna = new Pengguna();
            pengguna.setUsername(username);
            pengguna.setPassword(password);
            pengguna.setEmail(email);
            proses(pengguna);
            viewDialog.showDialog();
        }
    }

    private void proses(Pengguna pengguna) {
        compositeSubscription.add(NetworkUtil.getRetrofit().register(pengguna)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void handleResponse(ResponseSignup responseSignup) {
        viewDialog.hideDialog();
        boolean status = responseSignup.getSuccess();
        if (status){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(Constants.TEMP_UID, Integer.parseInt(responseSignup.getMessage()));
            editor.apply();
            Bundle bundle = new Bundle();
            bundle.putString("username", username);
            Intent intent = new Intent(this, PostSignUpActivty.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }else {
            Toasty.info(getApplicationContext(), responseSignup.getMessage(), Toasty.LENGTH_SHORT, true).show();
        }
    }

    private void onError(Throwable throwable) {

    }
    public static final String md5(final String toEncrypt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(toEncrypt.getBytes());
            final byte[] bytes = digest.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(String.format("%02X", bytes[i]));
            }
            return sb.toString().toLowerCase();
        } catch (Exception exc) {
            return ""; // Impossibru!
        }
    }

    private void setError(){
        username_ti.setError(null);
        password_ti.setError(null);
        email_ti.setError(null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
