package ardhanmz.com.kanecater.acitivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ardhanmz.com.kanecater.R;
import android.support.v4.app.FragmentTransaction;

import ardhanmz.com.kanecater.fragment.PembayaranFragment;
import ardhanmz.com.kanecater.fragment.PilihTanggalCustomFragment;

public class PemesananCustomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemesanan_custom);
        String anak = getIntent().getExtras().getString("idanak", "");
        String nama_anak = getIntent().getExtras().getString("nama_anak", "");
        String startDate = getIntent().getExtras().getString("start_date","");
        String endDate = getIntent().getExtras().getString("end_date", "");
        String idOrder = getIntent().getExtras().getString("idorder", "");
        Integer val = getIntent().getExtras().getInt("paket",0);
        boolean lihat_order = getIntent().getExtras().getBoolean("lihat_order", false);
        int idpaket = getIntent().getIntExtra("idpaket", 0);
        int jumlah_order = getIntent().getIntExtra("order", 0);
        int harga = getIntent().getIntExtra("harga",0);
         if (val==0){
            Bundle bundle = new Bundle();
            bundle.putString("start_date", startDate);
            bundle.putString("end_date", endDate);
            bundle.putString("nama_anak", nama_anak);
            bundle.putString("idanak", anak);
            bundle.putString("idorder", idOrder);
            bundle.putInt("idpaket", idpaket);
            bundle.putInt("order", jumlah_order);
            bundle.putBoolean("lihat_order", lihat_order);
            bundle.putInt("harga",harga);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            PilihTanggalCustomFragment pilihTanggalCustomFragment = new PilihTanggalCustomFragment();
            pilihTanggalCustomFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.fragframe, pilihTanggalCustomFragment);
            fragmentTransaction.commit();
        }
        else if (val>0){
            Bundle bundle1 = new Bundle();
            bundle1.putString("idorder", idOrder);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            PembayaranFragment pembayaranFragment = new PembayaranFragment();
            pembayaranFragment.setArguments(bundle1);
            fragmentTransaction.add(R.id.fragframe, pembayaranFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0){
            super.onBackPressed();
        }else {
            getFragmentManager().popBackStack();
        }
    }
}
