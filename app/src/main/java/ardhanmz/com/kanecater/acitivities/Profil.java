package ardhanmz.com.kanecater.acitivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ardhanmz.com.kanecater.response.AnakResponse;
import ardhanmz.com.kanecater.response.DataAnakResponse;
import ardhanmz.com.kanecater.utils.BottomNavigationViewHelper;
import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.adapter.DataAnakAdapter;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.utils.Constants;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

public class Profil extends AppCompatActivity {
    @BindView(R.id.img_ortu)
    ImageView imgortu;
    @BindView(R.id.nama_ortu)
    TextView namaortu;
    @BindView(R.id.recycler_data_anak)
    RecyclerView data_anak;
    @BindView(R.id.tambah_anak) TextView tambahanak;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    ArrayList<AnakResponse> anaklist;
    private DataAnakAdapter adapter;
    RecyclerELEAdapter RAdapter;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intent2 = new Intent(Profil.this, DashboardActivity.class);
                    startActivity(intent2);
                    break;
                case R.id.navigation_pembelian:
                    Intent intent = new Intent(Profil.this, Pembelian.class);
                    startActivity(intent);
                    break;
                case R.id.navigation_search:
                    Intent intent1 = new Intent(Profil.this, Pencarian.class);
                    startActivity(intent1);
                    break;
                case R.id.navigation_profile:

            }
            return false;
        }
    };
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        ButterKnife.bind(this);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.nav_bar_profil);
        BottomNavigationViewHelper.removeShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Menu nav_menu = navigation.getMenu();
        MenuItem menuItem = nav_menu.getItem(3);
        menuItem.setChecked(true);
        anaklist = new ArrayList<>();
        adapter = new DataAnakAdapter(this, anaklist);
        data_anak.setHasFixedSize(true);
        data_anak.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        View loadingView = getLayoutInflater().inflate(R.layout.layout_rvloading, data_anak, false);
        View errorView = getLayoutInflater().inflate(R.layout.layout_rv_error, data_anak, false);
        View emptyView = getLayoutInflater().inflate(R.layout.layout_rvempty, data_anak, false);
        RAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
        data_anak.setAdapter(RAdapter);
        getProfile();
        loadDataAnak();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Autentikasi..");
        tambahanak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambahAnak();
            }
        });
        data_anak.addOnItemTouchListener(new RecyclerItemClickListener(getBaseContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                    Intent intent = new Intent(getBaseContext(), ManajemenAnakActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("args", "lihat_anak");
                    bundle.putInt("idanak", anaklist.get(position).getIdanak());
                    intent.putExtras(bundle);
                    startActivity(intent);
            }
        }));
    }
    private void getProfile() {
        String ortu = sharedPreferences.getString(Constants.USERNAME,"");
        namaortu.setText(ortu);
//        Glide.with(getApplicationContext()).load(pengguna.getAvatar()).into(imgortu);
    }

    private void loadDataAnak() {
//        RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_LOADING);
        Integer iduser = sharedPreferences.getInt(Constants.IDPENGGUNA, 0);
        String id = Integer.toString(iduser);
        compositeSubscription.add(NetworkUtil.getRetrofit().getAnakByUser(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }


    private void handleResponse(ArrayList<AnakResponse> arrayList) {
        if(arrayList!=null){
            if (mProgressDialog.isShowing()){
                mProgressDialog.dismiss();
            }
            anaklist.clear();
            anaklist.addAll(arrayList);
            RAdapter.notifyDataSetChanged();
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_NORMAL);
        }
        else {
            if (mProgressDialog.isShowing()){
                mProgressDialog.dismiss();
            }
            RAdapter.notifyDataSetChanged();
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_EMPTY);
        }
    }
    private void onError(Throwable throwable) {
    }

    private void tambahAnak() {
        Intent intent = new Intent(this, ManajemenAnakActivity.class);
        Bundle bundle = new Bundle();
//        bundle.getString("args",);
        intent.putExtra("args", "tambah_anak");
        startActivity(intent);
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
