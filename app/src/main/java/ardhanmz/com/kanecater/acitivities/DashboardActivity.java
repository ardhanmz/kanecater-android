package ardhanmz.com.kanecater.acitivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.adapter.DashboardPaketAdapter;
import ardhanmz.com.kanecater.model.DashboardPaketModel;
import ardhanmz.com.kanecater.model.KategoriPaketDBModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.utils.BottomNavigationViewHelper;
import ardhanmz.com.kanecater.utils.Constants;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class DashboardActivity extends AppCompatActivity {
   /* ArrayList<KategoriPaketDBModel> allsamplelist;
    ArrayList<DashboardPaketModel> list_paket;

    @BindView(R.id.dashboard_horizontal_recycler_view)
    RecyclerView kategori_recyclerview;
    @BindView(R.id.paketCustom)
    ImageButton paket_custom;
    @BindView(R.id.paketLangganan)
    ImageButton paket_langganan;
    @BindView(R.id.textusername) TextView usertext;
//    @BindView(R.id.recycler_view_list) RecyclerView paket_rv;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                case R.id.navigation_pembelian:
                    Intent intent = new Intent(DashboardActivity.this, Pembelian.class);
                    startActivity(intent);
                    break;
                case R.id.navigation_search:
                    Intent intent1 = new Intent(DashboardActivity.this, Pencarian.class);
                    startActivity(intent1);
                    break;
                case R.id.navigation_profile:
                    Intent intent2 = new Intent(DashboardActivity.this, Profil.class);
                    startActivity(intent2);
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        allsamplelist = new ArrayList<KategoriPaketDBModel>();
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.removeShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        imageButton();
//        createSection();
        kategori_recyclerview.setHasFixedSize(true);
        DashboardPaketAdapter adapter = new DashboardPaketAdapter(this, allsamplelist);
        kategori_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        kategori_recyclerview.setAdapter(adapter);

        loadPaket();

    }

    private void imageButton() {
        String uname = sharedPreferences.getString(Constants.USERNAME,"");
        usertext.setText(uname);
        paket_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(DashboardActivity.this, PaketCustomActivity.class);
                startActivity(in);
            }
        });
        paket_langganan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                Intent intent = new Intent(DashboardActivity.this, PaketLanggananActivity.class);
                bundle.putString("args", "paket_langganan");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void loadPaket() {
        compositeSubscription.add(NetworkUtil.getRetrofit().getAllPaket()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void onError(Throwable throwable) {
    }

    private void handleResponse(ArrayList<DashboardPaketModel> response) {
        KategoriPaketDBModel dm = new KategoriPaketDBModel();

        dm.setHeaderTitle("Section ");
        dm.setAllItemInSection(response);
        ArrayList<DashboardPaketModel> singleItem = new ArrayList<DashboardPaketModel>();
        singleItem.clear();
        singleItem.addAll(response);
        allsamplelist.add(dm);
//        list_paket.clear();
//        list_paket.add(response);

    }

    private void createSection() {
        for (int i = 1; i <= 5; i++) {

            KategoriPaketDBModel dm = new KategoriPaketDBModel();

            dm.setHeaderTitle("Section " + i);

            ArrayList<DashboardPaketModel> singleItem = new ArrayList<DashboardPaketModel>();
            for (int j = 0; j <= 5; j++) {
                singleItem.add(new DashboardPaketModel("Item " + j, "URL " + j));
            }

            dm.setAllItemInSection(singleItem);

            allsamplelist.add(dm);

        }

    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }*/
}
