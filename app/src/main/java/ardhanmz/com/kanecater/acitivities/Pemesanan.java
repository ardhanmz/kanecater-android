package ardhanmz.com.kanecater.acitivities;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.response.AnakResponse;
import ardhanmz.com.kanecater.response.DataAnakResponse;
import ardhanmz.com.kanecater.model.OrderRequest;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.netutil.ResponseSignup;
import ardhanmz.com.kanecater.response.SummaryOrderResponse;
import ardhanmz.com.kanecater.utils.Constants;
import ardhanmz.com.kanecater.utils.SublimePickerFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class Pemesanan extends AppCompatActivity {
    @BindView(R.id.spinner_data_anak)
    Spinner spinner_data_anak;
    @BindView(R.id.tanggal_mulai)
    TextView tanggal_mulai;
    @BindView(R.id.tanggal_selesai)
    TextView tanggal_selesai;
    @BindView(R.id.calendar_pemesanan)
    Button kalender_pemesanan;
    @BindView(R.id.next_button)
    Button selanjutnya;
    @BindView(R.id.pilih_tanggal) TextView pilih_tanggal;
    @BindView(R.id.namasekolah_pemesanan) TextView nama_sekolah;
    @BindView(R.id.alamatsekolah_pemesanan) TextView alamat_sekolah;
    @BindView(R.id.textView3) TextView nama_an;
    @BindView(R.id.img_bank)
    ImageView img_bank;
    @BindView(R.id.tv_metodepemb) TextView metode_pembayaran;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    String mDateStart;
    String mDateEnd;

    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private ArrayList<AnakResponse> anaklist;
    Unbinder unbinder;
    private Integer idanak, idpaket;
    private boolean langganan;
    private String list;
    private String wkwk;
    String namasekolah_, alamat_;
    private String jam;
    private Integer n_anak;
    private Context context;


    public Pemesanan pemesanan(String namasekolah, String alamat){
        nama_sekolah.setText(namasekolah);
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemesanan);
        ButterKnife.bind(this);
        unbinder =ButterKnife.bind(this);
        idpaket = getIntent().getIntExtra("idpaket",0);
        anaklist = new ArrayList<>();
        langganan = getIntent().getBooleanExtra("langganan", false);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String idorder = getIntent().getStringExtra("idorder");
        context = getApplicationContext();
        if (idorder!=null){
            nama_an.setVisibility(View.VISIBLE);
            loadSummaryOrder(idorder);
            toolbar.setTitle("Ringkasan Order");
        }else {
            loadSpinner();
        }
        kalender_pemesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDateRangePicker();
            }
        });
        selanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (n_anak!=null){
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else {
                    if(spinner_data_anak != null){
                        int idpengguna = sharedPreferences.getInt(Constants.IDPENGGUNA, 0);
                        OrderRequest orderRequest = new OrderRequest();
                        orderRequest.setFk_idanak(idanak);
                        orderRequest.setFk_idpengguna(idpengguna);
                        orderRequest.setFk_idpaket(idpaket);
                        orderRequest.setAlamat_order(alamat_sekolah.getText().toString());
                        orderRequest.setLangganan_awal(mDateStart);
                        orderRequest.setLangganan_akhir(mDateEnd);
                        orderRequest.setJam_order(jam);
                        sendDataPesanan(orderRequest);
                    }else{
                        Toasty.error(getApplicationContext(), "Tidak Ada Data Anak, Harap Tambahkan Dahulu", Toasty.LENGTH_LONG, true).show();
                        Pemesanan.super.onBackPressed();
                    }

                }
            }
        });
        pilih_tanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(Pemesanan.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        jam = selectedHour + ":" + selectedMinute;
                        pilih_tanggal.setText(jam);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Pilih Tanggal Mulai Sampai Selesai");
                mTimePicker.show();
            }
        });
    }

    private void loadSummaryOrder(String idorder) {
        compositeSubscription.add(NetworkUtil.getRetrofit().getSummaryOrder(idorder)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void handleResponse(SummaryOrderResponse summaryOrderResponse) {
        if (summaryOrderResponse!=null){
            tanggal_mulai.setText(summaryOrderResponse.getLanggananAwal());
            tanggal_selesai.setText(summaryOrderResponse.getLanggananAkhir());
            n_anak = summaryOrderResponse.getFkIdanak();
            nama_sekolah.setVisibility(View.GONE);
            alamat_sekolah.setText(summaryOrderResponse.getAlamatOrder());
            pilih_tanggal.setText(summaryOrderResponse.jamOrder);
            selanjutnya.setText("Selesai");
            img_bank.setVisibility(View.VISIBLE);
            metode_pembayaran.setVisibility(View.VISIBLE);
            String metode = getIntent().getStringExtra("Metode");
            if (metode.equalsIgnoreCase("Bank BCA")){
                img_bank.setBackgroundResource(R.drawable.ic_bca);
                metode_pembayaran.setText(metode);
            } else if (metode.equalsIgnoreCase("Bank Bri")){
                img_bank.setBackgroundResource(R.drawable.bri);
                metode_pembayaran.setText(metode);
            }else if (metode.equalsIgnoreCase("Alfamart")){
                img_bank.setBackgroundResource(R.drawable.ic_alfamart);
                metode_pembayaran.setText(metode);
            }else if (metode.equalsIgnoreCase("Indomaret")){
                img_bank.setBackgroundResource(R.drawable.ic_indomaret);
                metode_pembayaran.setText(metode);
            }
            loadData();
        }
    }


    private void openDateRangePicker() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {
                Toast.makeText(Pemesanan.this, "User cancel",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDateTimeRecurrenceSet(final SelectedDate selectedDate, int hourOfDay, int minute,
                                                SublimeRecurrencePicker.RecurrenceOption recurrenceOption,
                                                String recurrenceRule) {

                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                mDateStart = formatDate.format(selectedDate.getStartDate().getTime());
                mDateEnd = formatDate.format(selectedDate.getEndDate().getTime());
                // set date start ke textview
                tanggal_mulai.setText(mDateStart);
                // set date end ke textview
                tanggal_selesai.setText(mDateEnd);
            }
        });

        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(true);
        options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getSupportFragmentManager(), "SUBLIME_PICKER");
    }


    private void loadSpinner() {

        loadData();
        spinner_data_anak.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                list = adapterView.getItemAtPosition(i).toString();
                if (anaklist.get(i).getNamaAnak().equals(list)){
                    idanak = anaklist.get(i).getIdanak();
                    namasekolah_ = anaklist.get(i).getAsal_sekolah();
                    alamat_ = anaklist.get(i).getAlamat_sekolah();
                    nama_sekolah.setText(namasekolah_);
                    alamat_sekolah.setText(alamat_);
                    if (langganan==true){
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        double t = anaklist.get(i).getTinggiBadan();
                        double b = anaklist.get(i).getBeratBadan();
                        double t_m = t / 100;
                        double pow = Math.pow(t_m, 2);
                        double IMT = b / pow;
                        int BMR = anaklist.get(i).getBeratBadan() * 24;
                        double SDA = 0.1 * BMR;
                        Double total = 1.7*(BMR+SDA);
                        double akhir = total / 3;
                        String hasil_akhir = decimalFormat.format(akhir);
                        int max_calori = (int) Math.rint(akhir);
//                        String h = Double.toString(total);
//                        String wkwk = Double.toString(max_calori);
//                        max_kalori.setText(wkwk);
                    }
                }
                wkwk = Integer.toString(idanak);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void loadData() {
        Integer iduser = sharedPreferences.getInt(Constants.IDPENGGUNA, 0);
        String id = Integer.toString(iduser);
        compositeSubscription.add(NetworkUtil.getRetrofit().getAnakByUser(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void onError(Throwable throwable) {
    }

    private void handleResponse(ArrayList<AnakResponse> dataAnakResponses) {
        anaklist.clear();
        anaklist.addAll(dataAnakResponses);
        String hoho = null;
        if (n_anak!=null){
            for (int i = 0;i<=dataAnakResponses.size(); i++){
                int id = dataAnakResponses.get(i).getIdanak();
                if (id==n_anak){
                    hoho = dataAnakResponses.get(i).getNamaAnak();
                    break;
                }
            }
            spinner_data_anak.setVisibility(View.GONE);
            nama_an.setText(hoho);
            kalender_pemesanan.setEnabled(false);
            kalender_pemesanan.setClickable(false);
            pilih_tanggal.setEnabled(false);
            pilih_tanggal.setClickable(false);
        }else {
            List<String> nama_anak = new ArrayList<String>();
            for (int i = 0; i < dataAnakResponses.size(); i++){
                nama_anak.add(dataAnakResponses.get(i).getNamaAnak());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),
                    android.R.layout.simple_spinner_item, nama_anak);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_data_anak.setAdapter(adapter);
        }
    }
    private void sendDataPesanan(OrderRequest orderRequest) {
        compositeSubscription.add(NetworkUtil.getRetrofit().addOrder(orderRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }
    private void handleResponse(ResponseSignup responseSignup) {
        if (responseSignup.getSuccess()){
            String idorder = responseSignup.getData();
            String idmasakan= getIntent().getExtras().getString("idmasakan","");
            if (langganan==true){
                Intent i = new Intent(Pemesanan.this, PemesananCustomActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("idorder", idorder);
                bundle.putInt("idpaket", idpaket);
                bundle.putInt("paket", 1);
                i.putExtras(bundle);
                startActivity(i);
            }else {
                Intent i = new Intent(Pemesanan.this, KomponenKateringActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("start_date", mDateStart);
                bundle.putString("end_date", mDateEnd);
                bundle.putString("idanak", wkwk);
                bundle.putInt("idpaket", idpaket);
                bundle.putString("nama_anak", list);
                bundle.putString("idorder", idorder);
                i.putExtras(bundle);
                startActivity(i);
            }
        } else {
            Toast.makeText(getBaseContext(), responseSignup.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        compositeSubscription.unsubscribe();
    }
}
