package ardhanmz.com.kanecater.acitivities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import org.w3c.dom.Text;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.app.Config;
import ardhanmz.com.kanecater.fragment.BaseFragment;
import ardhanmz.com.kanecater.fragment.TabDashboardFragment;
import ardhanmz.com.kanecater.fragment.TabOrderFragment;
import ardhanmz.com.kanecater.fragment.TabProfileFragment;
import ardhanmz.com.kanecater.fragment.TabSearchFragment;
import ardhanmz.com.kanecater.model.PushFirebaseIDModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.netutil.ResponseLogin;
import ardhanmz.com.kanecater.netutil.ResponseSignup;
import ardhanmz.com.kanecater.utils.Constants;
import ardhanmz.com.kanecater.utils.FragmentHistory;
import ardhanmz.com.kanecater.utils.NotificationUtils;
import ardhanmz.com.kanecater.utils.Utils;
import ardhanmz.com.kanecater.views.FragNavController;
import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class MainActivity extends BaseActivity implements BaseFragment.FragmentNavigation, FragNavController.TransactionListener, FragNavController.RootFragmentListener {
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;

    private int[] mTabIconsSelected = {
            R.drawable.tab_home,
            R.drawable.tab_pembelian,
            R.drawable.tab_search,
            R.drawable.tab_profil};


    @BindArray(R.array.tab_name)
    String[] TABS;
    @BindView(R.id.bottom_tab_layout)
    TabLayout bottomTabLayout;

    private FragNavController mNavController;
    private FragmentHistory fragmentHistory;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private String iduser;
    public static Integer pos=0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        initTab();
        fragmentHistory = new FragmentHistory();
        mNavController = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.content_frame)
                .transactionListener(this)
                .rootFragmentListener(this, TABS.length)
                .build();
        switchTab(0);

        bottomTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                fragmentHistory.push(tab.getPosition());
                switchTab(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                mNavController.clearStack();
                switchTab(tab.getPosition());
            }
        });
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    Log.d("FCM","Sucess Registration");
                    saveFirebaseRegId();
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };
        saveFirebaseRegId();
    }


    private void saveFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Integer userid = sharedPreferences.getInt(Constants.IDPENGGUNA,0);
        PushFirebaseIDModel pushFirebaseIDModel = new PushFirebaseIDModel();
        pushFirebaseIDModel.setIduser(String.valueOf(userid));
        pushFirebaseIDModel.setDevice_id(regId);
        compositeSubscription.add(NetworkUtil.getRetrofit().updateFirebaseID(pushFirebaseIDModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
        Log.e("FCM", "Firebase reg id: " + regId);

    }

    private void onError(Throwable throwable) {
        //do nothing
    }

    private void handleResponse(ResponseSignup responseSignup) {
        Log.d("FCM",responseSignup.getMessage());
    }

    private void initTab() {
        if (bottomTabLayout != null) {
            for (int i = 0; i < TABS.length; i++) {
                bottomTabLayout.addTab(bottomTabLayout.newTab());
                TabLayout.Tab tab = bottomTabLayout.getTabAt(i);
                if (tab != null)
                    tab.setCustomView(getTabView(i));
            }
        }
    }

    private View getTabView(int position) {
        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.tab_item_bottom, null);
        ImageView icon = (ImageView) view.findViewById(R.id.tab_icon);
        icon.setImageDrawable(Utils.setDrawableSelector(MainActivity.this, mTabIconsSelected[position], mTabIconsSelected[position]));
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    private void switchTab(int position) {
        mNavController.switchTab(position);
//        updateToolbarTitle(position);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!mNavController.isRootFragment()) {
            mNavController.popFragment();
        } else {
            if (fragmentHistory.isEmpty()) {
                new AlertDialog.Builder (this)
                        .setIcon (android.R.drawable.ic_dialog_alert)
                        .setTitle ("Keluar Aplikasi")
                        .setMessage ("Apa Anda Yakin?")
                        .setPositiveButton ("Ya", new DialogInterface.OnClickListener ()
                        {
                            @Override
                            public void onClick (DialogInterface dialog, int which) {
                                moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(1);
                            }
                        })
                        .setNegativeButton ("Tidak", null)
                        .show ();
            } else {
                if (fragmentHistory.getStackSize() > 1) {
                    int position = fragmentHistory.popPrevious();
                    switchTab(position);
                    updateTabSelection(position);
                } else {
                    switchTab(0);
                    updateTabSelection(0);
                    fragmentHistory.emptyStack();
                }
            }
        }
    }

    private void updateTabSelection(int currentTab){
        for (int i = 0; i <  TABS.length; i++) {
            TabLayout.Tab selectedTab = bottomTabLayout.getTabAt(i);
            if(currentTab != i) {
                selectedTab.getCustomView().setSelected(false);
            }else{
                selectedTab.getCustomView().setSelected(true);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mNavController != null) {
            mNavController.onSaveInstanceState(outState);
        }
    }

    @Override
    public void pushFragment(Fragment fragment) {
        if (mNavController != null) {
            mNavController.pushFragment(fragment);
        }
    }


    @Override
    public void onTabTransaction(Fragment fragment, int index) {
        // If we have a backstack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {
            updateToolbar();
        }
    }

    private void updateToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(!mNavController.isRootFragment());
        getSupportActionBar().setDisplayShowHomeEnabled(!mNavController.isRootFragment());
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }


    @Override
    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType transactionType) {
        //do fragmentty stuff. Maybe change title, I'm not going to tell you how to live your life
        // If we have a backstack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {
            updateToolbar();
        }
    }

    @Override
    public Fragment getRootFragment(int index) {
        switch (index) {
            case FragNavController.TAB1:
                return new TabDashboardFragment();
            case FragNavController.TAB2:
                return new TabOrderFragment();
            case FragNavController.TAB3:
                return new TabSearchFragment();
            case FragNavController.TAB4:
                return new TabProfileFragment();
        }
        throw new IllegalStateException("Need to send an index that we know");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
