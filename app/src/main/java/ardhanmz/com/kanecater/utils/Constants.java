package ardhanmz.com.kanecater.utils;

public class Constants {
    public static final String AUTH_URLS = "http://192.168.0.15:9890/api/";
    public static final String NET_URLS = "http://kanecater-node.herokuapp.com/api/";
    public static final String IDPENGGUNA = "idpengguna";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String IMGPENGGUNA = "image_profile";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String TEMP_UID = "temp_uid";
}
