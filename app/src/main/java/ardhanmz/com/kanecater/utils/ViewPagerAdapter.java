package ardhanmz.com.kanecater.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import ardhanmz.com.kanecater.fragment.OrderListFragment;
import ardhanmz.com.kanecater.fragment.ProgressListFragment;
import ardhanmz.com.kanecater.fragment.TransaksiListFragment;
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new OrderListFragment();
        }
        else if (position == 1) {
            fragment = new ProgressListFragment();
        }
        else if (position == 2) {
            fragment = new TransaksiListFragment();
        }
        return fragment;
    }
    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Riwayat";
        }
        else if (position == 1) {
            title = "Pembayaran";
        }
        else if (position == 2) {
            title = "Status";
        }
        return title;
    }
}

