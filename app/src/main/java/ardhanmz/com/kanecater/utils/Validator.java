package ardhanmz.com.kanecater.utils;

import android.text.TextUtils;
import android.util.Patterns;

import java.util.regex.Pattern;

public class Validator {
    public static boolean validateFields(String name){
        if (TextUtils.isEmpty(name)){
            return false;
        }else{
            return true;
        }
    }
    public static boolean validateEmail(String email){
        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            return false;
        } else {
            return true;
        }
    }
}
