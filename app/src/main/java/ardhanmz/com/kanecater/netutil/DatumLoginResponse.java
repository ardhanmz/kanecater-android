
package ardhanmz.com.kanecater.netutil;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumLoginResponse {

    @SerializedName("idpengguna")
    @Expose
    private Integer idpengguna;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;

    public Integer getIdpengguna() {
        return idpengguna;
    }

    public void setIdpengguna(Integer idpengguna) {
        this.idpengguna = idpengguna;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
