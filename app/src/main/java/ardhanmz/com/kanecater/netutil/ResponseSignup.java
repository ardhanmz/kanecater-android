package ardhanmz.com.kanecater.netutil;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseSignup {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    @Expose
    private String data;

    public String getData() {
        return data;
    }

    public Boolean getSuccess() {
        return success;
    }
    public String getMessage() {
        return message;
    }

}
