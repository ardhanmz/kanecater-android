package ardhanmz.com.kanecater.netutil;
import ardhanmz.com.kanecater.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

public class NetworkUtil {
    public static RetrofitInterface getRetrofit(){
        RxJavaCallAdapterFactory rxAdapter =
                RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

        return new Retrofit.Builder().baseUrl(Constants.NET_URLS)
                .addCallAdapterFactory(rxAdapter)
                .addConverterFactory(GsonConverterFactory.create()).build()
                .create(RetrofitInterface.class);
    }
    public RetrofitInterface getRetrofit(String token){

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(chain -> {
           Request original = chain.request();
           Request.Builder builder = original.newBuilder()
                   .addHeader("token", token)
                   .method(original.method(), original.body());
           return chain.proceed(builder.build());
        });

        RxJavaCallAdapterFactory rxAdapter =
                RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

        return new Retrofit.Builder().baseUrl("hhh").client(httpClient.build())
                .addCallAdapterFactory(rxAdapter)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(RetrofitInterface.class);
    }


}
