package ardhanmz.com.kanecater.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListOrderModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("fk_idanak")
    @Expose
    private Integer fkIdanak;
    @SerializedName("fk_idpengguna")
    @Expose
    private Integer fkIdpengguna;
    @SerializedName("fk_idpembayaran")
    @Expose
    private Integer fkIdpembayaran;
    @SerializedName("langganan_awal")
    @Expose
    private String langgananAwal;
    @SerializedName("langganan_akhir")
    @Expose
    private String langgananAkhir;
    @SerializedName("status_pembayaran")
    @Expose
    private String statusPembayaran;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFkIdanak() {
        return fkIdanak;
    }

    public void setFkIdanak(Integer fkIdanak) {
        this.fkIdanak = fkIdanak;
    }

    public Integer getFkIdpengguna() {
        return fkIdpengguna;
    }

    public void setFkIdpengguna(Integer fkIdpengguna) {
        this.fkIdpengguna = fkIdpengguna;
    }

    public Integer getFkIdpembayaran() {
        return fkIdpembayaran;
    }

    public void setFkIdpembayaran(Integer fkIdpembayaran) {
        this.fkIdpembayaran = fkIdpembayaran;
    }

    public String getLanggananAwal() {
        return langgananAwal;
    }

    public void setLanggananAwal(String langgananAwal) {
        this.langgananAwal = langgananAwal;
    }

    public String getLanggananAkhir() {
        return langgananAkhir;
    }

    public void setLanggananAkhir(String langgananAkhir) {
        this.langgananAkhir = langgananAkhir;
    }

    public String getStatusPembayaran() {
        return statusPembayaran;
    }

    public void setStatusPembayaran(String statusPembayaran) {
        this.statusPembayaran = statusPembayaran;
    }

}
