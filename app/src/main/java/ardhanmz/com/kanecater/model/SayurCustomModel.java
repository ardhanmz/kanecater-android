package ardhanmz.com.kanecater.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SayurCustomModel {
    @SerializedName("idsayur")
    @Expose
    private Integer idsayur;
    @SerializedName("fk_idmasakan")
    @Expose
    private Integer fkIdmasakan;
    @SerializedName("nama_sayur")
    @Expose
    private String namaSayur;
    @SerializedName("kalori")
    @Expose
    private Integer kalori;
    @SerializedName("berat")
    @Expose
    private Integer berat;

    public Integer getIdsayur() {
        return idsayur;
    }

    public void setIdsayur(Integer idsayur) {
        this.idsayur = idsayur;
    }

    public Integer getFkIdmasakan() {
        return fkIdmasakan;
    }

    public void setFkIdmasakan(Integer fkIdmasakan) {
        this.fkIdmasakan = fkIdmasakan;
    }

    public String getNamaSayur() {
        return namaSayur;
    }

    public void setNamaSayur(String namaSayur) {
        this.namaSayur = namaSayur;
    }

    public Integer getKalori() {
        return kalori;
    }

    public void setKalori(Integer kalori) {
        this.kalori = kalori;
    }

    public Integer getBerat() {
        return berat;
    }

    public void setBerat(Integer berat) {
        this.berat = berat;
    }
}
