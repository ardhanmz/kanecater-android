package ardhanmz.com.kanecater.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LaukDuaCustomModel {
    @SerializedName("idlaukdua")
    @Expose
    private Integer idlaukdua;
    @SerializedName("fk_idmasakan")
    @Expose
    private Integer fkIdmasakan;
    @SerializedName("nama_laukdua")
    @Expose
    private String namaLaukDua;
    @SerializedName("kalori")
    @Expose
    private Integer kalori;
    @SerializedName("berat")
    @Expose
    private Integer berat;

    public Integer getIdlaukdua() {
        return idlaukdua;
    }

    public void setIdlaukdua(Integer idlaukdua) {
        this.idlaukdua = idlaukdua;
    }

    public Integer getFkIdmasakan() {
        return fkIdmasakan;
    }

    public void setFkIdmasakan(Integer fkIdmasakan) {
        this.fkIdmasakan = fkIdmasakan;
    }

    public String getNamaLaukDua() {
        return namaLaukDua;
    }

    public void setNamaLaukDua(String namaLaukDua) {
        this.namaLaukDua = namaLaukDua;
    }

    public Integer getKalori() {
        return kalori;
    }

    public void setKalori(Integer kalori) {
        this.kalori = kalori;
    }

    public Integer getBerat() {
        return berat;
    }

    public void setBerat(Integer berat) {
        this.berat = berat;
    }
}
