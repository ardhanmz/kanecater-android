package ardhanmz.com.kanecater.model;

/**
 * Created by Zephyrus on 25/06/2019.
 */

public class CheckOrderModel {
    private String iddata_order;
    private String tanggal;

    public void setIddata_order(String iddata_order) {
        this.iddata_order = iddata_order;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
