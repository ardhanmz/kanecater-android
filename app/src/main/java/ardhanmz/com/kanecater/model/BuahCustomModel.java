package ardhanmz.com.kanecater.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BuahCustomModel {
    @SerializedName("idbuah")
    @Expose
    private Integer idbuah;
    @SerializedName("fk_idmasakan")
    @Expose
    private Integer fkIdmasakan;
    @SerializedName("nama_buah")
    @Expose
    private String namaBuah;
    @SerializedName("kalori")
    @Expose
    private Integer kalori;
    @SerializedName("berat")
    @Expose
    private Integer berat;

    public Integer getIdbuah() {
        return idbuah;
    }

    public void setIdbuah(Integer idbuah) {
        this.idbuah = idbuah;
    }

    public Integer getFkIdmasakan() {
        return fkIdmasakan;
    }

    public void setFkIdmasakan(Integer fkIdmasakan) {
        this.fkIdmasakan = fkIdmasakan;
    }

    public String getNamaBuah() {
        return namaBuah;
    }

    public void setNamaBuah(String namaBuah) {
        this.namaBuah = namaBuah;
    }

    public Integer getKalori() {
        return kalori;
    }

    public void setKalori(Integer kalori) {
        this.kalori = kalori;
    }

    public Integer getBerat() {
        return berat;
    }

    public void setBerat(Integer berat) {
        this.berat = berat;
    }
}
