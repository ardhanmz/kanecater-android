package ardhanmz.com.kanecater.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardPaketModel {
    @SerializedName("idpaket")
    @Expose
    private Integer idpaket;
    @SerializedName("nama_paket")
    @Expose
    private String namaPaket;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("fk_idmenu")
    @Expose
    private Integer fkIdmenu;

    public DashboardPaketModel(String s, String s1) {
        this.namaPaket = s;
        this.image = s1;
    }

    public Integer getIdpaket() {
        return idpaket;
    }

    public void setIdpaket(Integer idpaket) {
        this.idpaket = idpaket;
    }

    public String getNamaPaket() {
        return namaPaket;
    }

    public void setNamaPaket(String namaPaket) {
        this.namaPaket = namaPaket;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getFkIdmenu() {
        return fkIdmenu;
    }

    public void setFkIdmenu(Integer fkIdmenu) {
        this.fkIdmenu = fkIdmenu;
    }
}
