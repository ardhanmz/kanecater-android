package ardhanmz.com.kanecater.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LaukSatuCustomModel {
    @SerializedName("idlauksatu")
    @Expose
    private Integer idlauksatu;
    @SerializedName("fk_idmasakan")
    @Expose
    private Integer fkIdmasakan;
    @SerializedName("nama_lauksatu")
    @Expose
    private String namaLaukSatu;
    @SerializedName("kalori")
    @Expose
    private Integer kalori;
    @SerializedName("berat")
    @Expose
    private Integer berat;

    public Integer getIdlauksatu() {
        return idlauksatu;
    }

    public void setIdlauksatu(Integer idlauksatu) {
        this.idlauksatu = idlauksatu;
    }

    public Integer getFkIdmasakan() {
        return fkIdmasakan;
    }

    public void setFkIdmasakan(Integer fkIdmasakan) {
        this.fkIdmasakan = fkIdmasakan;
    }

    public String getNamaLaukSatu() {
        return namaLaukSatu;
    }

    public void setNamaLaukSatu(String namaLaukSatu) {
        this.namaLaukSatu = namaLaukSatu;
    }

    public Integer getKalori() {
        return kalori;
    }

    public void setKalori(Integer kalori) {
        this.kalori = kalori;
    }

    public Integer getBerat() {
        return berat;
    }

    public void setBerat(Integer berat) {
        this.berat = berat;
    }
}
