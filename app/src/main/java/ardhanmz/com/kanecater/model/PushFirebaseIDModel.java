package ardhanmz.com.kanecater.model;

/**
 * Created by Zephyrus on 18/03/2019.
 */

public class PushFirebaseIDModel {
    private String iduser, device_id;

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }
}
