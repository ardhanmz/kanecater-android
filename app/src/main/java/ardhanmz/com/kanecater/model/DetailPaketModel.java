package ardhanmz.com.kanecater.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailPaketModel {
    @SerializedName("idpaket")
    @Expose
    private Integer idpaket;
    @SerializedName("nama_paket")
    @Expose
    private String namaPaket;
    @SerializedName("harga_paket")
    @Expose
    private String hargaPaket;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("kalori")
    @Expose
    private String kalori;
    @SerializedName("protein")
    @Expose
    private String protein;
    @SerializedName("lemak")
    @Expose
    private String lemak;
    @SerializedName("karbo")
    @Expose
    private String karbo;

    public Integer getIdpaket() {
        return idpaket;
    }

    public void setIdpaket(Integer idpaket) {
        this.idpaket = idpaket;
    }

    public String getNamaPaket() {
        return namaPaket;
    }

    public void setNamaPaket(String namaPaket) {
        this.namaPaket = namaPaket;
    }

    public String getHargaPaket() {
        return hargaPaket;
    }

    public void setHargaPaket(String hargaPaket) {
        this.hargaPaket = hargaPaket;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getKalori() {
        return kalori;
    }

    public void setKalori(String kalori) {
        this.kalori = kalori;
    }

    public String getProtein() {
        return protein;
    }

    public void setProtein(String protein) {
        this.protein = protein;
    }

    public String getLemak() {
        return lemak;
    }

    public void setLemak(String lemak) {
        this.lemak = lemak;
    }

    public String getKarbo() {
        return karbo;
    }

    public void setKarbo(String karbo) {
        this.karbo = karbo;
    }
}
