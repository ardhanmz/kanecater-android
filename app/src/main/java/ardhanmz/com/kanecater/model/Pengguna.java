package ardhanmz.com.kanecater.model;

public class Pengguna {
    private String iduser;
    private String username;
    private String email;
    private String password;
    private String avatar;

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getIduser() {
        return iduser;
    }

    public String getPassword() {
        return password;
    }
}
