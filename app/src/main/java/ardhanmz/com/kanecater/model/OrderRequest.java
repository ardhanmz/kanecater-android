package ardhanmz.com.kanecater.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class OrderRequest {
    @SerializedName("iddata_order")
    private String iddata_order;
    @SerializedName("langganan_awal")
    private String langganan_awal;
    @SerializedName("langganan_akhir")
    private String langganan_akhir;
    @SerializedName("status_pembayaran")
    private String status_pembayaran;
    @SerializedName("status_order")
    private String status_order;
    @SerializedName("fk_idanak")
    private Integer fk_idanak;
    @SerializedName("fk_idpengguna")
    private Integer fk_idpengguna;
    @SerializedName("fk_idpaket")
    private Integer fk_idpaket;
    @SerializedName("fk_idpembayaran")
    private Integer fk_idpembayaran;
    @SerializedName("alamat_order")
    private String alamat_order;
    @SerializedName("jam_order")
    private String jam_order;

    public void setLangganan_awal(String langganan_awal) {
        this.langganan_awal = langganan_awal;
    }

    public void setLangganan_akhir(String langganan_akhir) {
        this.langganan_akhir = langganan_akhir;
    }

    public void setStatus_pembayaran(String status_pembayaran) {
        this.status_pembayaran = status_pembayaran;
    }

    public void setStatus_order(String status_order) {
        this.status_order = status_order;
    }

    public void setFk_idanak(Integer fk_idanak) {
        this.fk_idanak = fk_idanak;
    }

    public void setFk_idpengguna(Integer fk_idpengguna) {
        this.fk_idpengguna = fk_idpengguna;
    }

    public void setFk_idpaket(Integer fk_idpaket) {
        this.fk_idpaket = fk_idpaket;
    }

    public void setFk_idpembayaran(Integer fk_idpembayaran) {
        this.fk_idpembayaran = fk_idpembayaran;
    }

    public void setAlamat_order(String alamat_order) {
        this.alamat_order = alamat_order;
    }

    public void setJam_order(String jam_order) {
        this.jam_order = jam_order;
    }

    public void setIddata_order(String iddata_order) {
        this.iddata_order = iddata_order;
    }
}
