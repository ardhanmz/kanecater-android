package ardhanmz.com.kanecater.model;

public class MasakanModel {
    private String idmasakan, nama_masakan, image_masakan;

    public void setNama_masakan(String nama_masakan) {
        this.nama_masakan = nama_masakan;
    }

    public void setImage_masakan(String image_masakan) {
        this.image_masakan = image_masakan;
    }

    public void setIdmasakan(String idmasakan) {
        this.idmasakan = idmasakan;
    }

    public String getIdmasakan() {
        return idmasakan;
    }

    public String getNama_masakan() {
        return nama_masakan;
    }

    public String getImage_masakan() {
        return image_masakan;
    }
}
