package ardhanmz.com.kanecater.model;

import java.util.ArrayList;

import ardhanmz.com.kanecater.response.PaketLanggananResponse;

public class KategoriPaketDBModel {
    private String headerTitle;
    private ArrayList<PaketLanggananResponse> allItemInSection;

    public KategoriPaketDBModel() {
    }

    public KategoriPaketDBModel(String headerTitle, ArrayList<PaketLanggananResponse> allItemInSection) {
        this.headerTitle = headerTitle;
        this.allItemInSection = allItemInSection;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<PaketLanggananResponse> getAllItemInSection() {
        return allItemInSection;
    }

    public void setAllItemInSection(ArrayList<PaketLanggananResponse> allItemInSection) {
        this.allItemInSection = allItemInSection;
        this.notifyAll();
    }
}
