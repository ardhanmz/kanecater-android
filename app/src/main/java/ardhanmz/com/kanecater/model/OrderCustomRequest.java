package ardhanmz.com.kanecater.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderCustomRequest {
    @SerializedName("fk_iddata_order")
    @Expose
    private int fk_iddata_order;
    @SerializedName("fk_idmenu_nasi")
    @Expose
    private int fk_idmenu_nasi;
    @SerializedName("fk_idmenu_lauksatu")
    @Expose
    private int fk_idmenu_lauksatu;
    @SerializedName("fk_idmenu_laukdua")
    @Expose
    private int fk_idmenu_laukdua;
    @SerializedName("fk_idmenu_sayur")
    @Expose
    private int fk_idmenu_sayur;
    @SerializedName("fk_idmenu_buah")
    @Expose
    private int fk_idmenu_buah;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;
    @SerializedName("fk_idpaket")
    @Expose
    private String fk_idpaket;
    @SerializedName("kalori")
    @Expose
    private String kalori;

    public void setFk_iddata_order(int fk_iddata_order) {
        this.fk_iddata_order = fk_iddata_order;
    }

    public void setFk_idmenu_nasi(int fk_idmenu_nasi) {
        this.fk_idmenu_nasi = fk_idmenu_nasi;
    }

    public void setFk_idmenu_lauksatu(int fk_idmenu_lauksatu) {
        this.fk_idmenu_lauksatu = fk_idmenu_lauksatu;
    }

    public void setFk_idmenu_laukdua(int fk_idmenu_laukdua) {
        this.fk_idmenu_laukdua = fk_idmenu_laukdua;
    }

    public void setFk_idmenu_sayur(int fk_idmenu_sayur) {
        this.fk_idmenu_sayur = fk_idmenu_sayur;
    }

    public void setFk_idmenu_buah(int fk_idmenu_buah) {
        this.fk_idmenu_buah = fk_idmenu_buah;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public void setFk_idpaket(String fk_idpaket) {
        this.fk_idpaket = fk_idpaket;
    }

    public void setKalori(String kalori) {
        this.kalori = kalori;
    }
}
