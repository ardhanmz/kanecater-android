package ardhanmz.com.kanecater.model;

/**
 * Created by Zephyrus on 14/02/2019.
 */

public class ChangePassword {
    private String old_password;
    private String new_password;
    private String validate_password;

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getValidate_password() {
        return validate_password;
    }

    public void setValidate_password(String validate_password) {
        this.validate_password = validate_password;
    }
}
