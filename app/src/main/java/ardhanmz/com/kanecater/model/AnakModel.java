package ardhanmz.com.kanecater.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnakModel {
    @SerializedName("nama_anak")
    @Expose
    private String nama_anak;
    @SerializedName("tanggal_lahir")
    @Expose
    private String tanggal_lahir;
    @SerializedName("jenis_kelamin")
    @Expose
    private String jenis_kelamin;
    @SerializedName("tinggi_badan")
    @Expose
    private int tinggi_badan;
    @SerializedName("berat_badan")
    @Expose
    private int berat_badan;
    @SerializedName("asal_sekolah")
    @Expose
    private String asal_sekolah;
    @SerializedName("alamat_sekolah")
    @Expose
    private String alamat_sekolah;
    @SerializedName("kelas")
    @Expose
    private String kelas;
    @SerializedName("fk_idpengguna")
    @Expose
    private String fk_idpengguna;

    public void setNama_anak(String nama_anak) {
        this.nama_anak = nama_anak;
    }

    public void setTanggal_lahir(String tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public void setTinggi_badan(int tinggi_badan) {
        this.tinggi_badan = tinggi_badan;
    }

    public void setBerat_badan(int berat_badan) {
        this.berat_badan = berat_badan;
    }

    public void setAsal_sekolah(String asal_sekolah) {
        this.asal_sekolah = asal_sekolah;
    }

    public void setAlamat_sekolah(String alamat_sekolah) {
        this.alamat_sekolah = alamat_sekolah;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public void setFk_idpengguna(String fk_idpengguna) {
        this.fk_idpengguna = fk_idpengguna;
    }
}
