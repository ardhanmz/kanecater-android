
package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenuNasiResponse {

    @SerializedName("idmenu_nasi")
    @Expose
    private Integer idmenu_nasi;
    @SerializedName("fk_id_paket")
    @Expose
    private String fk_id_paket;
    @SerializedName("nama_menu")
    @Expose
    private String namaNasi;
    @SerializedName("kalori")
    @Expose
    private Integer kalori;
    @SerializedName("berat")
    @Expose
    private Integer berat;
    @SerializedName("image_menu")
    @Expose
    private Integer image_menu;

    public Integer getIdnasi() {
        return idmenu_nasi;
    }

    public String getFkIdmasakan() {
        return fk_id_paket;
    }

    public String getNamaNasi() {
        return namaNasi;
    }

    public Integer getKalori() {
        return kalori;
    }

    public void setKalori(Integer kalori) {
        this.kalori = kalori;
    }

    public Integer getBerat() {
        return berat;
    }

    public void setBerat(Integer berat) {
        this.berat = berat;
    }

    public Integer getImage_menu() {
        return image_menu;
    }
}
