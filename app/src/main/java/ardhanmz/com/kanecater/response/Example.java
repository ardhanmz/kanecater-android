
package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Example {

    @SerializedName("paket")
    @Expose
    private Paket paket;
    @SerializedName("anakresponse")
    @Expose
    private AnakResponse anakresponse;
    @SerializedName("nasi")
    @Expose
    private Nasi nasi;
    @SerializedName("lauksatu")
    @Expose
    private Lauksatu lauksatu;
    @SerializedName("laukdua")
    @Expose
    private Laukdua laukdua;
    @SerializedName("sayur")
    @Expose
    private Sayur sayur;

    public Paket getPaket() {
        return paket;
    }

    public AnakResponse getAnakresponse() {
        return anakresponse;
    }

    public void setAnakresponse(AnakResponse anakresponse) {
        this.anakresponse = anakresponse;
    }

    public void setPaket(Paket paket) {
        this.paket = paket;
    }

    public Nasi getNasi() {
        return nasi;
    }

    public void setNasi(Nasi nasi) {
        this.nasi = nasi;
    }

    public Lauksatu getLauksatu() {
        return lauksatu;
    }

    public void setLauksatu(Lauksatu lauksatu) {
        this.lauksatu = lauksatu;
    }

    public Laukdua getLaukdua() {
        return laukdua;
    }

    public void setLaukdua(Laukdua laukdua) {
        this.laukdua = laukdua;
    }

    public Sayur getSayur() {
        return sayur;
    }

    public void setSayur(Sayur sayur) {
        this.sayur = sayur;
    }

}
