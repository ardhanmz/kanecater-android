package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnakResponse {

    @SerializedName("id_anak")
    @Expose
    private Integer id_anak;
    @SerializedName("nama_anak")
    @Expose
    private String namaAnak;
    @SerializedName("tanggal_lahir")
    @Expose
    private String tanggalLahir;
    @SerializedName("jenis_kelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("berat_badan")
    @Expose
    private Integer beratBadan;
    @SerializedName("tinggi_badan")
    @Expose
    private Integer tinggiBadan;
    @SerializedName("nama_sekolah")
    @Expose
    private String namaSekolah;
    @SerializedName("kelas")
    @Expose
    private String kelas;
    @SerializedName("fk_idpengguna")
    @Expose
    private Integer fkIdpengguna;
    @SerializedName("asal_sekolah")
    @Expose
    private String asal_sekolah;
    @SerializedName("alamat_sekolah")
    @Expose
    private String alamat_sekolah;

    public Integer getIdanak() {
        return id_anak;
    }

    public void setIdanak(Integer idanak) {
        this.id_anak = idanak;
    }

    public String getNamaAnak() {
        return namaAnak;
    }

    public void setNamaAnak(String namaAnak) {
        this.namaAnak = namaAnak;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public Integer getBeratBadan() {
        return beratBadan;
    }

    public void setBeratBadan(Integer beratBadan) {
        this.beratBadan = beratBadan;
    }

    public Integer getTinggiBadan() {
        return tinggiBadan;
    }

    public void setTinggiBadan(Integer tinggiBadan) {
        this.tinggiBadan = tinggiBadan;
    }

    public String getNamaSekolah() {
        return namaSekolah;
    }

    public void setNamaSekolah(String namaSekolah) {
        this.namaSekolah = namaSekolah;
    }

    public String getKelasSekolah() {
        return kelas;
    }

    public void setKelasSekolah(String kelasSekolah) {
        this.kelas = kelasSekolah;
    }

    public Integer getFkIdpengguna() {
        return fkIdpengguna;
    }

    public void setFkIdpengguna(Integer fkIdpengguna) {
        this.fkIdpengguna = fkIdpengguna;
    }

    public String getAsal_sekolah() {
        return asal_sekolah;
    }

    public String getAlamat_sekolah() {
        return alamat_sekolah;
    }

    public void setAlamat_sekolah(String alamat_sekolah) {
        this.alamat_sekolah = alamat_sekolah;
    }
}
