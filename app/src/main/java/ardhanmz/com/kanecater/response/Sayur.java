
package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sayur {

    @SerializedName("jumlah")
    @Expose
    private Integer jumlah;
    @SerializedName("kalori")
    @Expose
    private Double kalori;
    @SerializedName("lemak")
    @Expose
    private Integer lemak;

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public Double getKalori() {
        return kalori;
    }

    public void setKalori(Double kalori) {
        this.kalori = kalori;
    }

    public Integer getLemak() {
        return lemak;
    }

    public void setLemak(Integer lemak) {
        this.lemak = lemak;
    }

}
