
package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Nasi {

    @SerializedName("jumlah")
    @Expose
    private Integer jumlah;
    @SerializedName("kalori")
    @Expose
    private Integer kalori;
    @SerializedName("karbohidrat")
    @Expose
    private Integer karbohidrat;

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public Integer getKalori() {
        return kalori;
    }

    public void setKalori(Integer kalori) {
        this.kalori = kalori;
    }

    public Integer getKarbohidrat() {
        return karbohidrat;
    }

    public void setKarbohidrat(Integer karbohidrat) {
        this.karbohidrat = karbohidrat;
    }

}
