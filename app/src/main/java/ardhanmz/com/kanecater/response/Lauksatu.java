
package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lauksatu {

    @SerializedName("jumlah")
    @Expose
    private Integer jumlah;
    @SerializedName("kalori")
    @Expose
    private Double kalori;
    @SerializedName("protein")
    @Expose
    private Double protein;

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public Double getKalori() {
        return kalori;
    }

    public void setKalori(Double kalori) {
        this.kalori = kalori;
    }

    public Double getProtein() {
        return protein;
    }

    public void setProtein(Double protein) {
        this.protein = protein;
    }

}
