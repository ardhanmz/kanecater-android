
package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Paket {

    @SerializedName("id_paket")
    @Expose
    private Integer idPaket;
    @SerializedName("nama_paket")
    @Expose
    private String namaPaket;
    @SerializedName("tipe_paket")
    @Expose
    private String tipePaket;
    @SerializedName("image_paket")
    @Expose
    private String imagePaket;
    @SerializedName("harga_paket")
    @Expose
    private String hargaPaket;
    @SerializedName("deskripsi_paket")
    @Expose
    private String deskripsiPaket;

    public Integer getIdPaket() {
        return idPaket;
    }

    public void setIdPaket(Integer idPaket) {
        this.idPaket = idPaket;
    }

    public String getNamaPaket() {
        return namaPaket;
    }

    public void setNamaPaket(String namaPaket) {
        this.namaPaket = namaPaket;
    }

    public String getTipePaket() {
        return tipePaket;
    }

    public void setTipePaket(String tipePaket) {
        this.tipePaket = tipePaket;
    }

    public String getImagePaket() {
        return imagePaket;
    }

    public void setImagePaket(String imagePaket) {
        this.imagePaket = imagePaket;
    }

    public String getHargaPaket() {
        return hargaPaket;
    }

    public void setHargaPaket(String hargaPaket) {
        this.hargaPaket = hargaPaket;
    }

    public String getDeskripsiPaket() {
        return deskripsiPaket;
    }

    public void setDeskripsiPaket(String deskripsiPaket) {
        this.deskripsiPaket = deskripsiPaket;
    }

}
