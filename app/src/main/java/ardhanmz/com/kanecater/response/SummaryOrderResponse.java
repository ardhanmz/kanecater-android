package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zephyrus on 07/04/2019.
 */

public class SummaryOrderResponse {

    @SerializedName("iddata_order")
    @Expose
    public Integer iddataOrder;
    @SerializedName("langganan_awal")
    @Expose
    public String langgananAwal;
    @SerializedName("langganan_akhir")
    @Expose
    public String langgananAkhir;
    @SerializedName("status_pembayaran")
    @Expose
    public Object statusPembayaran;
    @SerializedName("fk_idanak")
    @Expose
    public Integer fkIdanak;
    @SerializedName("fk_idpengguna")
    @Expose
    public Integer fkIdpengguna;
    @SerializedName("fk_idpembayaran")
    @Expose
    public Integer fkIdpembayaran;
    @SerializedName("fk_idpaket")
    @Expose
    public Integer fkIdpaket;
    @SerializedName("status_order")
    @Expose
    public String statusOrder;
    @SerializedName("alamat_order")
    @Expose
    public String alamatOrder;
    @SerializedName("jam_order")
    @Expose
    public String jamOrder;

    public Integer getIddataOrder() {
        return iddataOrder;
    }

    public String getLanggananAwal() {
        return langgananAwal;
    }

    public String getLanggananAkhir() {
        return langgananAkhir;
    }

    public Object getStatusPembayaran() {
        return statusPembayaran;
    }

    public Integer getFkIdanak() {
        return fkIdanak;
    }

    public Integer getFkIdpengguna() {
        return fkIdpengguna;
    }

    public Integer getFkIdpembayaran() {
        return fkIdpembayaran;
    }

    public Integer getFkIdpaket() {
        return fkIdpaket;
    }

    public String getStatusOrder() {
        return statusOrder;
    }

    public String getAlamatOrder() {
        return alamatOrder;
    }

    public String getJamOrder() {
        return jamOrder;
    }
}
