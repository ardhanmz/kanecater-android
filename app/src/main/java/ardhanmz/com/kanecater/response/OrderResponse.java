package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderResponse {

    @SerializedName("iddata_order")
    @Expose
    public int iddataOrder;
    @SerializedName("langganan_awal")
    @Expose
    public String langgananAwal;
    @SerializedName("langganan_akhir")
    @Expose
    public String langgananAkhir;
    @SerializedName("status_pembayaran")
    @Expose
    public String statusPembayaran;
    @SerializedName("status_order")
    @Expose
    public String statusOrder;
    @SerializedName("nama_anak")
    @Expose
    public String namaAnak;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("metode_pembayaran")
    @Expose
    public String metodePembayaran;
    @SerializedName("nama_paket")
    @Expose
    public String namaPaket;
    @SerializedName("tipe_paket")
    @Expose
    public String tipePaket;
    @SerializedName("image_paket")
    @Expose
    public String imagePaket;

    public int getIddataOrder() {
        return iddataOrder;
    }

    public void setIddataOrder(int iddataOrder) {
        this.iddataOrder = iddataOrder;
    }

    public String getLanggananAwal() {
        return langgananAwal;
    }

    public void setLanggananAwal(String langgananAwal) {
        this.langgananAwal = langgananAwal;
    }

    public String getLanggananAkhir() {
        return langgananAkhir;
    }

    public void setLanggananAkhir(String langgananAkhir) {
        this.langgananAkhir = langgananAkhir;
    }

    public String getStatusPembayaran() {
        return statusPembayaran;
    }

    public void setStatusPembayaran(String statusPembayaran) {
        this.statusPembayaran = statusPembayaran;
    }

    public String getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(String statusOrder) {
        this.statusOrder = statusOrder;
    }

    public String getNamaAnak() {
        return namaAnak;
    }

    public void setNamaAnak(String namaAnak) {
        this.namaAnak = namaAnak;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMetodePembayaran() {
        return metodePembayaran;
    }

    public void setMetodePembayaran(String metodePembayaran) {
        this.metodePembayaran = metodePembayaran;
    }

    public String getNamaPaket() {
        return namaPaket;
    }

    public void setNamaPaket(String namaPaket) {
        this.namaPaket = namaPaket;
    }

    public String getTipePaket() {
        return tipePaket;
    }

    public void setTipePaket(String tipePaket) {
        this.tipePaket = tipePaket;
    }

    public String getImagePaket() {
        return imagePaket;
    }

    public void setImagePaket(String imagePaket) {
        this.imagePaket = imagePaket;
    }

    public Object getHargaPaket() {
        return hargaPaket;
    }

    public void setHargaPaket(Object hargaPaket) {
        this.hargaPaket = hargaPaket;
    }

    @SerializedName("harga_paket")
    @Expose
    public Object hargaPaket;

}
