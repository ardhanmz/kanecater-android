
package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchResponse {

    @SerializedName("idpaket")
    @Expose
    private Integer idpaket;
    @SerializedName("nama_paket")
    @Expose
    private String namaPaket;
    @SerializedName("harga_paket")
    @Expose
    private String hargaPaket;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("kalori")
    @Expose
    private Integer kalori;
    @SerializedName("protein")
    @Expose
    private Integer protein;
    @SerializedName("lemak")
    @Expose
    private Integer lemak;
    @SerializedName("karbo")
    @Expose
    private Integer karbo;

    public Integer getIdpaket() {
        return idpaket;
    }

    public void setIdpaket(Integer idpaket) {
        this.idpaket = idpaket;
    }

    public String getNamaPaket() {
        return namaPaket;
    }

    public void setNamaPaket(String namaPaket) {
        this.namaPaket = namaPaket;
    }

    public String getHargaPaket() {
        return hargaPaket;
    }

    public void setHargaPaket(String hargaPaket) {
        this.hargaPaket = hargaPaket;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getKalori() {
        return kalori;
    }

    public void setKalori(Integer kalori) {
        this.kalori = kalori;
    }

    public Integer getProtein() {
        return protein;
    }

    public void setProtein(Integer protein) {
        this.protein = protein;
    }

    public Integer getLemak() {
        return lemak;
    }

    public void setLemak(Integer lemak) {
        this.lemak = lemak;
    }

    public Integer getKarbo() {
        return karbo;
    }

    public void setKarbo(Integer karbo) {
        this.karbo = karbo;
    }

}
