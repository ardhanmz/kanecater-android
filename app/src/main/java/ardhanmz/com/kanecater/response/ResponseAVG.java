package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zephyrus on 10/03/2019.
 */

public class ResponseAVG {
    @SerializedName("kalori")
    @Expose
    private String kalori;
    @SerializedName("jumlah")
    @Expose
    private String jumlah;
    @SerializedName("protein")
    @Expose
    private double protein;
    @SerializedName("karbohidrat")
    @Expose
    private int karbohidrat;
    @SerializedName("lemak")
    @Expose
    private double lemak;

    public String getKalori() {
        return kalori;
    }

    public String getJumlah() {
        return jumlah;
    }

    public double getProtein() {
        return protein;
    }

    public int getKarbohidrat() {
        return karbohidrat;
    }

    public double getLemak() {
        return lemak;
    }
}
