package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zephyrus on 14/04/2019.
 */

public class StatusOrderResponse {
    @SerializedName("iddata_order")
    @Expose
    public int iddataOrder;
    @SerializedName("langganan_awal")
    @Expose
    public String langgananAwal;
    @SerializedName("langganan_akhir")
    @Expose
    public String langgananAkhir;
    @SerializedName("status_pembayaran")
    @Expose
    public String statusPembayaran;
    @SerializedName("status_order")
    @Expose
    public String statusOrder;
    @SerializedName("progres_order")
    @Expose
    public String progresOrder;
    @SerializedName("nama_anak")
    @Expose
    public String namaAnak;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("metode_pembayaran")
    @Expose
    public String metodePembayaran;
    @SerializedName("nama_paket")
    @Expose
    public String namaPaket;
    @SerializedName("tipe_paket")
    @Expose
    public String tipePaket;
    @SerializedName("image_paket")
    @Expose
    public String imagePaket;
    @SerializedName("harga_paket")
    @Expose
    public Object hargaPaket;

    public int getIddataOrder() {
        return iddataOrder;
    }

    public String getLanggananAwal() {
        return langgananAwal;
    }

    public String getLanggananAkhir() {
        return langgananAkhir;
    }

    public String getStatusPembayaran() {
        return statusPembayaran;
    }

    public String getStatusOrder() {
        return statusOrder;
    }

    public String getProgresOrder() {
        return progresOrder;
    }

    public String getNamaAnak() {
        return namaAnak;
    }

    public String getUsername() {
        return username;
    }

    public String getMetodePembayaran() {
        return metodePembayaran;
    }

    public String getNamaPaket() {
        return namaPaket;
    }

    public String getTipePaket() {
        return tipePaket;
    }

    public String getImagePaket() {
        return imagePaket;
    }

    public Object getHargaPaket() {
        return hargaPaket;
    }
}
