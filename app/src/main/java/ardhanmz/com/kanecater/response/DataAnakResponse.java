
package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataAnakResponse {

    @SerializedName("idanak")
    @Expose
    private Integer idanak;
    @SerializedName("idpengguna")
    @Expose
    private Integer idpengguna;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("nama")
    @Expose
    private String nama;

    public Integer getIdanak() {
        return idanak;
    }

    public void setIdanak(Integer idanak) {
        this.idanak = idanak;
    }

    public Integer getIdpengguna() {
        return idpengguna;
    }

    public void setIdpengguna(Integer idpengguna) {
        this.idpengguna = idpengguna;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

}
