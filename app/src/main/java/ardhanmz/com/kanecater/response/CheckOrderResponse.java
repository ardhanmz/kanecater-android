package ardhanmz.com.kanecater.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zephyrus on 25/06/2019.
 */

public class CheckOrderResponse {

    @SerializedName("NASI")
    @Expose
    private String nASI;
    @SerializedName("LAUK_SATU")
    @Expose
    private String lAUKSATU;
    @SerializedName("LAUK_DUA")
    @Expose
    private String lAUKDUA;
    @SerializedName("SAYUR")
    @Expose
    private String SAYUR;
    @SerializedName("BUAH")
    @Expose
    private String BUAH;
    @SerializedName("kalori")
    @Expose
    private String kalori;


    public String getNASI() {
        return nASI;
    }

    public void setNASI(String nASI) {
        this.nASI = nASI;
    }

    public String getLAUKSATU() {
        return lAUKSATU;
    }

    public void setLAUKSATU(String lAUKSATU) {
        this.lAUKSATU = lAUKSATU;
    }

    public String getLAUKDUA() {
        return lAUKDUA;
    }

    public void setLAUKDUA(String lAUKDUA) {
        this.lAUKDUA = lAUKDUA;
    }

    public String getSAYUR() {
        return SAYUR;
    }

    public String getBUAH() {
        return BUAH;
    }

    public String getKalori() {
        return kalori;
    }
}
