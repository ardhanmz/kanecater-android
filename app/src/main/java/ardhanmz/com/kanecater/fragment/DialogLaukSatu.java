package ardhanmz.com.kanecater.fragment;


import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.adapter.CustomLaukSatuAdapter;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.response.MenuLaukSatuResponse;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogLaukSatu extends DialogFragment {
    @BindView(R.id.rv_lauksatu)
    RecyclerView rv_lauksatu;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private CustomLaukSatuAdapter adapter;
    private RecyclerELEAdapter RAdapter;
    private OnLaukSatu callback;
    private ArrayList<MenuLaukSatuResponse> arrayList;
    private String idmasakan;

    public DialogLaukSatu() {
        // Required empty public constructor
    }

    public interface OnLaukSatu {
        public void onLaukData(int idlauksatu, String nama_lauksatu, int kalori, int berat, String image_lauksatu);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            callback = (OnLaukSatu) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling Fragment must implement OnAddFriendListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_custom_lauk_satu, container, false);
        ButterKnife.bind(this, v);
        arrayList = new ArrayList<>();
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        idmasakan = getArguments().getString("idpaket","");
        initAction();
        loadRV();
        return v;
    }
    private void loadRV() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        rv_lauksatu.setLayoutManager(mLayoutManager);
        rv_lauksatu.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        rv_lauksatu.setItemAnimator(new DefaultItemAnimator());
        View loadingView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvloading, rv_lauksatu, false);
        View errorView = getActivity().getLayoutInflater().inflate(R.layout.layout_rv_error, rv_lauksatu, false);
        View emptyView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvempty, rv_lauksatu, false);
        adapter = new CustomLaukSatuAdapter(getContext(), arrayList);
        RAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
        rv_lauksatu.setAdapter(adapter);
        rv_lauksatu.setHasFixedSize(true);
        rv_lauksatu.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Integer id_lauksatu = arrayList.get(position).getIdmenuLausatu();
                Integer idmasak = arrayList.get(position).getFkIdPaket();
                String namaLaukSatu = arrayList.get(position).getNamaMenu();
                String image_menu = arrayList.get(position).getImageMenu();
                Integer kalori1 = arrayList.get(position).getKalori();
                int berat1 = arrayList.get(position).getBerat();
                callback.onLaukData(id_lauksatu, namaLaukSatu, kalori1, berat1, image_menu);
                dismiss();
            }
        }));
    }
    private void initAction() {
        compositeSubscription.add(NetworkUtil.getRetrofit().getLaukSatuByMasakan(idmasakan)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void onError(Throwable throwable) {
    }

    private void handleResponse(ArrayList<MenuLaukSatuResponse> laukSatuCustomModels) {
        if (laukSatuCustomModels != null){
            arrayList.clear();
            arrayList.addAll(laukSatuCustomModels);
            RAdapter.notifyDataSetChanged();
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_NORMAL);
        } else {
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_EMPTY);
        }
    }
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }
    }
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
