package ardhanmz.com.kanecater.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.acitivities.ManajemenAnakActivity;
import ardhanmz.com.kanecater.acitivities.PaketCustomActivity;
import ardhanmz.com.kanecater.acitivities.PaketLanggananActivity;
import ardhanmz.com.kanecater.adapter.DashboardPaketAdapter;
import ardhanmz.com.kanecater.adapter.PaketAdapter;
import ardhanmz.com.kanecater.model.KategoriPaketDBModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.response.PaketLanggananResponse;
import ardhanmz.com.kanecater.utils.Constants;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabDashboardFragment extends BaseFragment {
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private ArrayList<KategoriPaketDBModel> allsamplelist;
    ArrayList<PaketLanggananResponse> arrayList;
    private Context mContext;

    @BindView(R.id.tv_username)
    TextView tv_username;
    @BindView(R.id.rv_fragment_dashboard)
    RecyclerView rv_dashboard;
    @BindView(R.id.imgbtn_Langganan)
    ImageButton imgbtn_langganan;
    @BindView(R.id.imgbtn_custom)
    ImageButton imgbtn_custom;
    ViewDialog viewDialog;
    private PaketAdapter adapter;

    public TabDashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //TODO Handle All Stuff When Fragment Selec
        // Inflate the layout for this fragment
        mContext = getActivity();
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this,view);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        arrayList = new ArrayList<>();
        viewDialog = new ViewDialog(getActivity());
        component();
        String uname = sharedPreferences.getString(Constants.USERNAME,"");
        tv_username.setText(uname);
        loadDataRV();
        rv_dashboard.setHasFixedSize(true);
        adapter = new PaketAdapter(getActivity(), arrayList);
        rv_dashboard.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rv_dashboard.setHasFixedSize(true);
        rv_dashboard.addOnItemTouchListener(new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Integer idlanggann =arrayList.get(position).getIdPaket();
                String id = Integer.toString(idlanggann);
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                bundle.putString("args", "detail");
                    /*Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();*/
                Intent intent = new Intent(mContext, PaketLanggananActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }));
        rv_dashboard.setAdapter(adapter);
        return view;
    }

    private void component() {
        imgbtn_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PaketCustomActivity.class);
                startActivity(intent);
            }
        });
        imgbtn_langganan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                Intent intent = new Intent(mContext, PaketLanggananActivity.class);
                bundle.putString("args", "paket_langganan");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void loadRVpaket() {
        //TODO Setting RecyclerView Adapter

    }

    private void loadDataRV() {
        //TODO Load Data RecyclerView From Server with Retrofit
        viewDialog.showDialog();
        compositeSubscription.add(NetworkUtil.getRetrofit().getAllPaket()
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(this::handleResponse,this::handleError));
    }


    private void handleResponse(ArrayList<PaketLanggananResponse> dashboardPaketModels) {
        viewDialog.hideDialog();
        //TODO Checking Response And Insert To ArrayList
        if (dashboardPaketModels!=null){
            arrayList.clear();
            arrayList.addAll(dashboardPaketModels);
            adapter.notifyDataSetChanged();
        }
    }
    private void handleError(Throwable throwable){
        //TODO Checking Error While Request
    }

    @Override
    public void onDestroy(){
        //TODO Destroy RXJava Scheduler
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }

}
