package ardhanmz.com.kanecater.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.acitivities.PaketLanggananActivity;
import ardhanmz.com.kanecater.adapter.ListLanggananAdapter;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.response.PaketLanggananResponse;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PaketLanggananFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class PaketLanggananFragment extends Fragment {
    @BindView(R.id.rv_list_langganan)
    RecyclerView rv_langganan;
    @BindView(R.id.text_rvlangganan_kosong)
    TextView rv_kosong;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    ArrayList<PaketLanggananResponse> arrayList;
    ListLanggananAdapter adapter;
    ViewDialog viewDialog;

    private OnFragmentInteractionListener mListener;
    private String mode, query;
    private String id;

    public PaketLanggananFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_paket_langganan, container, false);
        ButterKnife.bind(this, view);
        arrayList = new ArrayList<>();
        viewDialog = new ViewDialog(getActivity());
        query = getArguments().getString("query", "");
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        loadRecycler();
        return view;
    }

    private void loadRecycler() {
        rv_langganan.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        adapter = new ListLanggananAdapter(getActivity(), arrayList);
        rv_langganan.setHasFixedSize(true);
        rv_langganan.setAdapter(adapter);
        rv_langganan.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (query.length()>0){
                    Integer idlanggann = arrayList.get(position).getIdPaket();
                    String idpaket = idlanggann.toString(idlanggann);
                    Bundle bundle = new Bundle();
                    Intent intent = new Intent(getActivity(), PaketLanggananActivity.class);
                    bundle.putString("args", "detail");
                    bundle.putString("id", idpaket);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    Integer idlanggann = arrayList.get(position).getIdPaket();
                    id = Integer.toString(idlanggann);
                    Bundle bundle = new Bundle();
                    bundle.putString("id", id);
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    PaketLanggananFragment paketLanggananFragment = new PaketLanggananFragment();
                    DetailPaketFragment detailPaketFragment = new DetailPaketFragment();
                    detailPaketFragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.frame,detailPaketFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }
        }));
        initAction();
    }

    private void initAction() {
        if (query.length()>0){
            loadDataSearch(query);
        } else {
            loadDataLangganan();
        }

    }

    private void loadDataSearch(String query) {
        viewDialog.showDialog();
        compositeSubscription.add(NetworkUtil.getRetrofit().search(query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void loadDataLangganan() {
        viewDialog.showDialog();
        compositeSubscription.add(NetworkUtil.getRetrofit().getAllPaket()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void onError(Throwable throwable) {
    }

    private void handleResponse(ArrayList<PaketLanggananResponse> dashboardPaketModels) {
        viewDialog.hideDialog();
        if (dashboardPaketModels!=null){
            arrayList.clear();
            arrayList.addAll(dashboardPaketModels);
            adapter.notifyDataSetChanged();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/
    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
