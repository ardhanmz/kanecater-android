package ardhanmz.com.kanecater.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.acitivities.PemesananCustomActivity;
import ardhanmz.com.kanecater.adapter.OrderListAdapter;
import ardhanmz.com.kanecater.response.OrderResponse;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.utils.Constants;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderListFragment extends Fragment {
    @BindView(R.id.rv_orderlist)
    RecyclerView listorder;

    private ArrayList<OrderResponse> arrayList;
    OrderListAdapter orderListAdapter;
    private RecyclerELEAdapter recyclerELEAdapter;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    ViewDialog viewDialog;


    public OrderListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_list, container, false);
        ButterKnife.bind(this, view);
        arrayList = new ArrayList<>();
        viewDialog = new ViewDialog(getActivity());
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Integer wow = sharedPreferences.getInt(Constants.IDPENGGUNA, 0);
        String id = Integer.toString(wow);
        loadDataProgress(id);
        loadRecycler();
        return view;
    }

    private void loadRecycler() {
        listorder.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        View loadingView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvloading, listorder, false);
        View errorView = getActivity().getLayoutInflater().inflate(R.layout.layout_rv_error, listorder, false);
        View emptyView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvempty, listorder, false);
        orderListAdapter = new OrderListAdapter(getActivity(), arrayList);
        recyclerELEAdapter = new RecyclerELEAdapter(orderListAdapter, emptyView, loadingView, errorView);
        listorder.setHasFixedSize(true);
        listorder.setAdapter(recyclerELEAdapter);
        listorder.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (arrayList.get(position).getTipePaket().equalsIgnoreCase("CUSTOM")){
                    String dateawal = arrayList.get(position).getLanggananAwal();
                    String dateakhir = arrayList.get(position).getLanggananAkhir();
                    String nama = arrayList.get(position).getNamaAnak();
                    String idorder = String.valueOf(arrayList.get(position).getIddataOrder());
                    int val = 0;
                    Bundle bundle = new Bundle();
                    bundle.putString("start_date", dateawal);
                    bundle.putString("end_date", dateakhir);
                    bundle.putInt("paket", val);
                    bundle.putString("idorder", idorder);
                    bundle.putString("nama_anak", nama);
                    bundle.putBoolean("lihat_order", true);
                    Intent i = new Intent(getContext(), PemesananCustomActivity.class);
                    i.putExtras(bundle);
                    startActivity(i);
                }
            }
        }));
    }
//
//    private void initAction() {
//        loadDataProgress();
//    }

    private void loadDataProgress(String id) {
        viewDialog.showDialog();
        compositeSubscription.add(NetworkUtil.getRetrofit().getOrderByUserId(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void onError(Throwable throwable) {
    }

    private void handleResponse(ArrayList<OrderResponse> response) {
        viewDialog.hideDialog();
        if (response!=null){
            arrayList.clear();
            arrayList.addAll(response);
            orderListAdapter.notifyDataSetChanged();
            recyclerELEAdapter.setCurrentView(RecyclerELEAdapter.VIEW_NORMAL);
        }else {
            recyclerELEAdapter.setCurrentView(RecyclerELEAdapter.VIEW_EMPTY);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
