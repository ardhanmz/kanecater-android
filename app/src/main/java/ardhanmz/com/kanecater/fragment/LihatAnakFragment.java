package ardhanmz.com.kanecater.fragment;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.acitivities.MapsActivity;
import ardhanmz.com.kanecater.acitivities.Profil;
import ardhanmz.com.kanecater.model.AnakModel;
import ardhanmz.com.kanecater.model.KomponenModel;
import ardhanmz.com.kanecater.response.AnakResponse;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.netutil.ResponseSignup;
import ardhanmz.com.kanecater.response.Example;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * A simple {@link Fragment} subclass.
 */
public class LihatAnakFragment extends Fragment {
    @BindView(R.id.et_nama_anak_m)
    EditText et_nama_anak_m;
    @BindView(R.id.et_tanggal_lahir_m)
    EditText et_tanggal_lahir_m;
    @BindView(R.id.et_jenis_kelamin_m)
    EditText et_jenis_kelamin_m;
    @BindView(R.id.et_berat_badan_m)
    EditText et_berat_badan_m;
    @BindView(R.id.et_tinggi_badan_m)
    EditText et_tinggi_badan_m;
    @BindView(R.id.et_nama_sekolah_m)
    EditText et_nama_sekolah_m;
    @BindView(R.id.et_asal_sekolah_m)
    EditText et_alamat_sekolah;
    @BindView(R.id.et_kelas_sekolah_m)
    EditText et_kelas_sekolah_m;
    @BindView(R.id.buttonmale_m)
    Button buttonmale_m;
    @BindView(R.id.buttonfemale_m)
    Button buttonfemale_m;
    @BindView(R.id.btn_updateAnak)
    Button upDateAnak;
    @BindView(R.id.appbar_m)
    Toolbar toolbar;

    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private String id;
    private Date date, date1;
    private Calendar myCalendar;
    private ProgressDialog mProgressDialog;
    private ProgressDialog mProgressDialog1;
    ViewDialog viewDialog;
    public static String nama_sekolah = null, alamat_sekolah = "";

    public LihatAnakFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        if (nama_sekolah!=null){
            editAnak();
            et_nama_sekolah_m.setText(nama_sekolah);
            et_alamat_sekolah.setText(alamat_sekolah);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lihat_anak, container, false);
        ButterKnife.bind(this, view);
        compositeSubscription = new CompositeSubscription();
        int ids = getArguments().getInt("idanak", 0);
        id = Integer.toString(ids);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        viewDialog = new ViewDialog(getActivity());
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
        myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        buttonfemale_m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_jenis_kelamin_m.setText("Perempuan");
            }
        });
        buttonmale_m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_jenis_kelamin_m.setText("Laki-Laki");
            }
        });
        et_tanggal_lahir_m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        et_nama_sekolah_m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("edit_anak","edit_anak");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        upDateAnak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AnakModel model = new AnakModel();
                model.setNama_anak(et_nama_anak_m.getText().toString());
                model.setTanggal_lahir(et_tanggal_lahir_m.getText().toString());
                CharSequence charSequence1 = et_berat_badan_m.getText();
                int berat = Integer.parseInt(charSequence1.toString());
                model.setBerat_badan(berat);
                CharSequence charSequence = et_tinggi_badan_m.getText();
                int tinggi = Integer.parseInt(charSequence.toString());
                model.setTinggi_badan(tinggi);
                model.setAsal_sekolah(et_nama_sekolah_m.getText().toString());
                model.setAlamat_sekolah(et_alamat_sekolah.getText().toString());
                model.setKelas(et_kelas_sekolah_m.getText().toString());
                model.setJenis_kelamin(et_jenis_kelamin_m.getText().toString());
                upDateDataAnak(model);
            }
        });
        getDataAnak();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_manajemenanak, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.hapus_anak:
                deleteAnak();
            case R.id.edit_anak:
                editAnak();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void getDataAnak() {
        viewDialog.showDialog();
        KomponenModel komponenModel = new KomponenModel();
        komponenModel.setIdpaket(0);
        compositeSubscription.add(NetworkUtil.getRetrofit().getAnak(id,komponenModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }
    private void handleResponse(Example anakResponse) {
        viewDialog.hideDialog();
        if (anakResponse!=null){
            et_tanggal_lahir_m.setText(anakResponse.getAnakresponse().getTanggalLahir());
            et_nama_anak_m.setText(anakResponse.getAnakresponse().getNamaAnak());
            et_jenis_kelamin_m.setText(anakResponse.getAnakresponse().getJenisKelamin());
            et_berat_badan_m.setText(String.valueOf(anakResponse.getAnakresponse().getBeratBadan()));
            et_tinggi_badan_m.setText(String.valueOf(anakResponse.getAnakresponse().getTinggiBadan()));
            et_nama_sekolah_m.setText(anakResponse.getAnakresponse().getAsal_sekolah());
            et_alamat_sekolah.setText(anakResponse.getAnakresponse().getAlamat_sekolah());
            et_kelas_sekolah_m.setText(anakResponse.getAnakresponse().getKelasSekolah());
        }else {
            Toasty.warning(getActivity(), "Kesalahan Dalam Memuat Data Anak", Toasty.LENGTH_SHORT, true).show();
        }
    }

    private void upDateDataAnak(AnakModel model) {
        viewDialog.showDialog();
        compositeSubscription.add(NetworkUtil.getRetrofit().upDateAnak(id, model)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }


    private void editAnak() {
        et_nama_anak_m.setEnabled(true);
        et_tanggal_lahir_m.setEnabled(true);
        et_berat_badan_m.setEnabled(true);
        et_tinggi_badan_m.setEnabled(true);
        et_nama_sekolah_m.setEnabled(true);
        et_kelas_sekolah_m.setEnabled(true);
        buttonfemale_m.setVisibility(View.VISIBLE);
        buttonmale_m.setVisibility(View.VISIBLE);
        upDateAnak.setVisibility(View.VISIBLE);
    }

    private void onError(Throwable throwable) {
    }



    private void deleteAnak() {
        viewDialog.showDialog();
        compositeSubscription.add(NetworkUtil.getRetrofit().deleteAnak(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void handleResponse(ResponseSignup responseSignup) {
        viewDialog.hideDialog();
        if (responseSignup!=null){
            if (responseSignup.getSuccess()){
                getActivity().finish();
            }else {
                Toasty.warning(getActivity(), responseSignup.getMessage(), Toasty.LENGTH_SHORT, true).show();
            }
        }
    }
    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        et_tanggal_lahir_m.setText(sdf.format(myCalendar.getTime()));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
