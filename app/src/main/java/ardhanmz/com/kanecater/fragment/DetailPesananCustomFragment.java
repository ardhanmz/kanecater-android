package ardhanmz.com.kanecater.fragment;



import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budiyev.android.circularprogressbar.CircularProgressBar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.text.DecimalFormat;
import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.KomponenModel;
import ardhanmz.com.kanecater.response.AnakResponse;
import ardhanmz.com.kanecater.model.OrderCustomRequest;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.netutil.ResponseSignup;
import ardhanmz.com.kanecater.response.Example;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailPesananCustomFragment extends Fragment implements
        DialogMenuNasi.OnNasiClick,
        DialogLaukSatu.OnLaukSatu,
        DialogMenuLaukDua.OnLaukDuaClick,
        DialogMenuSayur.onSayurClick,
        DialogMenuBuah.OnBuahClick
{
    @BindView(R.id.date)TextView tanggal;
    ArrayList<AnakResponse> arrayList;
    @BindView(R.id.detail_pilihnasi)
    ImageButton nasi_custom;
    @BindView(R.id.detail_pilihsayur) ImageButton sayur_custom;
    @BindView(R.id.detail_pilihdessert) ImageButton dessert_custom;
    @BindView(R.id.detail_pilihlauk) ImageButton lauksatu_custom;
    @BindView(R.id.detail_pilihlauk2) ImageButton laukdua_custom;
    @BindView(R.id.kaloriProgressBar)
    CircularProgressBar kaloriProgressBar;
    @BindView(R.id.proteinProgressBar)
    CircularProgressBar proteinProgressBar;
    @BindView(R.id.lemakProgressBar)
    CircularProgressBar lemakProgressBar;
    @BindView(R.id.karboProgressBar)
    CircularProgressBar karboProgressBar;
    @BindView(R.id.nama_nasi) TextView tv_nama_nasi;
    @BindView(R.id.nama_lauksatu) TextView tv_nama_lauksatu;
    @BindView(R.id.nama_laukdua) TextView tv_nama_laukdua;
    @BindView(R.id.nama_sayur) TextView tv_nama_sayur;
    @BindView(R.id.nama_buah) TextView tv_nama_buah;
    @BindView(R.id.tv) TextView tv_kalori;
    @BindView(R.id.tv_protein) TextView tv_protein;
    @BindView(R.id.tv_lemak) TextView tv_lemak;
    @BindView(R.id.tv_karbo) TextView tv_karbo;
    @BindView(R.id.toolbar_detail)
    Toolbar toolbar;
    @BindView(R.id.max_kalori) TextView max_kalori;


    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private String idanak, tgl;
    private int idpaket;
    private int kalori_sayur = 0, kalori_nasi = 0, kalori_lauk1 = 0, kalori_lauk2 =0, kalori_buah=0, jumlah_kalori =0;
    private Integer tinggi_badan, berat_badan, berat_sayur = 0, berat_buah = 0, berat_nasi = 0, berat_lauksatu = 0, berat_laukdua=0;
    private int max_calori;
    private double akhir;
    private int beratProtein = 0;
    private Integer beratKarbo;
    private int beratLemak = 0;
    private String idOrder;
    private int idorder = 0;
    private String fk_idlauksatu , fk_idnasi, fk_idlaukdua, fk_idbuah, fk_idsayur;
    private int idnasi, idlauk1 , idlauk2, idsayur , idbuah;
    private int sum_order;
    private LinearLayout.LayoutParams parms;


    public DetailPesananCustomFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_pesanan_custom, container, false);
        ButterKnife.bind(this, view);
        arrayList = new ArrayList<>();
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int width = 300;
        int height = 300;
        parms = new LinearLayout.LayoutParams(width,height);
        tgl = getArguments().getString("tanggal","");
        idanak = getArguments().getString("idanak","");
        idpaket= getArguments().getInt("idpaket",0);
        idOrder = getArguments().getString("idorder", "");
        idorder = Integer.parseInt(idOrder);
        tanggal.setText(tgl);
        sum_order = getArguments().getInt("order",0);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
        menuButton();
        loadDataAnak();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detailpesanan, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.tambah_menu_custom:
                sendDataPesanan();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadDataAnak() {
        KomponenModel komponenModel = new KomponenModel();
        komponenModel.setIdpaket(0);
        compositeSubscription.add(NetworkUtil.getRetrofit().getAnak(idanak,komponenModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void handleResponse(Example anakResponse) {
        if (anakResponse!=null){
            tinggi_badan = anakResponse.getAnakresponse().getTinggiBadan();
            berat_badan = anakResponse.getAnakresponse().getBeratBadan();
            hitungGizi();
        } else {
            Toasty.warning(getActivity(), "Error Memuat Data Anak", Toasty.LENGTH_SHORT, true).show();
        }
    }

    private void onError(Throwable throwable) {
        //do Nothing
    }

    private void hitungGizi() {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        double t = tinggi_badan;
        double b = berat_badan;
        double t_m = t / 100;
        double pow = Math.pow(t_m, 2);
        double IMT = b / pow;
        int BMR = berat_badan * 24;
        double SDA = 0.1 * BMR;
        Double total = 1.7*(BMR+SDA);
        akhir = total / 3;
        String hasil_akhir = decimalFormat.format(akhir);
        max_calori = (int) Math.rint(akhir);
        String h = Double.toString(total);
        String wkwk = Double.toString(max_calori);
        max_kalori.setText(wkwk);
    }


    private void sendDataPesanan() {
        OrderCustomRequest orderCustomRequest = new OrderCustomRequest();
        if (sum_order == 2){
            orderCustomRequest.setFk_iddata_order(idorder);
            orderCustomRequest.setTanggal(tgl);
            orderCustomRequest.setFk_idmenu_nasi(idnasi);
            orderCustomRequest.setFk_idmenu_lauksatu(idlauk1);
            orderCustomRequest.setFk_idpaket(String.valueOf(idpaket));
            orderCustomRequest.setKalori(String.valueOf(jumlah_kalori));
            addOrder2Komponen(orderCustomRequest);
        } else if (sum_order==3){
            orderCustomRequest.setFk_iddata_order(idorder);
            orderCustomRequest.setTanggal(tgl);
            orderCustomRequest.setFk_idmenu_nasi(idnasi);
            orderCustomRequest.setFk_idmenu_lauksatu(idlauk1);
            orderCustomRequest.setFk_idmenu_laukdua(idlauk2);
            orderCustomRequest.setFk_idpaket(String.valueOf(idpaket));
            orderCustomRequest.setKalori(String.valueOf(jumlah_kalori));
            addOrder3Komponen(orderCustomRequest);
        }else if (sum_order==4){
            orderCustomRequest.setFk_iddata_order(idorder);
            orderCustomRequest.setTanggal(tgl);
            orderCustomRequest.setFk_idmenu_nasi(idnasi);
            orderCustomRequest.setFk_idmenu_lauksatu(idlauk1);
            orderCustomRequest.setFk_idmenu_laukdua(idlauk2);
            orderCustomRequest.setFk_idmenu_sayur(idsayur);
            orderCustomRequest.setFk_idpaket(String.valueOf(idpaket));
            orderCustomRequest.setKalori(String.valueOf(jumlah_kalori));
            addOrder4Komponen(orderCustomRequest);
        }else if (sum_order==5){
            orderCustomRequest.setFk_iddata_order(idorder);
            orderCustomRequest.setTanggal(tgl);
            orderCustomRequest.setFk_idmenu_nasi(idnasi);
            orderCustomRequest.setFk_idmenu_lauksatu(idlauk1);
            orderCustomRequest.setFk_idmenu_laukdua(idlauk2);
            orderCustomRequest.setFk_idmenu_sayur(idsayur);
            orderCustomRequest.setFk_idmenu_buah(idbuah);
            orderCustomRequest.setFk_idpaket(String.valueOf(idpaket));
            orderCustomRequest.setKalori(String.valueOf(jumlah_kalori));
            addOrderCustom(orderCustomRequest);
        }else{
            Toasty.info(getContext(), "Data Masakan Kosong, Harap Isi Terlebih Dahulu", Toasty.LENGTH_SHORT, true).show();
        }
    }

    private void addOrder3Komponen(OrderCustomRequest orderCustomRequest) {
        compositeSubscription.add(NetworkUtil.getRetrofit().addCustomOrder3(orderCustomRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void addOrder4Komponen(OrderCustomRequest orderCustomRequest) {
        compositeSubscription.add(NetworkUtil.getRetrofit().addCustomOrder4(orderCustomRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void addOrder2Komponen(OrderCustomRequest orderCustomRequest) {
        compositeSubscription.add(NetworkUtil.getRetrofit().addCustomOrder2(orderCustomRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void addOrderCustom(OrderCustomRequest orderCustomRequest) {
        compositeSubscription.add(NetworkUtil.getRetrofit().addCustomOrder(orderCustomRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void handleResponse(ResponseSignup responseSignup) {
        if (responseSignup.getSuccess()){
            Toasty.success(getActivity(), responseSignup.getMessage(), Toasty.LENGTH_SHORT, true).show();
            getFragmentManager().popBackStack();
        } else {
            Toasty.error(getActivity(), responseSignup.getMessage(), Toasty.LENGTH_SHORT, true).show();
        }
    }

    private void menuButton() {
        if (sum_order==2){
            nasi_custom.setVisibility(View.VISIBLE);
            lauksatu_custom.setVisibility(View.VISIBLE);
        }else if (sum_order==3){
            nasi_custom.setVisibility(View.VISIBLE);
            lauksatu_custom.setVisibility(View.VISIBLE);
            laukdua_custom.setVisibility(View.VISIBLE);
        }else if (sum_order==4){
            nasi_custom.setVisibility(View.VISIBLE);
            lauksatu_custom.setVisibility(View.VISIBLE);
            laukdua_custom.setVisibility(View.VISIBLE);
            sayur_custom.setVisibility(View.VISIBLE);
        }else if (sum_order==5){
            nasi_custom.setVisibility(View.VISIBLE);
            lauksatu_custom.setVisibility(View.VISIBLE);
            laukdua_custom.setVisibility(View.VISIBLE);
            sayur_custom.setVisibility(View.VISIBLE);
            dessert_custom.setVisibility(View.VISIBLE);
        }
        nasi_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showsDialogNasi();
            }
        });
        sayur_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogSayur();
            }
        });
        dessert_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showsDialogBuah();
            }
        });
        lauksatu_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showsDialogLaukSatu();
            }
        });
        laukdua_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showsDialogLaukDua();
            }
        });
    }

    private void showDialogSayur() {
        Bundle bundle = new Bundle();
        bundle.putString("idpaket", String.valueOf(idpaket));
        FragmentManager fm = getFragmentManager();
        DialogMenuSayur dialogMenuSayur = new DialogMenuSayur();
        dialogMenuSayur.setArguments(bundle);
        dialogMenuSayur.setTargetFragment(this, 1);
        dialogMenuSayur.show(fm, "Sayur");;
    }

    private void showsDialogBuah() {
        Bundle bundle = new Bundle();
        bundle.putString("idpaket", String.valueOf(idpaket));
        FragmentManager fm = getFragmentManager();
        DialogMenuBuah dialogMenuBuah = new DialogMenuBuah();
        dialogMenuBuah.setArguments(bundle);
        dialogMenuBuah.setTargetFragment(this, 1);
        dialogMenuBuah.show(fm, "Buah");
    }

    private void showsDialogLaukDua() {
        Bundle bundle = new Bundle();
        bundle.putString("idpaket", String.valueOf(idpaket));
        FragmentManager fm = getFragmentManager();
        DialogMenuLaukDua dialogMenuLaukDua = new DialogMenuLaukDua();
        dialogMenuLaukDua.setArguments(bundle);
        dialogMenuLaukDua.setTargetFragment(this, 1);
        dialogMenuLaukDua.show(fm, "LaukDua");
    }

    private void showsDialogLaukSatu() {
        Bundle bundle = new Bundle();
        bundle.putString("idpaket", String.valueOf(idpaket));
        FragmentManager fm = getFragmentManager();
        DialogLaukSatu dialogLaukSatu = new DialogLaukSatu();
        dialogLaukSatu.setArguments(bundle);
        dialogLaukSatu.setTargetFragment(this, 1);
        dialogLaukSatu.show(fm, "LaukSatu");
    }

    private void showsDialogNasi() {
        Bundle bundle = new Bundle();
        bundle.putString("idpaket", String.valueOf(idpaket));
        FragmentManager fm = getFragmentManager();
        DialogMenuNasi dialogMenuNasi = new DialogMenuNasi();
        dialogMenuNasi.setArguments(bundle);
        dialogMenuNasi.setTargetFragment(this, 1);
        dialogMenuNasi.show(fm, "NASI");
    }


    @Override
    public void onLaukData(int idlauksatu, String nama_lauksatu, int kalori, int berat, String image) {
        tv_nama_lauksatu.setText(nama_lauksatu);
        this.fk_idlauksatu = Integer.toString(idlauksatu);
        this.idlauk1 = idlauksatu;
        this.kalori_lauk1 = kalori;
        this.berat_lauksatu = berat;
        jumlah_kalori = kalori_nasi + kalori_lauk1 + kalori_lauk2 + kalori_sayur + kalori_buah;
        beratProtein = berat_lauksatu + berat_buah;
        if (jumlah_kalori > 0 ){
            if (jumlah_kalori <= max_calori ){
                kaloriProgressBar.setMaximum(max_calori);
                kaloriProgressBar.setProgress(jumlah_kalori);
                String jumlah = Integer.toString(jumlah_kalori);
                tv_kalori.setText(jumlah);
            }
            else {
                Toasty.warning(getActivity(), "Kalori Sudah Terpenuhi",Toasty.LENGTH_SHORT, true).show();
            }
        }
        if (beratProtein > 0){
            proteinProgressBar.setMaximum(100);
            proteinProgressBar.setProgress(beratProtein);
            String jumlah = Integer.toString(beratProtein);
            tv_protein.setText(jumlah);
        }

    }

    @Override
    public void onRetrieveData(int idnasi, String nama_nasi, Integer kalori, Integer berat) {
        tv_nama_nasi.setText(nama_nasi);
        this.kalori_nasi = kalori;
        this.fk_idnasi=Integer.toString(idnasi);
        this.idnasi=idnasi;
        this.berat_nasi = berat;
        beratKarbo = berat_nasi;
        jumlah_kalori = kalori_nasi + kalori_lauk1 + kalori_lauk2 + kalori_sayur + kalori_buah;
        if (jumlah_kalori > 0 ){
            if (jumlah_kalori <= max_calori ){
                kaloriProgressBar.setMaximum(max_calori);
                kaloriProgressBar.setProgress(jumlah_kalori);
                String jumlah = Integer.toString(jumlah_kalori);
                tv_kalori.setText(jumlah);
                if (beratKarbo > 0){
                    karboProgressBar.setMaximum(300);
                    karboProgressBar.setProgress(beratKarbo);
                    String karbo = Integer.toString(beratKarbo);
                    tv_karbo.setText(karbo);
                }
            }
            else {
                Toasty.warning(getActivity(), "Kalori Sudah Terpenuhi",Toasty.LENGTH_SHORT, true).show();
            }
        }
    }
    @Override
    public void onSayurFinished(int idsayur, String nama_sayur, int kalori, int berat) {
        tv_nama_sayur.setText(nama_sayur);
        this.kalori_sayur = kalori;
        this.fk_idsayur = Integer.toString(idsayur);
        this.berat_sayur = berat;
        this.idsayur=idsayur;
        beratLemak = berat_laukdua + berat_sayur;
        jumlah_kalori = kalori_nasi + kalori_lauk1 + kalori_lauk2 + kalori_sayur + kalori_buah;
        if (jumlah_kalori > 0 ){
            if (jumlah_kalori <= max_calori ){
                kaloriProgressBar.setMaximum(max_calori);
                kaloriProgressBar.setProgress(jumlah_kalori);
                String jumlah = Integer.toString(jumlah_kalori);
                tv_kalori.setText(jumlah);
            }
            else {
                Toasty.warning(getActivity(), "Kalori Sudah Terpenuhi",Toasty.LENGTH_SHORT, true).show();
            }
        }
        if (beratLemak > 0){
            proteinProgressBar.setMaximum(100);
            proteinProgressBar.setProgress(beratLemak);
            String jumlah = Integer.toString(beratLemak);
            tv_lemak.setText(jumlah);
        }
    }

    @Override
    public void onBuahFinished(int idsayur, String nama_sayur, Integer kalori, Integer berat) {
        tv_nama_buah.setText(nama_sayur);
        this.kalori_buah = kalori;
        this.fk_idbuah = Integer.toString(idsayur);
        this.idbuah=idsayur;
        this.berat_buah = berat;
        beratProtein = berat_lauksatu + berat_buah;
        jumlah_kalori = kalori_nasi + kalori_lauk1 + kalori_lauk2 + kalori_sayur + kalori_buah;
        if (jumlah_kalori > 0 ){
            if (jumlah_kalori <= max_calori ){
                kaloriProgressBar.setMaximum(max_calori);
                kaloriProgressBar.setProgress(jumlah_kalori);
                String jumlah = Integer.toString(jumlah_kalori);
                tv_kalori.setText(jumlah);
                if (beratProtein > 0){
                    proteinProgressBar.setMaximum(100);
                    proteinProgressBar.setProgress(beratProtein);
                    String protein = Integer.toString(beratProtein);
                    tv_protein.setText(protein);
                }
            }
            else {
                Toasty.warning(getActivity(), "Kalori Sudah Terpenuhi",Toasty.LENGTH_SHORT, true).show();
            }
        }

    }
    @Override
    public void onLaukDuaFinished(int idlaukdua, String nama_laudua, int kalori, int berat, String image) {
        tv_nama_laukdua.setText(nama_laudua);
        this.kalori_lauk2 = kalori;
        this.fk_idlaukdua = Integer.toString(idlaukdua);
        this.berat_laukdua = berat;
        this.idlauk2=idlaukdua;
        beratLemak = berat_laukdua + berat_sayur;
        jumlah_kalori = kalori_nasi + kalori_lauk1 + kalori_lauk2 + kalori_sayur + kalori_buah;
        Glide.with(getContext())
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        laukdua_custom.setLayoutParams(parms);
                        laukdua_custom.setImageDrawable(resource);
                    }
                });
        if (jumlah_kalori > 0 ){
            if (jumlah_kalori <= max_calori ){
                kaloriProgressBar.setMaximum(max_calori);
                kaloriProgressBar.setProgress(jumlah_kalori);
                String jumlah = Integer.toString(jumlah_kalori);
                tv_kalori.setText(jumlah);
            }
            else {
                Toasty.warning(getActivity(), "Kalori Sudah Terpenuhi",Toasty.LENGTH_SHORT, true).show();
            }
        }
        if (beratLemak > 0){
            proteinProgressBar.setMaximum(100);
            proteinProgressBar.setProgress(beratLemak);
            String jumlah = Integer.toString(beratLemak);
            tv_lemak.setText(jumlah);
        }
    }


}
