package ardhanmz.com.kanecater.fragment;


import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.adapter.CustomLaukDuaAdapter;
import ardhanmz.com.kanecater.adapter.CustomSayurAdapter;
import ardhanmz.com.kanecater.model.LaukDuaCustomModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.response.MenuLaukDuaResponse;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogMenuLaukDua extends DialogFragment {
    @BindView(R.id.rv_laukdua)
    RecyclerView rv_laukdua;
    ArrayList<MenuLaukDuaResponse> arrayList;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private CustomLaukDuaAdapter adapter;
    private RecyclerELEAdapter RAdapter;
    private Integer idnasi1;
    private Integer idmasak;
    private String idmasakan;
    private OnLaukDuaClick callback;
    private Integer idlaukdua1;


    public DialogMenuLaukDua() {
        // Required empty public constructor
    }

    public interface OnLaukDuaClick {
        public void onLaukDuaFinished(int idlaukdua, String nama_laudua, int kalori, int berat, String image_laukdua);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            callback = (OnLaukDuaClick) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling Fragment must implement OnAddFriendListener");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_lauk_dua_dialog, container, false);
        ButterKnife.bind(this, v);
        arrayList = new ArrayList<>();
        idmasakan = getArguments().getString("idpaket","");
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        initAction();
        loadRV();
        return v;
    }

    private void loadRV() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        rv_laukdua.setLayoutManager(mLayoutManager);
        rv_laukdua.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        rv_laukdua.setItemAnimator(new DefaultItemAnimator());
        View loadingView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvloading, rv_laukdua, false);
        View errorView = getActivity().getLayoutInflater().inflate(R.layout.layout_rv_error, rv_laukdua, false);
        View emptyView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvempty, rv_laukdua, false);
        adapter = new CustomLaukDuaAdapter(getActivity(), arrayList);
        RAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
        rv_laukdua.setAdapter(adapter);
        rv_laukdua.setHasFixedSize(true);
        rv_laukdua.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                int idlaukdua1 = arrayList.get(position).getIdmenuLaukdua();
                idmasak = arrayList.get(position).getFkIdPaket();
                String nama_laukdua = arrayList.get(position).getNamaMenu();
                int kalori1 = arrayList.get(position).getKalori();
                int berat1 = arrayList.get(position).getBerat();
                String image_menu = arrayList.get(position).getImageMenu();
                callback.onLaukDuaFinished(idlaukdua1, nama_laukdua, kalori1, berat1, image_menu);
                dismiss();
            }
        }));
    }
    private void initAction() {
        compositeSubscription.add(NetworkUtil.getRetrofit().getLaukDuaByMasakan(idmasakan)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::onError));
    }


    private void onError(Throwable throwable) {
    }

    private void handleResponse(ArrayList<MenuLaukDuaResponse> list) {
        if (list != null){
            arrayList.clear();
            arrayList.addAll(list);
            RAdapter.notifyDataSetChanged();
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_NORMAL);
        } else {
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_EMPTY);
        }
    }
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }
    }
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
