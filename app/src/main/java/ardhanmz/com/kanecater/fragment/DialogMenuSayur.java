package ardhanmz.com.kanecater.fragment;


import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.adapter.CustomSayurAdapter;
import ardhanmz.com.kanecater.model.SayurCustomModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.response.MenuSayurResponse;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogMenuSayur extends DialogFragment {
    @BindView(R.id.rv_sayur) RecyclerView rv_sayur;
    ArrayList<MenuSayurResponse> arrayList;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private CustomSayurAdapter adapter;
    private RecyclerELEAdapter RAdapter;
    private Integer idsayur1;
    private Integer idmasak;
    private String idmasakan;
    private onSayurClick callback;

    public DialogMenuSayur() {
        // Required empty public constructor
    }

    public interface onSayurClick {
        public void onSayurFinished(int idsayur, String nama_sayur, int kalori, int berat);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            callback = (onSayurClick) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling Fragment must implement OnAddFriendListener");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sayur_custom, container, false);
        ButterKnife.bind(this, v);
        arrayList = new ArrayList<>();
        idmasakan = getArguments().getString("idpaket","");
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        initAction();
        loadRV();
        return v;
    }
    private void loadRV() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        rv_sayur.setLayoutManager(mLayoutManager);
        rv_sayur.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        rv_sayur.setItemAnimator(new DefaultItemAnimator());
        View loadingView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvloading, rv_sayur, false);
        View errorView = getActivity().getLayoutInflater().inflate(R.layout.layout_rv_error, rv_sayur, false);
        View emptyView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvempty, rv_sayur, false);
        adapter = new CustomSayurAdapter(getContext(), arrayList);
        RAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
        rv_sayur.setAdapter(adapter);
        rv_sayur.setHasFixedSize(true);
        rv_sayur.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Integer idsayur1 = arrayList.get(position).getIdmenuSayur();
                idmasak = arrayList.get(position).getFkIdPaket();
                String nama_sayur = arrayList.get(position).getNamaMenu();
                Integer kalori1 = Integer.valueOf(arrayList.get(position).getKalori());
                Integer berat1 = Integer.valueOf(arrayList.get(position).getBerat());
                String idnasi = Integer.toString(idsayur1);
                String idkalori = Integer.toString(kalori1);
                callback.onSayurFinished(idsayur1, nama_sayur, kalori1, berat1 );
                dismiss();
            }
        }));
    }
    private void initAction() {
        compositeSubscription.add(NetworkUtil.getRetrofit().getSayurByMasakan(idmasakan)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::onError));
    }
    private void onError(Throwable throwable) {
    }

    private void handleResponse(ArrayList<MenuSayurResponse> list) {
        if (list != null){
            arrayList.clear();
            arrayList.addAll(list);
            RAdapter.notifyDataSetChanged();
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_NORMAL);
        } else {
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_EMPTY);
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }
    }
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
