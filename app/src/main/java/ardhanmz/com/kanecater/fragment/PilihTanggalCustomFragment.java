package ardhanmz.com.kanecater.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.CheckOrderModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.response.CheckOrderResponse;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * A simple {@link Fragment} subclass.
 */
public class PilihTanggalCustomFragment extends Fragment {
    @BindView(R.id.calendar_custom)
    CalendarView calendarView;
    @BindView(R.id.nama_data_anak)
    TextView nama_data;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.detail_custom) Button detail_custom;
    @BindView(R.id.button_pembayaran) Button pembayaran;
    @BindView(R.id.nasi_datecustom) TextView nasi_custom;
    @BindView(R.id.lauksatu_datecustom) TextView lauksatu_custom;
    @BindView(R.id.laukdua_datecustom) TextView laukdua_custom;
    @BindView(R.id.sayur_datecustom) TextView sayur_custom;
    @BindView(R.id.buah_datecustom) TextView buah_custom;
    @BindView(R.id.kalori_datecustom) TextView kalori_custom;
    @BindView(R.id.linear_custommenu)
    LinearLayout linearLayout;
    @BindView(R.id.kalori_linear) LinearLayout kalori_lin;
    long minDate, maxDate;
    String date,anak;
    Integer idpengguna;
    private int idpaket;
    private String idorder;
    private int jumlah_order;
    private CompositeSubscription compositeSubscription;
    private boolean lihat_order;
    ViewDialog viewDialog;
    private int harga;

    public PilihTanggalCustomFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pilih_tanggal_custom, container, false);
        ButterKnife.bind(this, v);
        viewDialog = new ViewDialog(getActivity());
        String nama_anak = getArguments().getString("nama_anak", "");
        String anak = getArguments().getString("idanak", "");
        lihat_order = getArguments().getBoolean("lihat_order", false);
        harga = getArguments().getInt("harga");
        if (lihat_order){
            toolbar.setTitle("Paket Custom");

        }else {
            linearLayout.setVisibility(View.GONE);
            kalori_lin.setVisibility(View.VISIBLE);
        }
        nama_data.setText(nama_anak);
        idorder = getArguments().getString("idorder", "");
        String strdate = getArguments().getString("start_date");
        String strdate2 = getArguments().getString("end_date");
        jumlah_order = getArguments().getInt("order");
        compositeSubscription = new CompositeSubscription();
        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date newdate = dateformat.parse(strdate);
            Date newdate2 = dateformat.parse(strdate2);
            minDate = newdate.getTime();
            maxDate = newdate2.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendarView.setMinDate(minDate);
        calendarView.setMaxDate(maxDate+1);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {

                if (i1<10){
                    date = (i + "-0"+(i1+1)+ "-"+i2);
                }else {
                    date = (i + "-" +(i1+1)+ "-"+i2);
                }
                if (lihat_order){
                    viewDialog.showDialog();
                    CheckOrderModel checkOrderModel = new CheckOrderModel();
                    checkOrderModel.setIddata_order(idorder);
                    checkOrderModel.setTanggal(date);
                    compositeSubscription.add(NetworkUtil.getRetrofit().checkOrderCustom(checkOrderModel)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(this::handleResponse,this::onError));
                    kalori_lin.setVisibility(View.VISIBLE);
                }else {
                    Toasty.info(getContext(), "Anda Memilih Tanggal="+date ,Toasty.LENGTH_SHORT, true).show();
                    linearLayout.setVisibility(View.GONE);

                }




            }

            private void onError(Throwable throwable) {
            }

            private void handleResponse(CheckOrderResponse checkOrderResponse) {
                viewDialog.hideDialog();
                if (checkOrderResponse!=null){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (checkOrderResponse.getNASI()!=null){
                                    nasi_custom.setText(checkOrderResponse.getNASI());
                                }
                                if (checkOrderResponse.getLAUKSATU()!=null){
                                    lauksatu_custom.setText(checkOrderResponse.getLAUKSATU());
                                }
                                if (checkOrderResponse.getLAUKDUA()!=null){
                                    laukdua_custom.setText(checkOrderResponse.getLAUKDUA());
                                }if (checkOrderResponse.getSAYUR()!=null){
                                    sayur_custom.setText(checkOrderResponse.getSAYUR());
                                }if (checkOrderResponse.getBUAH()!=null){
                                    laukdua_custom.setText(checkOrderResponse.getBUAH());
                                }
                                if (checkOrderResponse.getKalori()!=null){
                                    kalori_custom.setText(checkOrderResponse.getKalori());
                                }

                                if (lihat_order){
                                    detail_custom.setVisibility(View.GONE);
                                    pembayaran.setVisibility(View.GONE);
                                }
                            } catch (WindowManager.BadTokenException e) {
                                Log.e("WindowManagerBad ", e.toString());
                            }

                        }
                    });
                }else {
                    Toasty.warning(getContext(), "KESALAHAN MEMUAT DATA", Toasty.LENGTH_SHORT, true).show();
                }

            }
        });
        detail_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (date!=null){
                    idpaket = getArguments().getInt("idpaket",0);
                    idpengguna = getArguments().getInt("idpengguna",0);
                    Bundle bundle = new Bundle();
                    bundle.putInt("idpaket", idpaket);
                    bundle.putString("tanggal", date);
                    bundle.putString("idanak", anak);
                    bundle.putString("idorder", idorder);
                    bundle.putInt("order", jumlah_order);
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    DetailPesananCustomFragment detailPesananCustomFragment = new DetailPesananCustomFragment();
                    detailPesananCustomFragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.fragframe, detailPesananCustomFragment).addToBackStack(null).commit();
                }else{
                    Toasty.error(getContext(), "Anda Belum Memilih Tanggal", Toasty.LENGTH_SHORT,true).show();
                }
            }
        });
        pembayaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("idorder", idorder);
                bundle.putInt("harga",harga);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                PembayaranFragment pembayaranFragment = new PembayaranFragment();
                pembayaranFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.fragframe, pembayaranFragment).addToBackStack(null).commit();
            }
        });
        return  v;
    }

}
