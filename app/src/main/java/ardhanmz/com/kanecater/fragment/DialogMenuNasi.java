package ardhanmz.com.kanecater.fragment;


import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.adapter.CustomNasiAdapter;
import ardhanmz.com.kanecater.response.MenuNasiResponse;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogMenuNasi extends DialogFragment {
//    @BindView(R.id.tai)
//    TextView gaplek;
    @BindView(R.id.rv_nasi_custom)
    RecyclerView rv_nasi;
    ArrayList<MenuNasiResponse> arrayList;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private CustomNasiAdapter adapter;
    private RecyclerELEAdapter RAdapter;
    private Integer idnasi1;
    private String idmasak;
    private String idmasakan;
    private OnNasiClick callback;
    public DialogMenuNasi() {
        // Required empty public constructor
    }
    public interface OnNasiClick {
        public void onRetrieveData(int idnasi, String nama_nasi, Integer kalori, Integer berat);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            callback = (OnNasiClick) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling Fragment must implement OnAddFriendListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pilih_nasi_custom, container, false);
        ButterKnife.bind(this, view);
        arrayList = new ArrayList<>();
        idmasakan = getArguments().getString("idpaket","");
//        gaplek.setText(pusing);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        initAction();
        loadRV();
        return view;
    }

    private void loadRV() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        rv_nasi.setLayoutManager(mLayoutManager);
        rv_nasi.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        rv_nasi.setItemAnimator(new DefaultItemAnimator());
        View loadingView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvloading, rv_nasi, false);
        View errorView = getActivity().getLayoutInflater().inflate(R.layout.layout_rv_error, rv_nasi, false);
        View emptyView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvempty, rv_nasi, false);
        adapter = new CustomNasiAdapter(getContext(), arrayList);
        RAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
        rv_nasi.setAdapter(adapter);
        rv_nasi.setHasFixedSize(true);
        rv_nasi.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                idnasi1 = arrayList.get(position).getIdnasi();
                idmasak = arrayList.get(position).getFkIdmasakan();
                String nama_nasi = arrayList.get(position).getNamaNasi();
                int kalori1 = arrayList.get(position).getKalori();
                int berat1 = arrayList.get(position).getBerat();
                String idnasi = Integer.toString(idnasi1);
                String idkalori = Integer.toString(kalori1);
                callback.onRetrieveData(idnasi1, nama_nasi, kalori1, berat1);
                dismiss();
//                Bundle bundle = new Bundle();
//                bundle.putString("idnasi", idnasi);
//                bundle.putString("kalori_nasi", idkalori);
//                bundle.putString("berat_nasi", berat1);
//                bundle.putString("nama_nasi", nama_nasi);


//                /*Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();*/
//                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                DetailPesananCustomFragment detailPesananCustomFragment = new DetailPesananCustomFragment();
//                detailPesananCustomFragment.setArguments(bundle);
//                fragmentTransaction.replace(R.id.fragframe,detailPesananCustomFragment);
//                fragmentTransaction.commit();
            }
        }));
    }

    private void initAction() {
        compositeSubscription.add(NetworkUtil.getRetrofit().getNasiByMasakan(idmasakan)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void onError(Throwable throwable) {
    }

    private void handleResponse(ArrayList<MenuNasiResponse> list) {
        if (list != null){
            arrayList.clear();
            arrayList.addAll(list);
            RAdapter.notifyDataSetChanged();
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_NORMAL);
        } else {
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_EMPTY);
        }
    }
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }
    }
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
