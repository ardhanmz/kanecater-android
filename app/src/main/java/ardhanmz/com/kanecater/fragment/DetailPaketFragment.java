package ardhanmz.com.kanecater.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.acitivities.Pemesanan;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.response.DetailPaketLanggananResponse;
import ardhanmz.com.kanecater.response.Example;
import ardhanmz.com.kanecater.response.ResponseAVG;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailPaketFragment extends Fragment {
    @BindView(R.id.nama_paket_langganan)
    TextView nama_paket;
    @BindView(R.id.deskripsi_paket) TextView deskripsi_paket;
    @BindView(R.id.harga_paket) TextView harga_paket;
    @BindView(R.id.rating_paket)
    RatingBar rating_paket;
    @BindView(R.id.tv_kal1) TextView tv_kal1;
    @BindView(R.id.tv_karb1) TextView tv_karb1;
    @BindView(R.id.tv_pro1) TextView tv_pro1;
    @BindView(R.id.tv_lem1) TextView tv_lem1;
    @BindView(R.id.appBarLayout)
    Toolbar toolbar;
    @BindView(R.id.image_detailpaket)
    ImageView imageView;


    private CompositeSubscription compositeSubscription;
    private CompositeSubscription compositeSubscription1;
    private CompositeSubscription compositeSubscription2;
    private CompositeSubscription compositeSubscription3;
    private CompositeSubscription compositeSubscription4;
    private SharedPreferences sharedPreferences;
    private Bundle extras;
    private FragmentActivity listener;
    private String id;
    private String namaPaket;
    private String hargaPaket;
    private int idpaket, karbohidrat, protein, kalori, lemak;
    private int kalori_nasi, kalori_lauksatu, kalori_laukdua, kalori_sayur, kalori_buah;


    public DetailPaketFragment() {
        // Required empty public constructor
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            this.listener = (FragmentActivity) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_paket, container, false);
        ButterKnife.bind(this, view);
        compositeSubscription = new CompositeSubscription();
        compositeSubscription1 = new CompositeSubscription();
        compositeSubscription2 = new CompositeSubscription();
        compositeSubscription3 = new CompositeSubscription();
        compositeSubscription4 = new CompositeSubscription();
        id = getArguments().getString("id","");
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
        startAction();
        return view;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detailpaket, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.pesan_paket:
                sendDataPesanan();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sendDataPesanan() {
        Intent intent = new Intent(getActivity(), Pemesanan.class);
        Bundle bundle = new Bundle();
        bundle.putInt("idpaket",idpaket);
        bundle.putBoolean("langganan", true);
        bundle.putInt("kalori", kalori);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void startAction() {
//        extras = getArguments();
//        idpaket = extras.getInt("data",0);
        compositeSubscription.add(NetworkUtil.getRetrofit().getPaketById(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void handleResponse(Example example) {
        if(example!=null){
            idpaket = example.getPaket().getIdPaket();
            namaPaket = example.getPaket().getNamaPaket();
            hargaPaket = example.getPaket().getHargaPaket();
            nama_paket.setText(namaPaket);
            harga_paket.setText(hargaPaket);
            deskripsi_paket.setText(example.getPaket().getDeskripsiPaket());
            Glide.with(getActivity())
                    .load(example.getPaket().getImagePaket())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(imageView);
            kalori_nasi = (int) Math.round(example.getNasi().getKalori());
            kalori_sayur = (int) Math.round(example.getSayur().getKalori());
            kalori_lauksatu = (int) Math.round(example.getLauksatu().getKalori());
            kalori_laukdua = (int) Math.round(example.getLaukdua().getKalori());
            lemak = example.getSayur().getLemak();
            karbohidrat = example.getNasi().getKarbohidrat();
            long prot = Math.round(example.getLaukdua().getProtein() + example.getLauksatu().getProtein());
            tv_pro1.setText(String.valueOf(prot));
            tv_karb1.setText(String.valueOf(karbohidrat));
            tv_lem1.setText(String.valueOf(lemak));
            kalori = kalori_nasi + kalori_lauksatu + kalori_laukdua + kalori_sayur + kalori_buah;
            tv_kal1.setText(String.valueOf(kalori));

        }
    }

    private void onError(Throwable throwable) {
    }

    private void handleResponse(DetailPaketLanggananResponse detailPaketModel) {
        if (detailPaketModel != null){
            idpaket = detailPaketModel.getIdPaket();
            namaPaket = detailPaketModel.getNamaPaket();
            hargaPaket = detailPaketModel.getHargaPaket();
            nama_paket.setText(detailPaketModel.getNamaPaket());
            harga_paket.setText(detailPaketModel.getHargaPaket());
            deskripsi_paket.setText(detailPaketModel.getDeskripsiPaket());
            Glide.with(getActivity())
                    .load(detailPaketModel.getImagePaket())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(imageView);
        }
        compositeSubscription.clear();
    }
    private void loadNasiAvg() {

    }

    private void handleResponse(ResponseAVG responseAVG) {
        if (Integer.valueOf(responseAVG.getJumlah())==4){
            Double kaloris = Double.parseDouble(responseAVG.getKalori());
            kalori_nasi = (int) Math.round(kaloris);
            karbohidrat = responseAVG.getKarbohidrat();
            kalori = kalori_nasi + kalori_lauksatu + kalori_laukdua + kalori_sayur + kalori_buah;
            loadLaukSatuAvg();
        }
        if (Integer.valueOf(responseAVG.getJumlah())==46){
            Double kaloris = Double.parseDouble(responseAVG.getKalori());
            kalori_lauksatu = (int) Math.round(kaloris);
            protein = (int) Math.round(responseAVG.getProtein());
            kalori = kalori_nasi + kalori_lauksatu + kalori_laukdua + kalori_sayur + kalori_buah;
            loadLaukDuaAvg();
        }
        if (Integer.valueOf(responseAVG.getJumlah())==32){
            Double kaloris = Double.parseDouble(responseAVG.getKalori());
            kalori_laukdua = (int) Math.round(kaloris);
            protein = (int) Math.round(responseAVG.getProtein());
            kalori = kalori_nasi + kalori_lauksatu + kalori_laukdua + kalori_sayur + kalori_buah;
            loadSayurAvg();
        }if (Integer.valueOf(responseAVG.getJumlah())==27){
            Double kaloris = Double.parseDouble(responseAVG.getKalori());
            kalori_sayur = (int) Math.round(kaloris);
            protein = (int) Math.round(responseAVG.getProtein());
            kalori = kalori_nasi + kalori_lauksatu + kalori_laukdua + kalori_sayur + kalori_buah;
            tv_kal1.setText(kalori);
            tv_karb1.setText(karbohidrat);
            tv_pro1.setText(protein);
            tv_lem1.setText(lemak);
            compositeSubscription4.clear();
        }
        compositeSubscription.clear();
    }

    private void loadSayurAvg() {
            compositeSubscription4.add(NetworkUtil.getRetrofit().getLanggananSayurAvg()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::onError));

    }

    private void loadLaukDuaAvg() {
        compositeSubscription3.add(NetworkUtil.getRetrofit().getLanggananLaukDuaAvg()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void loadLaukSatuAvg() {
        compositeSubscription2.add(NetworkUtil.getRetrofit().getLanggananLaukSatuAvg()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
