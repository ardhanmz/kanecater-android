package ardhanmz.com.kanecater.fragment;


import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.adapter.CustomBuahAdapter;
import ardhanmz.com.kanecater.model.BuahCustomModel;
import ardhanmz.com.kanecater.model.SayurCustomModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogMenuBuah extends DialogFragment {
    @BindView(R.id.rv_buah)
    RecyclerView rv_buah;
    ArrayList<BuahCustomModel> arrayList;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private CustomBuahAdapter adapter;
    private RecyclerELEAdapter RAdapter;
    private Integer idbuah1;
    private Integer idmasak;
    private String idmasakan;
    private OnBuahClick callback;


    public interface OnBuahClick {
        public void onBuahFinished(int idbuah, String nama_sayur, Integer kalori, Integer berat);
    }


    public DialogMenuBuah() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_buah_custom_dialog, container, false);
        ButterKnife.bind(this, view);
        arrayList = new ArrayList<>();
        idmasakan = getArguments().getString("idpaket","");
//        gaplek.setText(pusing);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        initAction();
        loadRV();
        return view;
    }
    private void loadRV() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        rv_buah.setLayoutManager(mLayoutManager);
        rv_buah.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        rv_buah.setItemAnimator(new DefaultItemAnimator());
        View loadingView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvloading, rv_buah, false);
        View errorView = getActivity().getLayoutInflater().inflate(R.layout.layout_rv_error, rv_buah, false);
        View emptyView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvempty, rv_buah, false);
        adapter = new CustomBuahAdapter(getContext(), arrayList);
        RAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
        rv_buah.setAdapter(adapter);
        rv_buah.setHasFixedSize(true);
        rv_buah.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                idbuah1 = arrayList.get(position).getIdbuah();
                idmasak = arrayList.get(position).getFkIdmasakan();
                String nama_nasi = arrayList.get(position).getNamaBuah();
                Integer kalori1 = arrayList.get(position).getKalori();
                Integer berat1 = arrayList.get(position).getBerat();
                String idnasi = Integer.toString(idbuah1);
                String idkalori = Integer.toString(kalori1);
                callback.onBuahFinished(idbuah1, nama_nasi, kalori1, berat1);
                dismiss();
            }
        }));
    }
    private void initAction() {
        compositeSubscription.add(NetworkUtil.getRetrofit().getBuahByMasakan(idmasakan)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::onError));
    }

    private void onError(Throwable throwable) {
    }

    private void handleResponse(ArrayList<BuahCustomModel> buahCustomModels) {
        if (buahCustomModels != null){
            arrayList.clear();
            arrayList.addAll(buahCustomModels);
            RAdapter.notifyDataSetChanged();
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_NORMAL);
        } else {
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_EMPTY);
        }
    }


    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }
    }
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
