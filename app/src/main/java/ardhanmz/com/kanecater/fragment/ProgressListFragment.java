package ardhanmz.com.kanecater.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.adapter.ProgressOrderAdapter;
import ardhanmz.com.kanecater.model.OrderRequest;
import ardhanmz.com.kanecater.model.ProgressOrderModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.netutil.ResponseSignup;
import ardhanmz.com.kanecater.utils.Constants;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;


public class ProgressListFragment extends Fragment {
    @BindView(R.id.rv_progressorder)
    RecyclerView progresslist;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    ArrayList<ProgressOrderModel> arrayList;
    ProgressOrderAdapter adapter;
    RecyclerELEAdapter recyclerELEAdapter;
    private int pos_delete;
    ViewDialog viewDialog;

    public ProgressListFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_progress_list, container, false);
        ButterKnife.bind(this, v);
        viewDialog = new ViewDialog(getActivity());
        arrayList = new ArrayList<>();
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        loadRecycler();
        return v;
    }

    private void loadRecycler() {
        progresslist.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        adapter = new ProgressOrderAdapter(getActivity(), arrayList);
        progresslist.setHasFixedSize(true);
        progresslist.setAdapter(adapter);
        adapter.setOnItemClickListener(new ProgressOrderAdapter.onClickListener() {
            @Override
            public void onDeleteListener(int position) {
                cancelOrder(position);
                pos_delete = position;
            }

            @Override
            public void onHowPayListener(int position) {
                Integer id = 0;
                String harga_paket = null, metode_pembayaran = null;
                for (int i=0; i<=arrayList.size();i++){
                    if (i==position){
                        id = arrayList.get(i).getIddataOrder();
                        harga_paket = arrayList.get(i).getHargaPaket();
                        metode_pembayaran = arrayList.get(i).getMetodePembayaran();
                    }
                }
                Bundle bundle = new Bundle();
                bundle.putString("idorder", String.valueOf(id));
                bundle.putString("harga", harga_paket);
                bundle.putString("metode", metode_pembayaran);
                FragmentManager fragmentManager = getFragmentManager();
                HowToFragment howToFragment = new HowToFragment();
                howToFragment.setArguments(bundle);
                howToFragment.show(fragmentManager,"HOWTO");
            }
        });
        initAction();
    }
    public void cancelOrder(int position){
        viewDialog.showDialog();
        Integer id=0;
        for (int i = 0; i<=arrayList.size();i++){
            if (i==position){
                id = arrayList.get(i).getIddataOrder();
            }
        }
        String idorder = id.toString(id);
        String status = "dibatalkan";
        String status_order = "Tidak Aktif";
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setIddata_order(idorder);
        orderRequest.setStatus_pembayaran(status_order);
        orderRequest.setStatus_pembayaran(status);
        compositeSubscription.add(NetworkUtil.getRetrofit().cancelOrder(orderRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void handleResponse(ResponseSignup responseSignup) {
        viewDialog.hideDialog();
        if (responseSignup.getSuccess()){
            arrayList.remove(pos_delete);
            adapter.notifyDataSetChanged();
        }
    }

    private void initAction() {
        loadDataProgress();
    }

    private void loadDataProgress() {
        viewDialog.showDialog();
        Integer wow = sharedPreferences.getInt(Constants.IDPENGGUNA, 0);
        String id = Integer.toString(wow);
        compositeSubscription.add(NetworkUtil.getRetrofit().getOrderBelumDibayar(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void onError(Throwable throwable) {
    }

    private void handleResponse(ArrayList<ProgressOrderModel> progressOrderModels) {
        viewDialog.hideDialog();
        if (progressOrderModels!=null){
            arrayList.clear();
            arrayList.addAll(progressOrderModels);
            adapter.notifyDataSetChanged();
        } else {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
