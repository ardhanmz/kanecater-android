package ardhanmz.com.kanecater.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.adapter.SearchAdapter;
import ardhanmz.com.kanecater.response.SearchResponse;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabSearchFragment extends BaseFragment implements SearchView.OnQueryTextListener {
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private ArrayList<SearchResponse> searchResponses;
    private SearchAdapter searchAdapter;
    RecyclerELEAdapter recyclerELEAdapter;
    private Context mContext;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private MenuItem itemsearch;

    public TabSearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_search, container, false);
        ButterKnife.bind(this, view);
        compositeSubscription = new CompositeSubscription();
        mContext = getActivity();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
        return view;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_pencarian, menu);
        itemsearch = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(itemsearch);
        searchView.setOnQueryTextListener(this);
        MenuItemCompat.setOnActionExpandListener(itemsearch, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return true;
            }
        });
    }
    @Override
    public boolean onQueryTextSubmit(String query) {
        searchString(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
    private void searchString(String query){
        Bundle bundle = new Bundle();
        bundle.putString("query", query);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        PaketLanggananFragment paketLanggananFragment = new PaketLanggananFragment();
        paketLanggananFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frame_search,paketLanggananFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


}
