package ardhanmz.com.kanecater.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.acitivities.ManajemenAnakActivity;
import ardhanmz.com.kanecater.acitivities.SignInActivity;
import ardhanmz.com.kanecater.adapter.DataAnakAdapter;
import ardhanmz.com.kanecater.response.AnakResponse;
import ardhanmz.com.kanecater.response.DataAnakResponse;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.utils.Constants;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabProfileFragment extends BaseFragment {
    @BindView(R.id.img_ortu)
    ImageView imgortu;
    @BindView(R.id.nama_ortu)
    TextView namaortu;
    @BindView(R.id.recycler_data_anak)
    RecyclerView data_anak;
    @BindView(R.id.tambah_anak) TextView tambahanak;
    @BindView(R.id.logout) TextView logout;
    @BindView(R.id.edit_imgbtn)
    ImageButton edit_imgbtn;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    ArrayList<AnakResponse> anaklist;
    private DataAnakAdapter adapter;
    RecyclerELEAdapter RAdapter;
    private Context mContext;
    ViewDialog viewDialog;

    public TabProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_profile, container, false);
        ButterKnife.bind(this, view);
        compositeSubscription = new CompositeSubscription();
        mContext = getContext();
        viewDialog = new ViewDialog(getActivity());
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        anaklist = new ArrayList<>();
        String ortu = sharedPreferences.getString(Constants.USERNAME,"");
        namaortu.setText(ortu);
        initializeRV();
        initializeProfile();
        clickHandler();
        return view;
    }

    private void initializeProfile() {
        viewDialog.showDialog();
        Integer iduser = sharedPreferences.getInt(Constants.IDPENGGUNA, 0);
        String id = Integer.toString(iduser);
        compositeSubscription.add(NetworkUtil.getRetrofit().getAnakByUser(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void initializeRV() {
        adapter = new DataAnakAdapter(mContext, anaklist);
        data_anak.setHasFixedSize(true);
        data_anak.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        View loadingView = getLayoutInflater().inflate(R.layout.layout_rvloading, data_anak, false);
        View errorView = getLayoutInflater().inflate(R.layout.layout_rv_error, data_anak, false);
        View emptyView = getLayoutInflater().inflate(R.layout.layout_rvempty, data_anak, false);
        RAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
        data_anak.setAdapter(RAdapter);
    }

    private void clickHandler() {
        tambahanak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambahAnak();
            }
        });
        data_anak.addOnItemTouchListener(new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(mContext, ManajemenAnakActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("args", "lihat_anak");
                bundle.putInt("idanak", anaklist.get(position).getIdanak());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }));
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder (getContext())
                        .setIcon (android.R.drawable.ic_dialog_alert)
                        .setMessage ("Apakah anda YAKIN untuk keluar akun ini ? ")
                        .setPositiveButton ("Ya", new DialogInterface.OnClickListener ()
                        {
                            @Override
                            public void onClick (DialogInterface dialog, int which) {
                                logoutUser();
                            }
                        })
                        .setNegativeButton ("Tidak", null)
                        .show ();
            }
        });
        edit_imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                EditProfileFragment editProfileFragment = new EditProfileFragment();
                fragmentTransaction.add(R.id.content_frame, editProfileFragment).addToBackStack(null).commit();
            }
        });
    }
    private void handleResponse(ArrayList<AnakResponse> dataAnakResponses) {
        viewDialog.hideDialog();
        if(dataAnakResponses!=null){
            anaklist.clear();
            anaklist.addAll(dataAnakResponses);
            RAdapter.notifyDataSetChanged();
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_NORMAL);
        }
        else {
            RAdapter.notifyDataSetChanged();
            RAdapter.setCurrentView(RecyclerELEAdapter.VIEW_EMPTY);
        }
    }
    private void onError(Throwable throwable){

    }
    private void tambahAnak() {
        Intent intent = new Intent(mContext, ManajemenAnakActivity.class);
        Bundle bundle = new Bundle();
        intent.putExtra("args", "tambah_anak");
        startActivity(intent);
    }
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent intent = new Intent(getContext(), SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
