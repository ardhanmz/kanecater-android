package ardhanmz.com.kanecater.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.adapter.ListOrderAdapter;
import ardhanmz.com.kanecater.adapter.StatusOrderAdapter;
import ardhanmz.com.kanecater.model.ListOrderModel;
import ardhanmz.com.kanecater.model.StatusOrderModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.utils.Constants;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

public class TransaksiListFragment extends Fragment {
    @BindView(R.id.rv_status)
    RecyclerView rv_status;
    private ArrayList<StatusOrderModel> arrayList;
    private StatusOrderAdapter adapter;
    private RecyclerELEAdapter recyclerELEAdapter;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;


    public TransaksiListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_transaksi_list, container, false);
        ButterKnife.bind(this, view);
        arrayList = new ArrayList<>();
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        loadRecycler();
        return view;
    }

    private void loadRecycler() {
        rv_status.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        View loadingView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvloading, rv_status, false);
        View errorView = getActivity().getLayoutInflater().inflate(R.layout.layout_rv_error, rv_status, false);
        View emptyView = getActivity().getLayoutInflater().inflate(R.layout.layout_rvempty, rv_status, false);
        adapter = new StatusOrderAdapter(getActivity(), arrayList);
        recyclerELEAdapter = new RecyclerELEAdapter(adapter, emptyView, loadingView, errorView);
        rv_status.setHasFixedSize(true);
        rv_status.setAdapter(adapter);
        rv_status.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        }));
        initAction();
    }

    private void initAction() {
        recyclerELEAdapter.setCurrentView(RecyclerELEAdapter.VIEW_LOADING);
        loadDataProgress();
    }

    private void loadDataProgress() {
        Integer wow = sharedPreferences.getInt(Constants.IDPENGGUNA, 0);
        String id = Integer.toString(wow);
        compositeSubscription.add(NetworkUtil.getRetrofit().getOrderSudahDibayar(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void onError(Throwable throwable) {

    }


    private void handleResponse(ArrayList<StatusOrderModel> statusOrderModels) {
        if (statusOrderModels != null) {
            arrayList.clear();
            arrayList.addAll(statusOrderModels);
            adapter.notifyDataSetChanged();
        } else {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}
