package ardhanmz.com.kanecater.fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ardhanmz.com.kanecater.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HowToFragment extends DialogFragment {
    @BindView(R.id.idorder_custom)
    TextView idorder;
    @BindView(R.id.atas_nama) TextView atas_nama;
    @BindView(R.id.img_howto)
    ImageView imageView;
    @BindView(R.id.textView19) TextView harga;
    String idorders, pembayaran, harga_order;


    public HowToFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_how_to, container, false);
        ButterKnife.bind(this, v);
        idorders = getArguments().getString("idorder","");
        pembayaran = getArguments().getString("metode", "");
        harga_order = getArguments().getString("harga","");
        idorder.setText(idorders);
        harga.setText(harga_order);
        if (pembayaran.equalsIgnoreCase("Bank BCA")){
            imageView.setBackgroundResource(R.drawable.ic_bca);
            atas_nama.setVisibility(View.VISIBLE);
            atas_nama.setText("123213213213 a/n Kanecater");
        } else if (pembayaran.equalsIgnoreCase("Bank Bri")){
            imageView.setBackgroundResource(R.drawable.bri);
            atas_nama.setVisibility(View.VISIBLE);
            atas_nama.setText("9232132132121 a/n Kanecater");
        }else if (pembayaran.equalsIgnoreCase("Alfamart")){
            imageView.setBackgroundResource(R.drawable.ic_alfamart);
        }else if (pembayaran.equalsIgnoreCase("Indomaret")){
            imageView.setBackgroundResource(R.drawable.ic_indomaret);
        }
        return v;
    }


}
