package ardhanmz.com.kanecater.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.acitivities.MapsActivity;
import ardhanmz.com.kanecater.acitivities.SignInActivity;
import ardhanmz.com.kanecater.model.AnakModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.netutil.ResponseLogin;
import ardhanmz.com.kanecater.netutil.ResponseSignup;
import ardhanmz.com.kanecater.utils.Constants;
import ardhanmz.com.kanecater.utils.ViewDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static ardhanmz.com.kanecater.utils.Validator.validateFields;

public class TambahAnakFragment extends Fragment implements MapsActivity.onData{
    @BindView(R.id.ti_nama_anak)
    TextInputLayout ti_nama_anak;
    @BindView(R.id.ti_tinggi_badan)
    TextInputLayout ti_tinggi_badan;
    @BindView(R.id.ti_tanggal_lahir)
    TextInputLayout ti_tanggal_lahir;
    @BindView(R.id.ti_berat_badan)
    TextInputLayout ti_berat_badan;
    @BindView(R.id.ti_alamat_sekolah)
    TextInputLayout ti_alamat;
    @BindView(R.id.ti_nama_sekolah)
    TextInputLayout ti_nama_sekolah;
    @BindView(R.id.ti_kelas_sekolah)
    TextInputLayout ti_kelas_sekolah;
    @BindView(R.id.et_nama_anak)
    EditText nama_anak;
    @BindView(R.id.et_tanggal_lahir) EditText tanggal_lahir;
    @BindView(R.id.et_tinggi_badan) EditText tinggi_badan;
    @BindView(R.id.et_berat_badan) EditText berat_badan;
    @BindView(R.id.et_nama_sekolah) EditText et_nama_sekolah;
    @BindView(R.id.et_alamat_sekolah) EditText et_alamat_sekolah;
    @BindView(R.id.et_kelas_sekolah) EditText kelas_sekolah;

    @BindView(R.id.buttonmale)
    Button laki_laki;
    @BindView(R.id.buttonfemale) Button perempuan;
    @BindView(R.id.button_tambah_anak) Button tambah_anak;

    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private String jk;
    private Calendar myCalendar;
    private FragmentActivity listener;
    private MapsActivity.onData onMapData;
    public static String namasekolah ="", alamatsekolah="";
    private static String _nama_sekolah, _alamat_sekolah;
    private boolean lakiclicked = false, perempuanclicked = false;

    ViewDialog viewDialog;
    private int fk_idpengguna;

    public  TambahAnakFragment(){

    }
    public static TambahAnakFragment TambahAnakFragment(Bundle data){
        TambahAnakFragment fragment = new TambahAnakFragment();
        fragment.setArguments(data);
        return fragment;
    }
    public static TambahAnakFragment tambahAnakFragment(String namasekolah, String alamat) {
        TambahAnakFragment fragment = new TambahAnakFragment();
        TambahAnakFragment.namasekolah = null;
        TambahAnakFragment.alamatsekolah = null;
        Bundle bundle = new Bundle();

        bundle.putString("nama_sekolah", namasekolah);
        bundle.putString("alamat_sekolah", alamat);

        return TambahAnakFragment(bundle);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            _nama_sekolah = getArguments().getString(namasekolah);
            _alamat_sekolah = getArguments().getString(alamatsekolah);
        }
        viewDialog = new ViewDialog(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (namasekolah != null) {
            et_nama_sekolah.setText(namasekolah);
            et_alamat_sekolah.setText(alamatsekolah);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            this.listener = (FragmentActivity) context;
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tambahanak, container, false);
        ButterKnife.bind(this, view);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        startAction();
        return view;
    }

    private void startAction() {
        myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        tanggal_lahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tambah_anak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prosesTambah();
            }
        });
        laki_laki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lakiclicked){
                    jk = null;
                    laki_laki.setBackgroundColor(getResources().getColor(R.color.colorSecondary));
                    lakiclicked = false;
                }else {
                    jk = laki_laki.getText().toString();
                    laki_laki.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    lakiclicked = true;
                }
            }
        });
        perempuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (perempuanclicked){
                    jk = null;
                    perempuan.setBackgroundColor(getResources().getColor(R.color.colorSecondary));
                    perempuanclicked = false;
                } else{
                    jk = perempuan.getText().toString();
                    perempuan.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    perempuanclicked = true;
                }
            }
        });
        et_nama_sekolah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                intent.putExtra("tambah_anak","tambah_anak");
                startActivity(intent);
            }
        });
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tanggal_lahir.setText(sdf.format(myCalendar.getTime()));
    }

    private void prosesTambah() {
        String nama = nama_anak.getText().toString();
//        String tinggi = tinggi_badan.getText();
        CharSequence charSequence = tinggi_badan.getText();
        int tinggi = Integer.parseInt(charSequence.toString());
        String tanggal = tanggal_lahir.getText().toString();
//        String berat = berat_badan.getText().toString();
        CharSequence charSequence1 = berat_badan.getText();
        int berat = Integer.parseInt(charSequence1.toString());
        String sekolah = et_nama_sekolah.getText().toString();
        String kelas = kelas_sekolah.getText().toString();
        String alamat = et_alamat_sekolah.getText().toString();
        int temp = sharedPreferences.getInt(Constants.TEMP_UID, 0);
        if (temp>0){
            fk_idpengguna = temp;
        }else {
            fk_idpengguna = sharedPreferences.getInt(Constants.IDPENGGUNA, 0);
        }
        int err = 0;
        if (!validateFields(nama)){
            err++;
            ti_nama_anak.setError("Nama AnakModel Tidak Boleh Kosong");
        }
        if (!validateFields(tanggal)){
            err++;
            ti_tanggal_lahir.setError("Password Tidak Boleh Kosong");
        }

        if (!validateFields(nama)){
            err++;
            ti_tanggal_lahir.setError("Password Tidak Boleh Kosong");
        }

        if (!validateFields(kelas)){
            err++;
            ti_tanggal_lahir.setError("Password Tidak Boleh Kosong");
        }
        if (err == 0){
            AnakModel anakModel = new AnakModel();
            anakModel.setNama_anak(nama);
            anakModel.setTanggal_lahir(tanggal);
            anakModel.setJenis_kelamin(jk);
            anakModel.setTinggi_badan(tinggi);
            anakModel.setBerat_badan(berat);
            anakModel.setAsal_sekolah(sekolah);
            anakModel.setAlamat_sekolah(alamat);
            anakModel.setKelas(kelas);
            anakModel.setFk_idpengguna(String.valueOf(fk_idpengguna));
            proses(anakModel);

        }
    }

    private void proses(AnakModel anakModel) {
        viewDialog.showDialog();
        compositeSubscription.add(NetworkUtil.getRetrofit().tambah_anak(anakModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void handleResponse(ResponseSignup responseSignup) {
        if (responseSignup != null){
            viewDialog.hideDialog();
            boolean status = responseSignup.getSuccess();
            if (status){
                Toasty.success(getActivity(), responseSignup.getMessage(), Toasty.LENGTH_SHORT, true).show();
                if (fk_idpengguna == sharedPreferences.getInt(Constants.TEMP_UID, 0)){
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear().commit();
                    Intent intent = new Intent(getActivity(), SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }else {
                    getActivity().finish();
                }
            }else {
                Toasty.warning(getActivity(), responseSignup.getMessage(), Toasty.LENGTH_SHORT, true).show();
            }
        }
    }
    private void onError(Throwable err){
        if(err instanceof HttpException){
            Gson gson = new GsonBuilder().create();
            try {
                String errorBody = ((HttpException)err).response().errorBody().string();
                ResponseLogin response = gson.fromJson(errorBody, ResponseLogin.class);

            } catch (IOException er){
                er.printStackTrace();
            }
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    @Override
    public void OnMapDataRetrieve(String nama_sekolah, String alamat) {
        et_nama_sekolah.setText(nama_sekolah);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        et_alamat_sekolah.clearComposingText();
        et_nama_sekolah.clearComposingText();
        compositeSubscription.unsubscribe();
    }
}
