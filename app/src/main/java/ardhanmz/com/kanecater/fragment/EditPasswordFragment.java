package ardhanmz.com.kanecater.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.ChangePassword;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.netutil.ResponseSignup;
import ardhanmz.com.kanecater.netutil.RetrofitInterface;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditPasswordFragment extends Fragment {
    @BindView(R.id.ti_old_password)
    TextInputLayout ti_old_password;
    @BindView(R.id.ti_password_new)
    TextInputLayout ti_password_new;
    @BindView(R.id.ti_password_new_validation)
    TextInputLayout ti_password_new_v;
    @BindView(R.id.et_old_password)
    EditText et_password_old;
    @BindView(R.id.et_password_new)
    EditText et_password_new;
    @BindView(R.id.et_password_new_validation)
    EditText et_password_new_v;

    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private Context mContext;
    private String old_pass, new_pass, pass_validation;
    private ProgressDialog mProgressDialog;


    public EditPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mContext = getContext();
        View view= inflater.inflate(R.layout.fragment_edit_password, container, false);
        ButterKnife.bind(this, view);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        compositeSubscription = new CompositeSubscription();
        onTextListener();
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Autentikasi..");
        return view;
    }

    private void onTextListener() {
        old_pass = et_password_old.getText().toString();
        new_pass = et_password_new.getText().toString();
        pass_validation = et_password_new_v.getText().toString();
        boolean valid = false;
        if (pass_validation.equals(new_pass)){
            valid = true;
        }else {
            valid = false;
        }
        if (valid){
            ChangePassword changePassword = new ChangePassword();
            changePassword.setOld_password(old_pass);
            changePassword.setNew_password(new_pass);
            changePassword.setNew_password(pass_validation);
            initialize(changePassword);
        }else {
            et_password_new.setError("Password Baru Tidak Sama");
            et_password_new_v.setError("Password Baru Tidak Sama");
        }
    }

    private void initialize(ChangePassword changePassword) {
        mProgressDialog.show();
        compositeSubscription.add(NetworkUtil.getRetrofit().changePassword(changePassword)
                .observeOn(AndroidSchedulers.mainThread())
                .observeOn(Schedulers.io()).subscribe(this::handleResponse,this::onError));
    }


    private void handleResponse(ResponseSignup responseSignup) {
        if (responseSignup.getSuccess()){
            if (mProgressDialog.isShowing()){
                mProgressDialog.dismiss();
            }
            Toast.makeText(mContext, responseSignup.getMessage(), Toast.LENGTH_SHORT).show();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStack();
        } else {
            if (mProgressDialog.isShowing()){
                mProgressDialog.dismiss();
            }
            Toast.makeText(mContext, responseSignup.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    private void onError(Throwable throwable){

    }


}
