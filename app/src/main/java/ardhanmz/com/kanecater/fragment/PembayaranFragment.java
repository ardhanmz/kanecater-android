package ardhanmz.com.kanecater.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.acitivities.DashboardActivity;
import ardhanmz.com.kanecater.acitivities.Pemesanan;
import ardhanmz.com.kanecater.model.OrderRequest;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.netutil.ResponseSignup;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class PembayaranFragment extends Fragment {
    @BindView(R.id.bca)
    RadioButton bca;
    @BindView(R.id.bri)
    RadioButton bri;
    @BindView(R.id.alfamart)
    RadioButton alfamart;
    @BindView(R.id.indomaret)
    RadioButton indomaret;
    @BindView(R.id.paybutton)
    Button bayar;
    @BindView(R.id.radiogroup1)
    RadioGroup bank;
    @BindView(R.id.radiogroup2)
    RadioGroup retail;
    private CompositeSubscription compositeSubscription;
    private SharedPreferences sharedPreferences;
    private String metode, selectedstring;
    private Integer checked = 0;
    private String idorder;

    public PembayaranFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pembayaran, container, false);
        ButterKnife.bind(this, view);
        compositeSubscription = new CompositeSubscription();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        radioButton();
        return view;
    }

    private void radioButton() {
        bank.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.bca){
                    if (checked==0){
                        checked = 1;
                        metode = "1";
                    }else {
                        Toasty.warning(getActivity(), "Anda Telah Memilih Metode Pembayaran",Toasty.LENGTH_SHORT, true).show();
                        bank.clearCheck();
                        retail.clearCheck();
                        checked=0;
                    }
                }
                if (i == R.id.bri){
                    if (checked==0){
                        metode = "2";
                        checked = 2;
                    } else {
                        Toasty.warning(getActivity(), "Anda Telah Memilih Metode Pembayaran",Toasty.LENGTH_SHORT, true).show();
                        bank.clearCheck();
                        retail.clearCheck();
                        checked=0;
                    }

                }
            }
        });
        retail.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.alfamart){
                    if (checked==0){
                        metode = "3";
                        checked = 3;
                    } else {
                        Toasty.warning(getActivity(), "Anda Telah Memilih Metode Pembayaran",Toasty.LENGTH_SHORT, true).show();
                        retail.clearCheck();
                        bank.clearCheck();
                        checked = 0;
                    }

                }
                if (i == R.id.indomaret){
                    if (checked==0){
                        metode = "4";
                        checked = 4;
                    }else {
                        Toasty.warning(getActivity(), "Anda Telah Memilih Metode Pembayaran",Toasty.LENGTH_SHORT, true).show();
                        retail.clearCheck();
                        bank.clearCheck();
                        checked = 0;
                    }

                }
            }
        });
        bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedBank = bank.getCheckedRadioButtonId();
                int selectedRetail = retail.getCheckedRadioButtonId();
                idorder = getArguments().getString("idorder","");
                OrderRequest orderRequest = new OrderRequest();
                orderRequest.setIddata_order(idorder);
                if (selectedBank == bca.getId()){
                    orderRequest.setFk_idpembayaran(1);
                    selectedstring = "Bank BCA";
                } else if (selectedBank == bri.getId()){
                    orderRequest.setFk_idpembayaran(2);
                    selectedstring = "Bank BRI";
                }
                if (selectedRetail == alfamart.getId()){
                    orderRequest.setFk_idpembayaran(3);
                    selectedstring = "Alfamart";
                } else if (selectedRetail == indomaret.getId()){
                    orderRequest.setFk_idpembayaran(4);
                    selectedstring = "Indomaret";
                }
                updatePayment(orderRequest);
            }
        });
    }

    private void updatePayment(OrderRequest orderRequest) {
        compositeSubscription.add(NetworkUtil.getRetrofit().paymentOrderCustom(orderRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::onError));
    }

    private void onError(Throwable throwable) {
    }

    private void handleResponse(ResponseSignup responseSignup) {
        if (responseSignup.getSuccess()) {
            if (idorder!=null){
                Bundle bundle = new Bundle();
                bundle.putString("idorder", idorder);
                bundle.putString("Metode", selectedstring);
                Toasty.success(getContext(), responseSignup.getMessage(), Toasty.LENGTH_SHORT, true).show();
                Intent intent = new Intent(getContext(), Pemesanan.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        } else {
            Toasty.error(getContext(), responseSignup.getMessage(), Toasty.LENGTH_SHORT, true).show();
        }
    }
}
