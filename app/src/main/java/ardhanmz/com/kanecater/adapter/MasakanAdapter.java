package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.response.PaketCustomResponse;

public class MasakanAdapter extends RecyclerView.Adapter<MasakanAdapter.MasakanHolder>{
    private ArrayList<PaketCustomResponse> arrayList;
    private Context mContext;

    public MasakanAdapter(Context context, ArrayList<PaketCustomResponse> masakanModelArrayList){
        this.mContext = context;
        this.arrayList = masakanModelArrayList;
    }

    @Override
    public MasakanHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_makananlist, null);
        MasakanHolder mh = new MasakanHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(MasakanHolder holder, int i) {

        PaketCustomResponse singleItem = arrayList.get(i);

        holder.tvMasakan.setText(singleItem.getNAMA());
        Glide.with(mContext)
                .load(singleItem.getIMG())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.imageView);

       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }
    public class MasakanHolder extends RecyclerView.ViewHolder{
        protected TextView tvMasakan;
        protected ImageView imageView;
        public  MasakanHolder(View view){
            super(view);
            tvMasakan = (TextView) view.findViewById(R.id.nama_masakan);
            imageView = (ImageView) view.findViewById(R.id.img_masakan);

        }
    }
}
