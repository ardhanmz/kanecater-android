package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.ListOrderModel;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ListOrderAdapter extends RecyclerView.Adapter<ListOrderAdapter.ListOrderHolder>{
    private ArrayList<ListOrderModel> arrayList;
    private Context mContext;
    private Date date1, date;

    public ListOrderAdapter(Context context, ArrayList<ListOrderModel> listOrderModels){
        this.mContext = context;
        this.arrayList = listOrderModels;
    }

    @NonNull
    @Override
    public ListOrderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_orderlist,null);
        ListOrderHolder listOrderHolder = new ListOrderHolder(view);
        return listOrderHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListOrderHolder holder, int position) {
        ListOrderModel listOrderModel = arrayList.get(position);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
// use UTC as timezone
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            date = sdf.parse(listOrderModel.getLanggananAwal());
            date1 = sdf.parse(listOrderModel.getLanggananAkhir());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMMM yy", Locale.getDefault());
// assuming a timezone in India
        outputFormat.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
        System.out.println(outputFormat.format(date));
        String output1 = outputFormat.format(date);
        String output2 = outputFormat.format(date1);
        holder.tanggal_awal.setText(output1);
        holder.tanggal_akhir.setText(output2);
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class ListOrderHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.nama_order)
        TextView nama_order;
        @BindView(R.id.harga_order)
        TextView harga_order;
        @BindView(R.id.tanggal_awal) TextView
                tanggal_awal;
        @BindView(R.id.tanggal_akhir) TextView
                tanggal_akhir;
        public ListOrderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
