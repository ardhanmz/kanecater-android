package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.response.PaketLanggananResponse;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ListLanggananAdapter extends RecyclerView.Adapter<ListLanggananAdapter.ListLanggananHolder> {
    private ArrayList<PaketLanggananResponse> arrayList;
    private Context mContext;

    public ListLanggananAdapter(Context context, ArrayList<PaketLanggananResponse> dashboardPaketModels){
        this.mContext = context;
        this.arrayList = dashboardPaketModels;
    }
    @Override
    public ListLanggananHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_paket, null);
        ListLanggananHolder holder = new ListLanggananHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListLanggananHolder holder, int position) {
        PaketLanggananResponse paketLanggananResponse = arrayList.get(position);
        holder.nama_paket.setText(paketLanggananResponse.getNamaPaket());
        Glide.with(mContext)
                .load(paketLanggananResponse.getImagePaket())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class ListLanggananHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.paket_name)
        TextView nama_paket;
        @BindView(R.id.paket_image)
        ImageView imageView;
        public ListLanggananHolder (View view){
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
