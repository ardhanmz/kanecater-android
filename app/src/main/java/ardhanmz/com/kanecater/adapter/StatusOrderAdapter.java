package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.ListOrderModel;
import ardhanmz.com.kanecater.model.StatusOrderModel;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StatusOrderAdapter extends RecyclerView.Adapter<StatusOrderAdapter.StatusOrderHolder> {
    private ArrayList<StatusOrderModel> arrayList;
    private Context mContext;
    private Date date1, date;

    public StatusOrderAdapter(Context context, ArrayList<StatusOrderModel> listOrderModels){
        this.mContext = context;
        this.arrayList = listOrderModels;
    }

    @NonNull
    @Override
    public StatusOrderAdapter.StatusOrderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_orderlist,null);
        StatusOrderAdapter.StatusOrderHolder listOrderHolder = new StatusOrderAdapter.StatusOrderHolder(view);
        return listOrderHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull StatusOrderHolder statusOrderHolder, int i) {
        StatusOrderModel listOrderModel = arrayList.get(i);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String wkwk = listOrderModel.getNamaAnak()+" - "+listOrderModel.getTipePaket();
        statusOrderHolder.nama_order.setText(wkwk);
        statusOrderHolder.tanggal_awal.setText(listOrderModel.getLanggananAwal());
        statusOrderHolder.tanggal_akhir.setText(listOrderModel.getLanggananAkhir());
        if (listOrderModel.getProgresOrder().equalsIgnoreCase("Menunggu Pihak Katering")){
            statusOrderHolder.status_pengiriman.setText(listOrderModel.getProgresOrder());
            statusOrderHolder.status_pengiriman.setTextColor(mContext.getResources().getColor(R.color.yellow));
        }else if (listOrderModel.getProgresOrder().equalsIgnoreCase("Memproses Pemesanan")){
            statusOrderHolder.status_pengiriman.setText(listOrderModel.getProgresOrder());
            statusOrderHolder.status_pengiriman.setTextColor(mContext.getResources().getColor(R.color.orange));
        } else if (listOrderModel.getProgresOrder().equalsIgnoreCase("Mengirim Order")){
            statusOrderHolder.status_pengiriman.setText(listOrderModel.getProgresOrder());
            statusOrderHolder.status_pengiriman.setTextColor(mContext.getResources().getColor(R.color.green));
        }else if (listOrderModel.getProgresOrder().equalsIgnoreCase("Order Telah Sampai")){
            statusOrderHolder.status_pengiriman.setText(listOrderModel.getProgresOrder());
            statusOrderHolder.status_pengiriman.setTextColor(mContext.getResources().getColor(R.color.dark_blue));
        }
        Glide.with(mContext)
                .load(listOrderModel.getImagePaket())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(statusOrderHolder.gambar);
    }


    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class StatusOrderHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.nama_order)
        TextView nama_order;
        @BindView(R.id.harga_order)
        TextView harga_order;
        @BindView(R.id.tanggal_awal) TextView
                tanggal_awal;
        @BindView(R.id.tanggal_akhir) TextView
                tanggal_akhir;
        @BindView(R.id.progres_order) TextView status_pengiriman;
        @BindView(R.id.imageView3)
        ImageView gambar;
        public StatusOrderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
