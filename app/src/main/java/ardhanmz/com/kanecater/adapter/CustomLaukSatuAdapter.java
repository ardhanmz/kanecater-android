package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.LaukSatuCustomModel;
import ardhanmz.com.kanecater.response.MenuLaukSatuResponse;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomLaukSatuAdapter extends RecyclerView.Adapter<CustomLaukSatuAdapter.CustomLaukSatuHolder>{
    private ArrayList<MenuLaukSatuResponse> arrayList;
    private Context mContext;

    public CustomLaukSatuAdapter(Context context, ArrayList<MenuLaukSatuResponse> laukSatuCustomModels){
        this.arrayList = laukSatuCustomModels;
        this.mContext = context;
    }

    @Override
    public CustomLaukSatuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_lauk_satu, null);
        CustomLaukSatuHolder customNasiHolder = new CustomLaukSatuHolder(v);
        return customNasiHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomLaukSatuHolder holder, int position) {
        MenuLaukSatuResponse single = arrayList.get(position);
        holder.textView.setText(single.getNamaMenu());
        Glide.with(mContext)
                .load(single.getImageMenu())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class CustomLaukSatuHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_lauk_satu_custom)
        ImageView imageView;
        @BindView(R.id.nama_lauk_satu_custom)
        TextView textView;

        public CustomLaukSatuHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
