package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.response.PaketLanggananResponse;

public class PaketAdapter extends  RecyclerView.Adapter<PaketAdapter.SingleItemRowHolder>{
    private ArrayList<PaketLanggananResponse> itemsList;
    private Context mContext;

    public PaketAdapter(Context context, ArrayList<PaketLanggananResponse> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dashboard_paket_list_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {
        PaketLanggananResponse paketLanggananResponse = itemsList.get(i);

        holder.tvTitle.setText(paketLanggananResponse.getNamaPaket());
        holder.hargaPaket.setText(paketLanggananResponse.getNamaPaket());
        Glide.with(mContext)
                .load(paketLanggananResponse.getImagePaket())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.itemImage);

       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle,hargaPaket;
        public int id;

        protected ImageView itemImage;


        public SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.nama_paket);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
            this.hargaPaket = (TextView) view.findViewById(R.id.harga_paket);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();

                }
            });


        }

    }
}
