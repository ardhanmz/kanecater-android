package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.OrderRequest;
import ardhanmz.com.kanecater.model.ProgressOrderModel;
import ardhanmz.com.kanecater.netutil.NetworkUtil;
import ardhanmz.com.kanecater.netutil.ResponseSignup;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ProgressOrderAdapter extends RecyclerView.Adapter<ProgressOrderAdapter.ProgressOrderHolder> {
    private ArrayList<ProgressOrderModel> arrayList;
    private Context mContext;
    private Date date, date1;
    private CompositeSubscription compositeSubscription;
    private onClickListener mListener;

    public interface onClickListener{
        void onDeleteListener(int position);
        void onHowPayListener(int position);

    }
    public void setOnItemClickListener(onClickListener listener){
        mListener = listener;
    }
    public ProgressOrderAdapter(Context context, ArrayList<ProgressOrderModel> progressOrderModels){
        this.mContext= context;
        this.arrayList = progressOrderModels;
    }

    @Override
    public ProgressOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_progressorder, null);
        compositeSubscription = new CompositeSubscription();
        ProgressOrderHolder progressOrderHolder = new ProgressOrderHolder(view);
        return progressOrderHolder;
    }

    @Override
    public void onBindViewHolder(ProgressOrderHolder holder, int position) {
        ProgressOrderModel progressOrderModel = arrayList.get(position);
        String wkwk = progressOrderModel.getNamaAnak()+" - "+progressOrderModel.getTipePaket();
        holder.nama_order.setText(wkwk);
        holder.tanggal_awal.setText(progressOrderModel.getLanggananAwal());
        holder.tanggal_akhir.setText(progressOrderModel.getLanggananAkhir());
        holder.status_pembayaran.setText(progressOrderModel.getStatusPembayaran());
        Glide.with(mContext)
                .load(progressOrderModel.getImagePaket())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.gambar);
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class ProgressOrderHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.nama_order_progress)
        TextView nama_order;
        @BindView(R.id.harga_order_progress)
        TextView harga_order;
        @BindView(R.id.tanggal_awal_progress) TextView
        tanggal_awal;
        @BindView(R.id.tanggal_akhir_progress) TextView
        tanggal_akhir;
        @BindView(R.id.status_pembayaran) TextView
        status_pembayaran;
        @BindView(R.id.imageView3)
        ImageView gambar;
        @BindView(R.id.cancel_order)
        Button btn_cancel;
        @BindView(R.id.howtopay)
        Button btn_how;
        public ProgressOrderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener!=null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            mListener.onDeleteListener(position);
                        }
                    }
                }
            });
            btn_how.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener!=null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            mListener.onHowPayListener(position);
                        }
                    }
                }
            });
        }
    }
}
