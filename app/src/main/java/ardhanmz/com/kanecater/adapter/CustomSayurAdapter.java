package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.SayurCustomModel;
import ardhanmz.com.kanecater.response.MenuSayurResponse;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomSayurAdapter extends RecyclerView.Adapter<CustomSayurAdapter.CustomSayurHolder>{
    private ArrayList<MenuSayurResponse> arrayList;
    private Context mContext;

    public CustomSayurAdapter(Context context, ArrayList<MenuSayurResponse> laukSatuCustomModels){
        this.arrayList = laukSatuCustomModels;
        this.mContext = context;
    }


    @Override
    public CustomSayurHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_sayur, null);
        CustomSayurHolder customSayurHolder = new CustomSayurHolder(view);
        return customSayurHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomSayurHolder holder, int position) {
        MenuSayurResponse sayurCustomModel =  arrayList.get(position);
        holder.textView.setText(sayurCustomModel.getNamaMenu());
        Glide.with(mContext)
                .load(sayurCustomModel.getImageMenu())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.lauk_dua);
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class CustomSayurHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_sayur_custom)
        ImageView lauk_dua;
        @BindView(R.id.nama_sayur_custom)
        TextView textView;

        public CustomSayurHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
