package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.BuahCustomModel;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomBuahAdapter extends RecyclerView.Adapter<CustomBuahAdapter.CustomLaukDuaHolder>{
    private ArrayList<BuahCustomModel> arrayList;
    private Context mContext;

    public CustomBuahAdapter(Context context, ArrayList<BuahCustomModel> laukSatuCustomModels){
        this.arrayList = laukSatuCustomModels;
        this.mContext = context;
    }

    @NonNull
    @Override
    public CustomLaukDuaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomLaukDuaHolder holder, int position) {
        BuahCustomModel buahCustomModel = arrayList.get(position);
        holder.textView.setText(buahCustomModel.getNamaBuah());
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class CustomLaukDuaHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_buah_custom)
        ImageView lauk_dua;
        @BindView(R.id.nama_buah_custom)
        TextView textView;

        public CustomLaukDuaHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
