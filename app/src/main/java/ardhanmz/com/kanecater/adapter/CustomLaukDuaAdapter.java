package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.model.LaukDuaCustomModel;
import ardhanmz.com.kanecater.response.MenuLaukDuaResponse;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomLaukDuaAdapter extends RecyclerView.Adapter<CustomLaukDuaAdapter.CustomLaukDuaHolder> {
    private ArrayList<MenuLaukDuaResponse> arrayList;
    private Context mContext;

    public CustomLaukDuaAdapter(Context context, ArrayList<MenuLaukDuaResponse> laukSatuCustomModels){
        this.arrayList = laukSatuCustomModels;
        this.mContext = context;
    }

    @Override
    public CustomLaukDuaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_lauk_dua, null);
        CustomLaukDuaHolder customLaukDuaHolder = new CustomLaukDuaHolder(v);
        return customLaukDuaHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomLaukDuaHolder holder, int position) {
        MenuLaukDuaResponse single = arrayList.get(position);
        holder.textView.setText(single.getNamaMenu());
        Glide.with(mContext)
                .load(single.getImageMenu())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.lauk_dua);
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class CustomLaukDuaHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_lauk_dua_custom)
        ImageView lauk_dua;
        @BindView(R.id.nama_lauk_dua_custom)
        TextView textView;

        public CustomLaukDuaHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
