package ardhanmz.com.kanecater.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.response.OrderResponse;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListHolder>{
    private ArrayList<OrderResponse> arrayList;
    private Context mContext;
    private Date date, date1;

    public OrderListAdapter(Context context, ArrayList<OrderResponse> arrayList){
        this.arrayList = arrayList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public OrderListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_listorder,null);
        OrderListHolder orderListHolder = new OrderListHolder(view);
        return orderListHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListHolder holder, int position) {
        OrderResponse orderModel = arrayList.get(position);
        String wkwk = orderModel.getNamaAnak()+" - "+orderModel.getTipePaket();
        holder.order_tv.setText(wkwk);
//        holder.order_tv.setText(orderModel.getFk_idpembayaran());
        // treat "Z" as literal
        holder.tanggal_awal.setText(orderModel.getLanggananAwal());
        holder.tanggal_akhir.setText(orderModel.getLanggananAwal());
        Glide.with(mContext)
                .load(orderModel.getImagePaket())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.imageView);
    }
    public static String getMonthShortName(int monthNumber) {
        String monthName="";
        if(monthNumber>=0 && monthNumber<12)
            try
            {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.MONTH, monthNumber);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
                simpleDateFormat.setCalendar(calendar);
                monthName = simpleDateFormat.format(calendar.getTime());
            }
            catch (Exception e)
            {
                if(e!=null)
                    e.printStackTrace();
            }
        return monthName;
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class OrderListHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.nama_order)
        TextView order_tv;
        @BindView(R.id.harga_order)
        TextView harga_order;
        @BindView(R.id.gambar_12)
        ImageView imageView;
        @BindView(R.id.tanggal_awal)
        TextView tanggal_awal;
        @BindView(R.id.tanggal_akhir)
        TextView tanggal_akhir;

        public OrderListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
