package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.response.AnakResponse;
import ardhanmz.com.kanecater.response.DataAnakResponse;

public class DataAnakAdapter extends RecyclerView.Adapter<DataAnakAdapter.DataAnakHolder>{
    private ArrayList<AnakResponse> arrayList;
    private Context mContext;
    AnakResponse singleItem;

    public DataAnakAdapter (Context context, ArrayList<AnakResponse> arrayList){
        this.arrayList = arrayList;
        this.mContext = context;
    }
    @Override
    public DataAnakHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_profile, null);
        DataAnakHolder mh = new DataAnakHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(DataAnakHolder holder, int i) {

        singleItem = arrayList.get(i);

        holder.tvNamaAnak.setText(singleItem.getNamaAnak());
        /*Glide.with(mContext)
                .load(singleItem.getImage_masakan())
                .into(holder.imageView);
        */
       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }
    public class DataAnakHolder extends RecyclerView.ViewHolder{
        protected TextView tvNamaAnak;
        public DataAnakHolder (View view){
            super(view);
            tvNamaAnak = (TextView) view.findViewById(R.id.txtanak);
        }
    }
}
