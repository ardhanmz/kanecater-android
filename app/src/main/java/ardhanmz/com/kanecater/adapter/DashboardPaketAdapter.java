package ardhanmz.com.kanecater.adapter;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.acitivities.PaketLanggananActivity;
import ardhanmz.com.kanecater.model.KategoriPaketDBModel;
import ardhanmz.com.kanecater.utils.RecyclerItemClickListener;

public class DashboardPaketAdapter extends RecyclerView.Adapter<DashboardPaketAdapter.ItemRowHolder> {

        private ArrayList<KategoriPaketDBModel> dataList;
        private Context mContext;

        public DashboardPaketAdapter(Context context, ArrayList<KategoriPaketDBModel> dataList) {
            this.dataList = dataList;
            this.mContext = context;
        }

        @Override
        public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.paket_list_item, null);
            ItemRowHolder mh = new ItemRowHolder(v);
            return mh;
        }

        @Override
        public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {

            final String sectionName = dataList.get(i).getHeaderTitle();

            ArrayList singleSectionItems = dataList.get(i).getAllItemInSection();

            itemRowHolder.itemTitle.setText(sectionName);

           PaketAdapter itemListDataAdapter = new PaketAdapter(mContext, singleSectionItems);

            itemRowHolder.recycler_view_list.setHasFixedSize(true);
            itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);
            itemRowHolder.recycler_view_list.addOnItemTouchListener(new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    Integer idlanggann = dataList.get(0).getAllItemInSection().get(position).getIdPaket();
                    String id = Integer.toString(idlanggann);
                    Bundle bundle = new Bundle();
                    bundle.putString("id", id);
                    bundle.putString("args", "detail");
                    /*Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();*/
                    Intent intent = new Intent(mContext, PaketLanggananActivity.class);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            }));

            itemRowHolder.btnMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Bundle bundle = new Bundle();
                    Intent intent = new Intent(mContext, PaketLanggananActivity.class);
                    bundle.putString("args", "paket_langganan");
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);



                }
            });


       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
        }

        @Override
        public int getItemCount() {
            return (null != dataList ? dataList.size() : 0);
        }

public class ItemRowHolder extends RecyclerView.ViewHolder {

    protected TextView itemTitle;

    protected RecyclerView recycler_view_list;

    protected TextView btnMore;



    public ItemRowHolder(View view) {
        super(view);

        this.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
        this.recycler_view_list = (RecyclerView) view.findViewById(R.id.recycler_view_list);
        this.btnMore= (TextView) view.findViewById(R.id.btnMore);


    }

}


}
