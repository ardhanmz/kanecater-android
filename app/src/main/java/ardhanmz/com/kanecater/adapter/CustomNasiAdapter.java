package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.response.MenuNasiResponse;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomNasiAdapter extends RecyclerView.Adapter<CustomNasiAdapter.CustomNasiHolder> {
    private ArrayList<MenuNasiResponse> arrayList;
    private Context mContext;

    public CustomNasiAdapter(Context context, ArrayList<MenuNasiResponse> list){
        this.mContext = context;
        this.arrayList = list;
    }


    @Override
    public CustomNasiHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_nasi, parent, false);
        CustomNasiHolder customNasiHolder = new CustomNasiHolder(v);
        return customNasiHolder;
    }

    @Override
    public void onBindViewHolder(CustomNasiHolder holder, int position) {
        MenuNasiResponse singleitem = arrayList.get(position);
        holder.nasi_custom.setText(singleitem.getNamaNasi());
        Glide.with(mContext)
                .load(singleitem.getImage_menu())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class CustomNasiHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.content_nasi_custom)
        TextView nasi_custom;
        @BindView(R.id.img_nasi_custom)
        ImageView imageView;

        public CustomNasiHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
