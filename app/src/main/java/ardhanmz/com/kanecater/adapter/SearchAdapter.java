package ardhanmz.com.kanecater.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ardhanmz.com.kanecater.R;
import ardhanmz.com.kanecater.response.SearchResponse;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchHolder>{
    ArrayList<SearchResponse> arrayList;
    Context context;

    public SearchAdapter(Context context, ArrayList<SearchResponse> searchResponses){
        this.context = context;
        this.arrayList = searchResponses;
    }


    @Override
    public SearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_search, null);
        SearchHolder searchHolder = new SearchHolder(v);
        return searchHolder;
    }

    @Override
    public void onBindViewHolder(SearchHolder holder, int position) {
        SearchResponse searchResponse = arrayList.get(position);
        holder.nama_masakan_s.setText(searchResponse.getNamaPaket());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class SearchHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_masakan_s)
        ImageView img_masakan_s;
        @BindView(R.id.nama_masakan_s)
        TextView nama_masakan_s;
        public SearchHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
